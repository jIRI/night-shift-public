import { AvailableSounds } from "../builders/sound-player";
import { LocalizedText } from "../types/lang";

export enum ActorId {
  left,
  right,
  dummy,
}

export enum ActionId {
  playAnimation,
  continueAnimation,
  finishAnimation,
  show,
  hide,
  enableAnimationSync,
  disableAnimationSync,
  playSound,
}

export type ActionConfig = {
  do: ActionId.playAnimation
  | ActionId.continueAnimation
  | ActionId.finishAnimation
  | ActionId.show
  | ActionId.hide
  | ActionId.enableAnimationSync
  | ActionId.disableAnimationSync,
} | {
  do: ActionId.playSound,
  sound: AvailableSounds
};

export type ActorConfig = {
  id: ActorId;
  name: string;
  textEncoder?: (text:LocalizedText) => LocalizedText;
};

export type SelectItem = {
  text: string;
  gotoLabel: string;
};

export type Line = {
  actorId: ActorId;
  text: LocalizedText;
  visited?: boolean;
  actions?: Array<ActionConfig>;
  label?: string;
  select?: Array<SelectItem>;
}

export function getLocalizedText(localizedText: LocalizedText, lang: keyof LocalizedText): string {
  return localizedText[lang];
}

const telepathicEncoderReplacers: Array<(s: string) => string> = [
  s => s.replace(/[AÁ]/g, 'GR'),
  s => s.replace(/[EÉĚ]/g, 'RR'),
  s => s.replace(/[IÍ]/g, 'QR'),
  s => s.replace(/[OÓ]/g, 'RX'),
  s => s.replace(/[UÚŮ]/g, 'WR'),
  s => s.replace(/[YÝ]/g, 'TQ'),
  s => s.replace(/[ŠČŘŽĎŇ]/g, 'WT'),
];

export function applyReplacers(text: string, replacers: Array<(s: string) => string>): string {
  let result = text.toUpperCase();
  for(const replacer of replacers) {
    result = replacer(result);
  }
  return result;
}

export class Script {
  currentLineIndex = 0;
  lines: Array<Line>;
  actorMap = new Map<ActorId, ActorConfig>();

  constructor() {
    this.lines = Script.lines();
    Script.actors().forEach(actor => this.actorMap.set(actor.id, actor));
  }

  actor(id: ActorId): ActorConfig {
    return this.actorMap.get(id) ?? Script.actors()[0];
  }

  reset() {
    this.currentLineIndex = -1;
    this.lines.forEach(line => line.visited = false);
  }

  nextLine(input?: string): Line | null {
    if (this.currentLineIndex < 0) {
      // first invocation, just return first line
      this.currentLineIndex = 0;
    } else {
      if (input != null ) {
        // we got input, try find index of the label
        const nextLineIndex = this.lines.findIndex(line => line.label === input);
        if (nextLineIndex >= 0) {
          // label found, jump to label
          this.currentLineIndex = nextLineIndex;
        } else {
          // label not found, well, continue on next line...
          this.currentLineIndex++;
        }
      } else {
        // no input, just continue on next line
        this.currentLineIndex++;
      }
    }

    if (this.currentLineIndex >= this.lines.length) {
      return null;
    }

    this.lines[this.currentLineIndex].visited = true;
    const line = Object.assign({}, this.lines[this.currentLineIndex]);
    const textEncoder = this.actor(line.actorId).textEncoder;
    if( textEncoder != null) {
      line.text = textEncoder(line.text);
    }
    return line;
  }

  //
  // Actors and script definition follows
  //

  static actors(): Array<ActorConfig>  {
    return [
      { id: ActorId.left, name: 'Karaglaprax' },
      { id: ActorId.right, name: 'Okelidora' },
    ];
  }

  static telepathicEncoder(text: LocalizedText): LocalizedText {
    if(text.cz.startsWith('[') || text.en.startsWith('[')) {
      return text;
    }

    let cz = applyReplacers(`<${text.cz}>`, telepathicEncoderReplacers);
    let en = applyReplacers(`<${text.en}>`, telepathicEncoderReplacers);

    return {
      cz,
      en,
    };
  }

  static droidEncoder(text: LocalizedText): LocalizedText {
    if(text.cz.startsWith('[') || text.en.startsWith('[')) {
      return text;
    }

    let cz = `{ ${text.cz} }`;
    let en = `{ ${text.en} }`;

    return {
      cz,
      en,
    };
  }


  static lines(): Array<Line> {
    return [
      { actorId: ActorId.left,
        text: {
          cz: '[vzlyk]',
          en: '',
        },
      },
      { actorId: ActorId.left,
        text: {
          cz: '[...]',
          en: '',
        },
      },
      { actorId: ActorId.left,
        text: {
          cz: '[škyt]',
          en: '',
        },
        actions: [ { do: ActionId.playAnimation } ],
      },
      { actorId: ActorId.left,
        text: {
          cz: '[...]',
          en: '',
        },
      },
      { actorId: ActorId.left,
        text: {
          cz: '[fňuk]',
          en: '',
        },
      },
      { actorId: ActorId.right,
        text: {
          cz: '[...]',
          en: '',
        },
        actions: [ { do: ActionId.show } ],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Co bulíš?',
          en: '',
        },
      },
      { actorId: ActorId.left,
        text: {
          cz: '[smrk]',
          en: '',
        },
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Ah.',
          en: '',
        },
      },
      { actorId: ActorId.left,
        text: {
          cz: '[vzlyk]',
          en: '',
        },
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Ty jsi přečetl A každé ráno je cesta domů delší a delší, že jo?',
          en: '',
        },
      },
      { actorId: ActorId.left,
        text: {
          cz: '[kýv]',
          en: '',
        },
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Neříkala jsem ti, že to nemáš číst?',
          en: '',
        },
        actions: [ { do: ActionId.continueAnimation } ],
      },
      { actorId: ActorId.left,
        text: {
          cz: '[smrrrrrk]',
          en: '',
        },
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Já vím, já vím. Ale když já jsem od stejného autora četl Muž jménem Ove, a to, i když je to taky vlastně hrozně smutná kniha, tak je v ní i dost humoru a končí vlastně na optimistickou notu. Tak jsem myslel, že to bude něco podobného.',
          en: '',
        },
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Áááha, jasně. No ale Muž jménem Ove je kniha o hledaní naděje a smyslu života, když přijdeš o to, co tě drželo nad vodou v předchozí fázi života.',
          en: '',
        },
      },
      { actorId: ActorId.right,
        text: {
          cz: 'A taky o tom, že i když je život skoro až nesnesitelný a sakra těžký a přihodilo se ti spousta hnusných věcí tak se vždycky dá najít něco, pro co stojí za to žít a tak.',
          en: '',
        },
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Proti tomu je A každé ráno je cesta domů delší a delší přesně ten důvod, proč směrnice zakazují vystavovat se jinocivilizační fikci.',
          en: '',
        },
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Hlavně když je to fikce jen tak napůl.',
          en: '',
        },
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Pustíš se do knihy, která je sice krátká, ale emocionálně hrozně vyhrocená, a říkáš si, že těch pár stránek ti nemůže ublížit, ale ve skutečnosti je to mnohem nebezpečnější, než úplně smyšlené rozvláčné propírání stejné věci.',
          en: '',
        },
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Protože víš, že tohle se těm lidem ve skutečnosti děje.',
          en: '',
        },
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Takže musíš opatrně a postupně se zocelovat, než se do takové knihy pustíš. Pozemšťané v tomhle žijí pořád, takže mají rezistenci. Říkají tomu nezdolnej lidskej duch, myslím.',
          en: '',
        },
      },
      { actorId: ActorId.left,
        text: {
          cz: 'No jo, já vím. Ale i A každé ráno je cesta domů delší a delší končí vlastně malinko optimisticky, protože i když se někdy není pomoci jednotlivcům, tak skrze další generace vlastně věci nejsou tak hrozný. Ale dostalo mě to, no.',
          en: '',
        },
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Budu si na to dávat větší pozor.',
          en: '',
        },
        actions: [ { do: ActionId.continueAnimation } ],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'To je dobře. Hele, na, zkus si terapeuticky přečíst třeba tohle.',
          en: '',
        },
      },
      { actorId: ActorId.right,
        text: {
          cz: '[šoup]',
          en: '',
        },
      },
      { actorId: ActorId.left,
        text: {
          cz: '[chmát]',
          en: '',
        },
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Hearts of Oak? Co to je?',
          en: '',
        },
      },
      { actorId: ActorId.right,
        text: {
          cz: 'To je taková jako nenáročná, poměrně kompetentně napsaná fikce z žánru SF, i když to tak ze začátku nevypadá. Nedá se o tom moc říct, abych ti to nezkazila, ale myslím si, že to nějak odhadneš v první třetině sám, kam to míří.',
          en: '',
        },
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Ale žádné velké emoce, žádné hluboké poselství, spíš jako záhada k vyřešení a poměrně slušné vyústění.',
          en: '',
        },
      },
      { actorId: ActorId.right,
        text: {
          cz: 'To ti udělá dobře, uvidíš.',
          en: '',
        },
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Dobře, díky.',
          en: '',
        },
      },
      { actorId: ActorId.left,
        text: {
          cz: '[vzdych]',
          en: '',
        },
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Nicméně. Četla jsi dnešní oběžník?',
          en: '',
        },
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Že se z pátého ztratily ty tenepy, které nikdo neví co jsou zač?',
          en: '',
        },
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Přesně. Je to trochu prekérka. Mluvil jsem s analytiky od nás a jsou z toho celí nesví.',
          en: '',
        },
      },
      { actorId: ActorId.right,
        text: {
          cz: 'No, ani se nedivím. Nikdy není dobře, když nám tu jen tak poletuje nějaká pokročilá technologie.',
          en: '',
        },
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Máme tyhle věci hlídat a místo toho jsme nic nezjistili a ještě je ztratili.',
          en: '',
        },
      },
      { actorId: ActorId.left,
        text: {
          cz: '[kýv]',
          en: '',
        },
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Jsem zvědav, co se z toho vyvine.',
          en: '',
        },
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Doufejme, že nic.',
          en: '',
        },
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Hele, vyber si pauzu a dej se trochu dokupy. Já tu na to zatím dohlídnu.',
          en: '',
        },
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Díky. Hned jsem zpátky.',
          en: '',
        },
        actions: [ { do: ActionId.continueAnimation } ],
      },
      { actorId: ActorId.left,
        text: {
          cz: '',
          en: '',
        },
        actions: [ { do: ActionId.hide }, { do: ActionId.continueAnimation } ],
      },
      { actorId: ActorId.right,
        text: {
          cz: '[...]',
          en: '',
        },
        actions: [ { do: ActionId.continueAnimation } ],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Hej, vy tam. Nechte toho, nebo vás zastázuju a budete mít utrum.',
          en: '',
        },
      },
      { actorId: ActorId.right,
        text: {
          cz: '[mrač]',
          en: '',
        },
      },
      { actorId: ActorId.right,
        text: {
          cz: '[...]',
          en: '',
        },
      },
      { actorId: ActorId.right,
        text: {
          cz: 'No proto.',
          en: '',
        },
      },
      { actorId: ActorId.right,
        text: {
          cz: '[...]',
          en: '',
        },
        actions: [ { do: ActionId.continueAnimation } ],
      },
    ];
  }
}