import { GameObjects } from "phaser";
import { SoundPlayerBuilder } from "./sound-player";

export class BackgroundAnimationBuilder {
  soundPlayer: SoundPlayerBuilder | null = null;

  currentTweenIndex = 0;
  animationTweens = new Array<Phaser.Tweens.Tween>();
  activeTween: Phaser.Tweens.Tween | null = null;
  stopped = false;

  window: Phaser.GameObjects.Image | null = null;
  robotSprite: Phaser.GameObjects.Sprite | null = null;

  preloadAssets(scene: Phaser.Scene) {
    scene.load.image('window', 'images/window.png');
    scene.load.spritesheet('robot-idle', 'images/robot-idle.png', { frameWidth: 192, frameHeight: 180 });
    scene.load.spritesheet('robot-powershot', 'images/robot-powershot.png', { frameWidth: 685, frameHeight: 192 });
  }

  createObjects(scene: Phaser.Scene, soundPlayer: SoundPlayerBuilder) {
    this.soundPlayer = soundPlayer;

    const speedModifier = 1;

    const actorsY = 380;
    const robotFiringPosition = 700;

    this.window = scene.add.image(0, 0, 'window').setOrigin(0, 0).setDepth(-2);

    this.robotSprite = scene.add.sprite(robotFiringPosition, actorsY, 'robot').setScale(0.4).setOrigin(0.5, 1).setDepth(-5).setVisible(false);
    scene.anims.create({
      key: 'robot-idle',
      frames: scene.anims.generateFrameNames('robot-idle', { start: 0, end: 11 }),
      frameRate: 15,
      repeat: -1,
    });
    scene.anims.create({
      key: 'robot-powershot',
      frames: scene.anims.generateFrameNames('robot-powershot', { start: 0, end: 17 }),
      frameRate: 15,
    });

    // windows flashes
    for(let i = 0; i < 4; i++) {
      this.animationTweens.push(scene.add.tween({
        paused: true,
        targets: this.window,
        alpha: 1,
        duration: speedModifier * 250,
        onStart: () => {
          this.soundPlayer?.robotPowerShotSound?.play();
          this.window?.setTint(0xff0000);
        },
        onComplete: () => {
          this.window?.clearTint();
        },
      }));
    }

    // window slides up
    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.window,
      y: -500,
      duration: speedModifier * 1000,
      onStart: () => {
        this.soundPlayer?.windowSlide?.play();
        this.robotSprite?.setVisible(true);
        this.robotSprite?.play('robot-idle');
      },
    }));

    // robot shoots
    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.robotSprite,
      x: robotFiringPosition + 0.4 * 200,
      delay: speedModifier * 200,
      duration: speedModifier * 1000,
      onStart: () => {
        this.soundPlayer?.robotPowerShotSound?.play();
        this.robotSprite?.setX(robotFiringPosition + 0.4 * 200);
        this.robotSprite?.setVisible(true);
        this.robotSprite?.play('robot-powershot');
      },
      onComplete: () => {
        this.robotSprite?.setX(robotFiringPosition);
        this.robotSprite?.play('robot-idle');
      },
    }));

    // window slides down
    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.window,
      y: 0,
      duration: speedModifier * 1000,
      onStart: () => {
        this.soundPlayer?.windowSlide?.play();
      },
      onComplete: () => {
        this.robotSprite?.setVisible(false);
      }
    }));
  }

  startAnimation(scene: Phaser.Scene) {
    this.stopped = false;
    this.currentTweenIndex = 0;
    this.animationTweens.forEach(t => t.setTimeScale(1));

    this.playNext();
  }


  stopAnimation() {
    this.stopped = true;
    this.animationTweens.forEach(tween => {
      tween.stop();
      tween.setTimeScale(1);
    });
  }

  playNext() {
    if (this.stopped) {
      return;
    }

    if (this.currentTweenIndex < this.animationTweens.length) {
      this.activeTween = this.animationTweens[this.currentTweenIndex++];
      this.activeTween?.play();
    }
  }

  isAnimationPlaying() {
    return this.activeTween?.isPlaying() ?? false;
  }

  skip() {
    if (this.stopped) {
      return;
    }

    this.activeTween?.setTimeScale(100);
  }

}