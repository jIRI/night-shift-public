import { GameObjects } from "phaser";

export class Sounds {
  robotPowerShotSound: Phaser.Sound.BaseSound | null = null;
  windowSlide: Phaser.Sound.BaseSound | null = null;
}

export type AvailableSounds = keyof Sounds;

export class SoundPlayerBuilder extends Sounds{
  preloadAssets(scene: Phaser.Scene) {
    scene.load.audio('robot-powershot-sound', 'audio/robot-powershot.ogg');
    scene.load.audio('window-slide-sound', 'audio/window-slide.ogg');
  }

  createObjects(scene: Phaser.Scene) {
    this.robotPowerShotSound = scene.sound.add('robot-powershot-sound', {
      volume: 0.05,
      rate: 2,
    });
    this.windowSlide = scene.sound.add('window-slide-sound', {
      volume: 0.05,
      rate: 2,
    });
   }

   play(key: AvailableSounds) {
    switch(key) {
      case "robotPowerShotSound":
        this.robotPowerShotSound?.play();
        return;
      case "windowSlide":
        this.windowSlide?.play();
        return;
      default:
        return;
    }
   }
}