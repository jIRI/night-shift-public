import { AvailableSounds } from "../builders/sound-player";
import { LocalizedText } from "../types/lang";

export enum ActorId {
  left,
  right,
  robot,
  drone,
  dummy,
}

export enum ActionId {
  playAnimation,
  continueAnimation,
  finishAnimation,
  show,
  hide,
  enableAnimationSync,
  disableAnimationSync,
  playSound,
}

export type ActionConfig = {
  do: ActionId.playAnimation
  | ActionId.continueAnimation
  | ActionId.finishAnimation
  | ActionId.show
  | ActionId.hide
  | ActionId.enableAnimationSync
  | ActionId.disableAnimationSync,
} | {
  do: ActionId.playSound,
  sound: AvailableSounds
};

export type ActorConfig = {
  id: ActorId;
  name: string;
  textEncoder?: (text:LocalizedText) => LocalizedText;
};

export type SelectItem = {
  text: string;
  gotoLabel: string;
};

export type Line = {
  actorId: ActorId;
  text: LocalizedText;
  visited?: boolean;
  actions?: Array<ActionConfig>;
  label?: string;
  select?: Array<SelectItem>;
}

export function getLocalizedText(localizedText: LocalizedText, lang: keyof LocalizedText): string {
  return localizedText[lang];
}

const telepathicEncoderReplacers: Array<(s: string) => string> = [
  s => s.replace(/[AÁ]/g, 'GR'),
  s => s.replace(/[EÉĚ]/g, 'RR'),
  s => s.replace(/[IÍ]/g, 'QR'),
  s => s.replace(/[OÓ]/g, 'RX'),
  s => s.replace(/[UÚŮ]/g, 'WR'),
  s => s.replace(/[YÝ]/g, 'TQ'),
  s => s.replace(/[ŠČŘŽĎŇ]/g, 'WT'),
];

export function applyReplacers(text: string, replacers: Array<(s: string) => string>): string {
  let result = text.toUpperCase();
  for(const replacer of replacers) {
    result = replacer(result);
  }
  return result;
}

export class Script {
  currentLineIndex = 0;
  lines: Array<Line>;
  actorMap = new Map<ActorId, ActorConfig>();

  constructor() {
    this.lines = Script.lines();
    Script.actors().forEach(actor => this.actorMap.set(actor.id, actor));
  }

  actor(id: ActorId): ActorConfig {
    return this.actorMap.get(id) ?? Script.actors()[0];
  }

  reset() {
    this.currentLineIndex = -1;
    this.lines.forEach(line => line.visited = false);
  }

  nextLine(input?: string): Line | null {
    if (this.currentLineIndex < 0) {
      // first invocation, just return first line
      this.currentLineIndex = 0;
    } else {
      if (input != null ) {
        // we got input, try find index of the label
        const nextLineIndex = this.lines.findIndex(line => line.label === input);
        if (nextLineIndex >= 0) {
          // label found, jump to label
          this.currentLineIndex = nextLineIndex;
        } else {
          // label not found, well, continue on next line...
          this.currentLineIndex++;
        }
      } else {
        // no input, just continue on next line
        this.currentLineIndex++;
      }
    }

    if (this.currentLineIndex >= this.lines.length) {
      return null;
    }

    this.lines[this.currentLineIndex].visited = true;
    const line = Object.assign({}, this.lines[this.currentLineIndex]);
    const textEncoder = this.actor(line.actorId).textEncoder;
    if( textEncoder != null) {
      line.text = textEncoder(line.text);
    }
    return line;
  }

  //
  // Actors and script definition follows
  //

  static actors(): Array<ActorConfig>  {
    return [
      { id: ActorId.left, name: 'Yoraggoraxuros' },
      { id: ActorId.right, name: 'Choraqorax' },
    ];
  }

  static telepathicEncoder(text: LocalizedText): LocalizedText {
    if(text.cz.startsWith('[') || text.en.startsWith('[')) {
      return text;
    }

    let cz = applyReplacers(`<${text.cz}>`, telepathicEncoderReplacers);
    let en = applyReplacers(`<${text.en}>`, telepathicEncoderReplacers);

    return {
      cz,
      en,
    };
  }

  static droidEncoder(text: LocalizedText): LocalizedText {
    if(text.cz.startsWith('[') || text.en.startsWith('[')) {
      return text;
    }

    let cz = `{ ${text.cz} }`;
    let en = `{ ${text.en} }`;

    return {
      cz,
      en,
    };
  }

  static lines(): Array<Line> {
    return [
      { actorId: ActorId.left,
        text: {
          cz: 'Takže: co se to, sakra, zase děje?',
          en: '',
        },
        actions: [  ],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'No, to je velmi dobrá otázka.',
          en: '',
        },
        actions: [  ],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'V podstatě jedna z těch civilizací, která asi tak pět cyklů zpátky objevila PE-brány, není úplně spokojená s tím, jak se věci mají.',
          en: '',
        },
        actions: [  ],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Ah. A kontaktovali jsme je?',
          en: '',
        },
        actions: [  ],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Samozřejmě. Prošli vstupním procesem, seznámením s chartou a tak dále. A byly to tehdy dlouhé diskuse o tom, jak se celá věc vede z naší strany, a pořád se odmítali smířit s naší regulací a tak dále.',
          en: '',
        },
        actions: [  ],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Nakonec to vypadalo, že jsme došli ke konsensu, k chartě se připojili, podepsali, ratifikovali. Ale jak se zdá, buď si to rozmysleli, nebo neměli dodržování pravidel nikdy doopravdy v úmyslu.',
          en: '',
        },
        actions: [  ],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Eh. A to jsme u nich nenechali nějaký dozor?',
          en: '',
        },
        actions: [  ],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Nechali, nechali. Ale něco nám muselo proklouznout. Dokázali dát dohromady tuhle otravnou mikro-invazi, pomocí dost hrubé síly se probránovali a teď tu pobíhají po baráku a dělají potíže.',
          en: '',
        },
        actions: [  ],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Chm. To musí být pořádné potíže, když povolali od stolu i takové hlavouny jako jsi ty.',
          en: '',
        },
        actions: [  ],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Ha, ha. Hlavouny, to je dobré. ',
          en: '',
        },
        actions: [  ],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Ale ano, jelikož se to, jako obvykle, muselo stát noční směně, jsme malinko omezení co se týče personálu. Takže než dorazí lidi z denní směny co mají pohotovost, pomáhám i já.',
          en: '',
        },
        actions: [  ],
      },
      { actorId: ActorId.right,
        text: {
          cz: '[...]',
          en: '',
        },
        actions: [  ],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Nicméně, když už jsem tady, chtěl jsem se zeptat --',
          en: '',
        },
        actions: [ { do: ActionId.enableAnimationSync }, { do: ActionId.playAnimation }  ],
      },
      { actorId: ActorId.right,
        text: {
          cz: '-- eh, momentíček.',
          en: '',
        },
        actions: [  ],
      },
      { actorId: ActorId.right,
        text: {
          cz: '',
          en: '',
        },
        actions: [  { do: ActionId.hide }, { do: ActionId.continueAnimation } ],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Jsem zpět.',
          en: '',
        },
        actions: [ { do: ActionId.show } ],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Co jsem to... Aha, ano.',
          en: '',
        },
        actions: [  ],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Takže, když už jsem tady u vás v archívu, neměl bys pro mě nějakou pěknou fikci ze Země?',
          en: '',
        },
        actions: [  ],
      },
      { actorId: ActorId.left,
        text: {
          cz: '[???]',
          en: '',
        },
        actions: [  ],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'C-c-co? Fikci? Jako že bych ti dal nějakou regulovanou literaturu?',
          en: '',
        },
        actions: [  ],
      },
      { actorId: ActorId.right,
        text: {
          cz: "No jasně. Přečetl jsem teď dvě takové publikace odtamtud. Jednak The Quick Fix: Why Fad Psychology Can't Cure Our Social Ills a pak Black Hole Blues and Other Songs from Outer Space.",
          en: '',
        },
        actions: [  ],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'To první je trochu suchárna o tom, jak u nich mají sociální psychologii a podobné vědy, a jak i ten nejpochybnější výsledek nejslabších experimentů dokáží prodávat jako cestu ke spasení celé planety.',
          en: '',
        },
        actions: [  ],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Jak říkám, je to trochu suché čtení, ale je to docela poučné.',
          en: '',
        },
        actions: [  ],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'To ta druhá kniha, ta je mnohem zajímavější. Popisuje jak stavěli detektory gravitačních vln a jak dlouho to trvalo, a jaké to mělo problémy a v epilogu nakonec ty vlny i zachytí.',
          en: '',
        },
        actions: [  ],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Kdybych nevěděl, že je to popis skutečných událostí, úplně bych věřil tomu, že je to fikce, jak je to zajímavě napsané.',
          en: '',
        },
        actions: [  ],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Takže jsem si říkal, že bys mi mohl půjčit nějakou nenáročnou literaturu, něco lehkého, možná trochu té fantastické vědeckosti.',
          en: '',
        },
        actions: [  ],
      },
      { actorId: ActorId.left,
        text: {
          cz: '[!!!]',
          en: '',
        },
        actions: [  ],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Ale no tak, nehraj to na mě. Vždyť my na vnitřním víme, že z archívu pouštíte pozemskou fikci do oběhu.',
          en: '',
        },
        actions: [  ],
      },
      { actorId: ActorId.left,
        text: {
          cz: '[??????]',
          en: '',
        },
        actions: [  ],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Podívej, zatím je názor takový, že je to jedna z těch vln, co se občas objevují.',
          en: '',
        },
        actions: [  ],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Jako před dvanácti cykly Treploxiranská telepatická synkopace. Na tom tehdy taky ujížděl celý barák, a za jeden dva cykly po tom nikdo ani nevzdechnul.',
          en: '',
        },
        actions: [  ],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Takže to monitorujeme a dokud se kontaminace udržuje uvnitř úřadu a neovlivňuje to nějak zásadně pracovní výkon, bereme to jako zaměstnanecký benefit, nebo tak něco.',
          en: '',
        },
        actions: [  ],
      },
      { actorId: ActorId.left,
        text: {
          cz: '[polk]',
          en: '',
        },
        actions: [  ],
      },
      { actorId: ActorId.left,
        text: {
          cz: '[chraply chraply]',
          en: '',
        },
        actions: [  ],
      },
      { actorId: ActorId.left,
        text: {
          cz: '[POLK]',
          en: '',
        },
        actions: [  ],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Ehm. Tedy pro začátečníky se zájmem o vědeckou fantastiku bych mohl doporučit třeba knihy od Andyho Weira: Martian, Artemis a Project Hail Marry.',
          en: '',
        },
        actions: [  ],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Ehe, ehe. A o čem to je?',
          en: '',
        },
        actions: [  ],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'V zásadě je to jedna a ta samá kniha třikrát jinak. Hlavní postava je sympatická, dobře naložená inženýrka (s i bez oficiálního vzdělání) nebo vědec, který srší vtipem a tak dále.',
          en: '',
        },
        actions: [  ],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Taková postava se dostane do nějaké obtížné situace, jako například že zůstane opuštěná na jiné planetě, nebo se zaplete do nějakých podivných machinací, nebo zůstane opuštěná uprostřed vesmíru v kosmické lodi.',
          en: '',
        },
        actions: [  ],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'No a pak se pomocí svého nezměrného tvůrčího génia vyvědá a vyinženýruje z té šlamastyky ven za častého troušení vtipných poznámek.',
          en: '',
        },
        actions: [  ],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Mhm, zatím to zní zajímavě.',
          en: '',
        },
        actions: [  ],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Takže popořadě: Martian je skvělý. V rámci toho, co dělá je to skoro bezchybná kniha na prvotinu. Hrdina nemá moc osobnosti, ale je to talent na všechno, neztrácí optimismus, pořád jde neohroženě kupředu.',
          en: '',
        },
        actions: [  ],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Prostě skvělá zábava.',
          en: '',
        },
        actions: [  ],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Proto je potřeba být připravený na to, že Artemis je z těch tří knih jednoznačně nejslabší, místy je to až trochu otrava. Já osobně jsem ve třetině musel přečíst jinou knihu, abych si od toho odpočinul.',
          en: '',
        },
        actions: [  ],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Problém vidím jednak v tom, že psát něco jako rafinovanou loupež ve vesmíru není Weirova parketa, dvak hlavní postava působí ne jako skutečná osoba, ale spíš jako jak si šprt ze střední školy představuje, že mluví zábavné chytré holky co mají všichni rádi.',
          en: '',
        },
        actions: [  ],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Taky jsou tam občas věci, které vypadají, že k něčemu povedou a nakonec prostě vyšumí do prázdna, a konečně celá zápletka je hodně přitažená za vlasy.',
          en: '',
        },
        actions: [  ],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Uh, když tě tak poslouchám, má to vůbec cenu číst?',
          en: '',
        },
        actions: [  ],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Záleží asi na vkusu. Zkusil bych tak první třetinu, pokud tě to nebude bavit, je velká pravděpodobnost, že budeš na konci celkově zklamaný. Pokud bude, užiješ si to celé.',
          en: '',
        },
        actions: [  ],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'A ta třetí kniha?',
          en: '',
        },
        actions: [  ],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Project Hail Mary. Stejné schéma jako Martian -- vědátor opuštěný ve vesmíru, musí se vlastní chytrostí dostat z různých zapeklitých situací a tak dále.',
          en: '',
        },
        actions: [  ],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Svižné, zábavné čtení, občas nějaké to překvapení, hodně ironických poznámek. Skvělá začátečnická četba.',
          en: '',
        },
        actions: [  ],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Postava sice nemá skoro žádnou osobnost, spíš je to taková nádoba na náhodná vědecká a technická fakta, ale nevadí to, protože skoro pořád se něco děje a je to zajímavé číst.',
          en: '',
        },
        actions: [  ],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Skvělé.',
          en: '',
        },
        actions: [  ],
      },
      { actorId: ActorId.right,
        text: {
          cz: '[...]',
          en: '',
        },
        actions: [  ],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Tak jak se to dělá?',
          en: '',
        },
        actions: [  ],
      },
      { actorId: ActorId.left,
        text: {
          cz: '[...]',
          en: '',
        },
        actions: [  ],
      },
      { actorId: ActorId.left,
        text: {
          cz: '[šoup?]',
          en: '',
        },
        actions: [  ],
      },
      { actorId: ActorId.right,
        text: {
          cz: '[chmát!]',
          en: '',
        },
        actions: [  ],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Výborně. Nicméně. Tohle neznamená, že máte oficiální povolení úplně zdivočet. Držte to na uzdě, protože pokud by se nám tu kontaminace rozjela ve velkém, budeme muset začít vyšetřovat a tak dále. A to bychom nikdo nechtěli, že ne?',
          en: '',
        },
        actions: [  ],
      },
      { actorId: ActorId.left,
        text: {
          cz: '[polk]',
          en: '',
        },
        actions: [  ],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Já jsem ti to myslel.',
          en: '',
        },
        actions: [  ],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Tak to bude všechno. Myslím, že tady u vás už máme čisto, takže se přesunu dál.',
          en: '',
        },
        actions: [  ],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Měj se!',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.left,
        text: {
          cz: '[máv]',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.right,
        text: {
          cz: '',
          en: '',
        },
        actions: [ { do: ActionId.hide } ],
      },
      { actorId: ActorId.left,
        text: {
          cz: '[polk]',
          en: '',
        },
        actions: [  ],
      },
    ];
  }
}