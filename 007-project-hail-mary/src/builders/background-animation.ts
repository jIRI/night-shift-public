import { SoundPlayerBuilder } from "./sound-player";

export class BackgroundAnimationBuilder {
  soundPlayer: SoundPlayerBuilder | null = null;

  currentTweenIndex = 0;
  animationTweens = new Array<Phaser.Tweens.Tween>();
  activeTween: Phaser.Tweens.Tween | null = null;
  stopped = false;

  warrior: Phaser.GameObjects.Image | null = null;
  warriorSpellSprite: Phaser.GameObjects.Sprite | null = null;

  swordsmanSprite: Phaser.GameObjects.Sprite | null = null;
  swordsmanAttackSprite: Phaser.GameObjects.Sprite | null = null;

  preloadAssets(scene: Phaser.Scene) {
    scene.load.image('warrior', 'images/right.png');
    scene.load.image('beam', 'images/beam.png');

    scene.load.spritesheet('swordsman-idle', 'images/swordsman-idle.png', { frameWidth: 134.25, frameHeight: 133 });
    scene.load.spritesheet('swordsman-walk', 'images/swordsman-walk.png', { frameWidth: 135.5, frameHeight: 137 });
    scene.load.spritesheet('swordsman-attack', 'images/swordsman-attack.png', { frameWidth: 228.5, frameHeight: 128 });
    scene.load.spritesheet('swordsman-hurt', 'images/swordsman-hurt.png', { frameWidth: 154.5, frameHeight: 122 });

    scene.load.spritesheet('spell-swordsman-attack', 'images/spell-swordsman-attack.png', { frameWidth: 192, frameHeight: 192 });
    scene.load.spritesheet('spell-warrior', 'images/spell-warrior.png', { frameWidth: 192, frameHeight: 192 });
  }

  createObjects(scene: Phaser.Scene, soundPlayer: SoundPlayerBuilder) {
    this.soundPlayer = soundPlayer;

    const tintColor = 0xa0a0a0;

    const speedModifier = 1;

    const swordsmanY = 368;
    const swordsmanX = 630;
    const swordsmanAttackX = 670;

    const warriorY = swordsmanY + 18;
    const warriorX = 950;

    this.warrior = scene.add.image(warriorX + 200, warriorY, 'warrior').setScale(0.22, 0.22).setOrigin(0.5, 1).setDepth(-5).setTint(tintColor).setVisible(true);
    scene.add.image(scene.game.scale.width / 2 + 6, swordsmanY - 11, 'beam').setOrigin(0, 1).setDepth(-2).setAlpha(0.9).setVisible(true);

    // warrior's vertical wobble
    scene.add.tween({
      targets: this.warrior,
      y: { from: warriorY - 1, to: warriorY + 1 },
      ease: Phaser.Math.Easing.Sine.InOut,
      repeat: -1,
      duration: 1500,
      yoyo: true,
    });

    this.swordsmanSprite = scene.add.sprite(-50, swordsmanY, 'swordsman').setScale(0.6, 0.6).setOrigin(0.5, 1).setDepth(-5).setTint(tintColor).setVisible(true);
    scene.anims.create({
      key: 'swordsman-idle',
      frames: scene.anims.generateFrameNames('swordsman-idle', { start: 0, end: 11 }),
      frameRate: 15,
      repeat: -1,
    });
    scene.anims.create({
      key: 'swordsman-walk',
      frames: scene.anims.generateFrameNames('swordsman-walk', { start: 0, end: 15 }),
      frameRate: 15,
      repeat: -1,
    });
    scene.anims.create({
      key: 'swordsman-attack',
      frames: scene.anims.generateFrameNames('swordsman-attack', { start: 0, end: 11 }),
      frameRate: 15,
      repeat: 1,
    });
    scene.anims.create({
      key: 'swordsman-hurt',
      frames: scene.anims.generateFrameNames('swordsman-hurt', { start: 0, end: 14 }),
      frameRate: 15,
      repeat: -1,
    });

    this.swordsmanAttackSprite = scene.add.sprite(scene.game.scale.width / 2 + 12, swordsmanY - 50, 'spell-swordsman-attack').setScale(1, 1).setOrigin(0.5, 0.5).setDepth(-1).setVisible(false);
    scene.anims.create({
      key: 'spell-swordsman-attack',
      frames: scene.anims.generateFrameNames('spell-swordsman-attack', { start: 0, end: 34 }),
      frameRate: 30,
      repeat: 1,
    });

    this.warriorSpellSprite = scene.add.sprite(swordsmanX, swordsmanY - 50, 'spell-warrior').setScale(1.5, 1.5).setOrigin(0.5, 0.5).setDepth(-1).setVisible(false);
    scene.anims.create({
      key: 'spell-warrior',
      frames: scene.anims.generateFrameNames('spell-warrior', { start: 0, end: 60 }),
      frameRate: 30,
      repeat: 1,
    });

    // start
    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.swordsmanSprite,
      x: { from: swordsmanX - 300, to : swordsmanX },
      duration: speedModifier * 1500,
      onStart: () => {
        this.swordsmanSprite?.play('swordsman-walk');
      },
      onComplete: () => {
        this.swordsmanSprite?.play('swordsman-idle');
        this.playNext();
      },
    }));

    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.swordsmanSprite,
      y: swordsmanY,
      duration: speedModifier * 800,
      delay: speedModifier * 800,
      onStart: () => {
        this.swordsmanSprite?.setX(swordsmanAttackX);
        this.swordsmanSprite?.play('swordsman-attack');
        this.playNext();
      },
      onComplete: () => {
      },
    }));

    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.swordsmanSprite,
      y: swordsmanY,
      duration: speedModifier * 1000,
      delay:  speedModifier * 400,
      onStart: () => {
        this.swordsmanAttackSprite?.setVisible(true);
        this.swordsmanAttackSprite?.play('spell-swordsman-attack');
        this.soundPlayer?.play('swordsmanAttack');
        this.playNext();
      },
      onComplete: () => {
        this.swordsmanAttackSprite?.setVisible(false);
      },
    }));

    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.swordsmanSprite,
      x: swordsmanX - 20,
      duration: speedModifier * 100,
      onStart: () => {
      },
      onComplete: () => {
        this.swordsmanSprite?.play('swordsman-hurt');
      },
    }));

    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.warrior,
      x: warriorX,
      duration: speedModifier * 600,
      onStart: () => {
      },
      onComplete: () => {
        this.playNext();
      },
    }));

    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.swordsmanSprite,
      alpha: 0,
      duration: speedModifier * 1000,
      delay: 1000,
      onStart: () => {
        this.warriorSpellSprite?.setVisible(true);
        this.warriorSpellSprite?.play('spell-warrior');
        this.soundPlayer?.play('warriorSpell');
      },
      onComplete: () => {
        this.warriorSpellSprite?.setVisible(false);

        this.playNext();
      },
    }));

    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.warrior,
      x: warriorX + 200,
      duration: speedModifier * 600,
      delay:  speedModifier * 1000,
      onStart: () => {
      },
      onComplete: () => {
      },
    }));


  }

  startAnimation(scene: Phaser.Scene) {
    this.stopped = false;
    this.currentTweenIndex = 0;
    this.animationTweens.forEach(t => t.setTimeScale(1));

    this.playNext();
  }


  stopAnimation() {
    this.stopped = true;
    this.animationTweens.forEach(tween => {
      tween.stop();
      tween.setTimeScale(1);
    });
  }

  playNext() {
    if (this.stopped) {
      return;
    }

    if (this.currentTweenIndex < this.animationTweens.length) {
      this.activeTween = this.animationTweens[this.currentTweenIndex++];
      this.activeTween?.play();
    }
  }

  isAnimationPlaying() {
    return this.activeTween?.isPlaying() ?? false;
  }

  skip() {
    if (this.stopped) {
      return;
    }

    this.activeTween?.setTimeScale(100);
  }

}