import { LanguageCode, LanguageList, LanguageMap, LanguageRegistryKey } from "../types/lang";

export class LanguageSelectorBuilder {
  selectedLanguage: LanguageCode = LanguageCode.cz;
  selectedLanguageIndex = 0;
  buttons = new Map<LanguageCode, Phaser.GameObjects.Text>();
  ignoreInput = false;
  baseColor = '#14EAE8';

  public preloadAssets(scene: Phaser.Scene) {
  }

  public createObjects(scene: Phaser.Scene) {
    const buttonWidth = 0.2 * scene.scale.width;
    const buttonX = 0.50 * scene.scale.width;
    const buttonHeight = 0.06 * scene.scale.height;
    const buttonDistance = 0.07 * scene.scale.height;
    let buttonY = 0.75 * scene.scale.height;


    for(const langCode of LanguageList) {
      const langString = LanguageMap.get(langCode);
      const button = scene.add.text(buttonX, buttonY, langString, {
        fixedWidth: buttonWidth,
        fixedHeight: buttonHeight,
        align: 'center',
        fontFamily: 'IntroFont',
        fontSize: '5em',
        color: this.baseColor,
      }).setOrigin(0.5, 0.5).setInteractive().setData(LanguageRegistryKey, langCode);
      this.buttons.set(langCode, button);
      buttonY += buttonDistance;
    }

    this.refreshSelection();
    this.registerInputHandlers(scene);
  }

  private registerInputHandlers(scene: Phaser.Scene) {
    this.ignoreInput = scene.game.scene.isActive('IntroScene');
    scene.input.on('gameobjectdown', (pointer: any, object: Phaser.GameObjects.Text) => {
      if (this.ignoreInput) {
        return;
      }
      this.selectedLanguage = object.getData(LanguageRegistryKey);
      scene.game.registry.set(LanguageRegistryKey, this.selectedLanguage);
      this.refreshSelection();
      this.ignoreInput = true;
    });

    scene.input.keyboard.on('keydown', (event: any) => {
      if (this.ignoreInput) {
        return;
      }

      switch (event.keyCode) {
        case Phaser.Input.Keyboard.KeyCodes.UP:
          if( this.selectedLanguageIndex > 0 ) {
            this.selectedLanguageIndex--;
          }
          break;
        case Phaser.Input.Keyboard.KeyCodes.DOWN:
          if( this.selectedLanguageIndex < LanguageList.length - 1 ) {
            this.selectedLanguageIndex++;
          }
          break;
        default:
          break;
      }

      this.selectedLanguage = LanguageList[this.selectedLanguageIndex];
      scene.game.registry.set(LanguageRegistryKey, this.selectedLanguage);
      this.refreshSelection();
    });
  }

  private refreshSelection() {
    LanguageList.forEach(langCode => {
      if( langCode === this.selectedLanguage ) {
        this.buttons.get(langCode)?.setFill('red').setScale(1.2);
      } else {
        this.buttons.get(langCode)?.setFill(this.baseColor).setScale(1);
      }
    });
  }
}