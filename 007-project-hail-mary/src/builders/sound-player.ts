export class Sounds {
  swordsmanAttack: Phaser.Sound.BaseSound | null = null;
  warriorSpell: Phaser.Sound.BaseSound | null = null;
}

export type AvailableSounds = keyof Sounds;

export class SoundPlayerBuilder extends Sounds {
  preloadAssets(scene: Phaser.Scene) {
    scene.load.audio('swordsman-attack-sound', 'audio/swordsman-attack.ogg');
    scene.load.audio('warrior-spell-sound', 'audio/warrior-spell.mp3');
  }

  createObjects(scene: Phaser.Scene) {
    this.swordsmanAttack = scene.sound.add('swordsman-attack-sound', {
      volume: 0.05,
      rate: 1,
    });
    this.warriorSpell = scene.sound.add('warrior-spell-sound', {
      volume: 0.05,
      rate: 1,
    });
  }

  play(key: AvailableSounds) {
    switch(key) {
      case 'swordsmanAttack':
        this.swordsmanAttack?.play();
        return;
      case 'warriorSpell':
        this.warriorSpell?.play();
        return;
      default:
        return;
    }
   }
}