import { GameObjects } from "phaser";

export class MainAnimationBuilder {
  currentTweenIndex = 0;
  animationTweens = new Array<Phaser.Tweens.Tween>();
  activeTween: Phaser.Tweens.Tween | null = null;
  stopped = false;

  spellSprites: Array<Phaser.GameObjects.Sprite> = [];
  spellSound: Phaser.Sound.BaseSound | null = null;

  suitcase: Phaser.GameObjects.Image | null = null;
  suitcaseSound: Phaser.Sound.BaseSound | null = null;

  preloadAssets(scene: Phaser.Scene) {
    scene.load.image('suitcase', 'images/suitcase.png');

    scene.load.spritesheet('spell-01', 'images/spell-01.png', { frameWidth: 192, frameHeight: 192 });
    scene.load.spritesheet('spell-02', 'images/spell-02.png', { frameWidth: 192, frameHeight: 192 });
    scene.load.spritesheet('spell-03', 'images/spell-03.png', { frameWidth: 192, frameHeight: 192 });
    scene.load.spritesheet('spell-04', 'images/spell-04.png', { frameWidth: 192, frameHeight: 192 });
    scene.load.spritesheet('spell-05', 'images/spell-05.png', { frameWidth: 192, frameHeight: 192 });
    scene.load.spritesheet('spell-06', 'images/spell-06.png', { frameWidth: 192, frameHeight: 192 });
    scene.load.spritesheet('spell-07', 'images/spell-07.png', { frameWidth: 192, frameHeight: 192 });
    scene.load.spritesheet('spell-08', 'images/spell-08.png', { frameWidth: 192, frameHeight: 192 });
    scene.load.spritesheet('spell-09', 'images/spell-09.png', { frameWidth: 192, frameHeight: 192 });
    scene.load.spritesheet('spell-10', 'images/spell-10.png', { frameWidth: 192, frameHeight: 192 });
    scene.load.spritesheet('spell-11', 'images/spell-11.png', { frameWidth: 192, frameHeight: 192 });

    scene.load.audio('suitcase-sound', 'audio/suitcase.mp3');
    scene.load.audio('spell-sound', 'audio/spell.mp3');
  }

  createObjects(scene: Phaser.Scene) {
    const speedModifier = 1;

    const suitcaseX = scene.scale.width / 2;
    const suitcaseY = 0.52  * scene.scale.height;

    this.suitcase = scene.add.image(suitcaseX, suitcaseY, 'suitcase').setOrigin(0.5, 1).setScale(0.35, 0.35).setTint(0xa0a0a0).setDepth(10).setAlpha(0);

    const createAnim = (index: number, frameCount: number) => {
      const indexString = ('0' + index).slice(-2);
      scene.anims.create({
        key: `spell-${indexString}`,
        frames: scene.anims.generateFrameNames(`spell-${indexString}`, { start: 0, end: frameCount }),
        frameRate: 30,
      });
    };

    this.spellSprites.push(scene.add.sprite(suitcaseX, suitcaseY, 'spell-01').setData('anim', 'spell-01').setScale(1).setOrigin(0.5, 0.5).setVisible(false));
    createAnim(1, 29);
    this.spellSprites.push(scene.add.sprite(suitcaseX, suitcaseY, 'spell-02').setData('anim', 'spell-02').setScale(1).setOrigin(0.5, 0.68).setVisible(false));
    createAnim(2, 14);
    this.spellSprites.push(scene.add.sprite(suitcaseX, suitcaseY, 'spell-03').setData('anim', 'spell-03').setScale(1).setOrigin(0.5, 0.68).setVisible(false));
    createAnim(3, 29);
    this.spellSprites.push(scene.add.sprite(suitcaseX, suitcaseY, 'spell-04').setData('anim', 'spell-04').setScale(1).setOrigin(0.5, 0.68).setVisible(false));
    createAnim(4, 24);
    this.spellSprites.push(scene.add.sprite(suitcaseX, suitcaseY, 'spell-05').setData('anim', 'spell-05').setScale(1).setOrigin(0.5, 0.68).setVisible(false));
    createAnim(5, 14);
    this.spellSprites.push(scene.add.sprite(suitcaseX, suitcaseY, 'spell-06').setData('anim', 'spell-06').setScale(1).setOrigin(0.5, 0.68).setVisible(false));
    createAnim(6, 29);
    this.spellSprites.push(scene.add.sprite(suitcaseX, suitcaseY, 'spell-07').setData('anim', 'spell-07').setScale(1).setOrigin(0.5, 0.68).setVisible(false));
    createAnim(7, 24);
    this.spellSprites.push(scene.add.sprite(suitcaseX, suitcaseY, 'spell-08').setData('anim', 'spell-08').setScale(1).setOrigin(0.5, 0.68).setVisible(false));
    createAnim(8, 14);
    this.spellSprites.push(scene.add.sprite(suitcaseX, suitcaseY, 'spell-09').setData('anim', 'spell-09').setScale(1).setOrigin(0.5, 0.68).setVisible(false));
    createAnim(9, 29);
    this.spellSprites.push(scene.add.sprite(suitcaseX, suitcaseY, 'spell-10').setData('anim', 'spell-10').setScale(1).setOrigin(0.5, 0.68).setVisible(false));
    createAnim(10, 24);
    this.spellSprites.push(scene.add.sprite(suitcaseX, suitcaseY, 'spell-11').setData('anim', 'spell-11').setScale(1).setOrigin(0.5, 0.68).setVisible(false));
    createAnim(11, 14);

    // sounds
    this.suitcaseSound = scene.sound.add('suitcase-sound', {
      volume: 0.2
    });
    this.spellSound = scene.sound.add('spell-sound', {
      volume: 0.07
    });


    // spell sequence
    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.suitcase,
      alpha: 1,
      duration: speedModifier * 100,
    }));

    this.animationTweens.push(...this.spellSprites.flatMap(sprite => [
      scene.add.tween({
        paused: true,
        targets: sprite,
        alpha: 1,
        duration: 1,
        onStart: () => {
          this.spellSound!.play();
        },
        onComplete: () => {
          this.playNext();
        },
      }),
      scene.add.tween({
        paused: true,
        targets: sprite,
        alpha: 1,
        delay: 300,
        duration: speedModifier * 500,
        onStart: () => {
          sprite?.setVisible(true);
          this.suitcase?.setAlpha(0.6);
          sprite?.play(sprite.getData('anim'));
        },
        onComplete: () => {
          this.suitcase?.setAlpha(1);
          sprite?.setVisible(false);
        },
      }),
    ]));

    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.suitcase,
      scale: { from: 0.4, to: 0.5 },
      ease: Phaser.Math.Easing.Sine.InOut,
      repeat: 12,
      duration: 55,
      yoyo: true,
      onStart: () => {
        this.suitcaseSound!.play();
        this.suitcase?.setTint(0x500000);
      },
      onComplete: () => {
        this.suitcase?.clearTint();
        scene.cameras.main.fadeOut(1000, 0xff, 0xff, 0xff);
      },
    }));
  }

  startAnimation(scene: Phaser.Scene) {
    this.stopped = false;
    this.currentTweenIndex = 0;
    this.suitcase?.setAlpha(0);
    this.animationTweens.forEach(t => t.setTimeScale(1));

    this.playNext();
  }


  stopAnimation() {
    this.stopped = true;
    this.animationTweens.forEach(tween => {
      tween.stop();
      tween.setTimeScale(1);
    });
  }

  playNext() {
    if (this.stopped) {
      return;
    }

    if (this.currentTweenIndex < this.animationTweens.length) {
      this.activeTween = this.animationTweens[this.currentTweenIndex++];
      this.activeTween?.play();
    }
  }

  isAnimationPlaying() {
    return this.activeTween?.isPlaying() ?? false;
  }

  skip() {
    if (this.stopped) {
      return;
    }

    this.activeTween?.setTimeScale(100);
  }

}