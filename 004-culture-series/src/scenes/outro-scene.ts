import { getLocalizedText, LanguageRegistryKey, LocalizedText } from "../types/lang";

export class OutroScene extends Phaser.Scene {
  outroSound: Phaser.Sound.BaseSound | null = null;

  approvedLocalizedText: LocalizedText = {
    cz: 'DOPORUČENO',
    en: 'RECOMMENDED'
  };
  nameLocalizedText: LocalizedText = {
    cz: 'Iain M. Banks: Culture Series\n'
    + ' • Consider Phlebas\n'
    + ' • The Player of Games\n'
    + ' • The State of the Art\n'
    + ' • Use of Weapons\n'
    + ' • Excession\n'
    + ' • Inversions\n'
    + ' • Look to Windward\n'
    + ' • Matter\n'
    + ' • Surface Detail\n'
    + ' • The Hydrogen Sonata\n\n',
    en: 'Iain M. Banks: Culture Series\n'
    + ' • Consider Phlebas\n'
    + ' • The Player of Games\n'
    + ' • The State of the Art\n'
    + ' • Use of Weapons\n'
    + ' • Excession\n'
    + ' • Inversions\n'
    + ' • Look to Windward\n'
    + ' • Matter\n'
    + ' • Surface Detail\n'
    + ' • The Hydrogen Sonata\n\n',
  };
  name2LocalizedText: LocalizedText = {
    cz: 'Simone Caroti: The Culture Series of Iain M. Banks: A Critical Introduction\n',
    en: 'Simone Caroti: The Culture Series of Iain M. Banks: A Critical Introduction\n',
  };


  constructor() {
    super({ key: 'OutroScene', active: false});
  }

  preload(): void {
    this.load.audio('outro', 'audio/outro.mp3');

    // ensure font is loaded prior to rendering
    (this.load as any).rexWebFont({
      custom: {
          families: ['OutroFont'],
          urls: [
            'fonts.css',
          ]
      }
    });
  }

  create(): void {
    this.add.text(
      0.5 * this.game.scale.width, 0.5 * this.game.scale.height,
      getLocalizedText(this.nameLocalizedText, this.game.registry.get(LanguageRegistryKey)),
      {
        fontFamily: 'serif',
        fontSize: '4em',
        color: '#14EAE8',
        align: 'left',
      }).setOrigin(0.5, 0.5);
    this.add.text(
      0.5 * this.game.scale.width, 0.8 * this.game.scale.height,
      getLocalizedText(this.name2LocalizedText, this.game.registry.get(LanguageRegistryKey)),
      {
        fontFamily: 'serif',
        fontSize: '3em',
        color: '#14EAE8',
        align: 'left',
      }).setOrigin(0.5, 0.5);


    const verdictBanner = this.add.text(
      0.5 * this.game.scale.width, 0.5 * this.game.scale.height,
      getLocalizedText(this.approvedLocalizedText, this.game.registry.get(LanguageRegistryKey)),
      {
        fontFamily: 'OutroFont',
        fontStyle: 'Bold',
        fontSize: '20em',
        color: 'red',
        align: 'center',
      }).setOrigin(0.5, 0.5).setAlpha(0.25).setRotation(-0.38);

    this.add.tween({
      targets: verdictBanner,
      scaleX: { from: 20, to: 1},
      scaleY: { from: 20, to: 1},
      duration: 800,
      ease: Phaser.Math.Easing.Expo.In,
    });

    this.outroSound = this.sound.add('outro', {
      volume: 0.5,
    });
    this.outroSound.play();

    this.cameras.main.fadeIn(1000, 0, 0, 0)
  }
}
