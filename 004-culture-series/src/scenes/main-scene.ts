import { MainAnimationBuilder } from '../builders/background-animation';
import { DialogBox } from '../objects/dialog-box';
import { Speaker, SpeakerPosition, SpeakerState } from '../objects/speaker';
import { ActionId, ActorId, Line, Script } from '../scripts/script';
import { getLocalizedText, LanguageRegistryKey } from '../types/lang';

export class MainScene extends Phaser.Scene {
  textBox: DialogBox | null = null;
  speakers = new Map<ActorId, Speaker>();
  leftSpeaker: Speaker | null = null;
  rightSpeaker: Speaker | null = null;
  dummySpeaker: Speaker | null = null;
  mouseDown: boolean = false;
  backgroundAnimBuilder = new MainAnimationBuilder();
  script = new Script();
  ignoreInput = false;
  waitForAnimation = false;

  constructor() {
    super({ key: 'MainScene', active: false });
  }

  preload(): void {
    this.createLoadingProgressBar();

    this.load.image('background', 'images/background.png');

    this.load.image('left', 'images/left.png');
    this.load.image('right', 'images/right.png');

    this.load.image('dialogContinue', 'images/dialog_continue.png');
    this.load.image('dialogContinueHighlight', 'images/dialog_continue_hl.png');
    this.load.image('dialogPanel', 'images/dialog_panel.png');
    this.load.image('dialogNameLeft', 'images/dialog_name_left.png');
    this.load.image('dialogNameRight', 'images/dialog_name_right.png');
    this.load.image('empty', 'images/empty.png');

    this.backgroundAnimBuilder.preloadAssets(this);
   }

  create(): void {
    const suitcaseY = 850;

    this.add.image(0, 0, 'background').setOrigin(0, 0);

    this.backgroundAnimBuilder.createObjects(this);

    this.leftSpeaker = new Speaker({
      scene: this,
      textureName: 'left',
      initialState: SpeakerState.active,
      position: SpeakerPosition.leftBottom,
      activeScale: 0.75,
      inactiveScale: 0.65,
      originOffset: {x: 0.1, y: 0},
    });
    this.speakers.set(ActorId.left, this.leftSpeaker);

    this.rightSpeaker = new Speaker({
      scene: this,
      initialState: SpeakerState.inactive,
      textureName: 'right',
      position: SpeakerPosition.rightBottom,
      activeScale: 0.9,
      inactiveScale: 0.8,
      originOffset: {x: -0.22, y: 0},
    });
    this.speakers.set(ActorId.right, this.rightSpeaker);

    this.dummySpeaker = new Speaker({
      scene: this,
      initialState: SpeakerState.hidden,
      textureName: 'empty',
      position: SpeakerPosition.rightBottom,
      activeScale: 1,
      inactiveScale: 1,
    });
    this.speakers.set(ActorId.dummy, this.dummySpeaker);

    this.textBox = new DialogBox({
      scene: this,
      textureNames: {
        nameLeft: 'dialogNameLeft',
        nameRight: 'dialogNameRight',
        box: 'dialogPanel',
        continue: 'dialogContinue',
        continueHighlighted: 'dialogContinueHighlight',
      }
    });

    this.registerInputHandlers();

    this.script.reset();
    this.tryGetNextLine();

    this.cameras.main.fadeIn(1000, 0, 0, 0)

    this.cameras.main.once(Phaser.Cameras.Scene2D.Events.FADE_OUT_COMPLETE, () => {
      this.backgroundAnimBuilder.stopAnimation();
      this.scene.start('OutroScene');
      this.scene.stop('MainScene');
    })
  }

  private registerInputHandlers() {
    this.ignoreInput = this.game.scene.isActive('MainScene');
    this.input.on('pointerup', () => {
      if (this.ignoreInput || (this.waitForAnimation && this.backgroundAnimBuilder.isAnimationPlaying())) {
        return;
      }
      this.backgroundAnimBuilder.skip();
      this.tryGetNextLine();
    }, this);

    this.input.keyboard.on('keydown', (event: any) => {
      if (this.ignoreInput || (this.waitForAnimation && this.backgroundAnimBuilder.isAnimationPlaying())) {
        return;
      }

      switch (event.keyCode) {
        case Phaser.Input.Keyboard.KeyCodes.SPACE:
        case Phaser.Input.Keyboard.KeyCodes.ENTER:
          this.backgroundAnimBuilder.skip();
          this.tryGetNextLine();
          break;
        default:
          break;
      }
    });
  }

  private tryGetNextLine() {
    const line = this.script.nextLine();
    if (line != null) {
      this.applyLine(line);
    } else {
      this.startTransition();
    }
  }

  private applyLine(line: Line) {
    let textBoxVisible = true;
    const activeActor = this.speakers.get(line.actorId);
    const inactiveActors = Array.from(this.speakers.values()).filter(i => i !== activeActor);

    if (line.actions?.includes(ActionId.show)) {
      activeActor?.show(SpeakerState.inactive);
    }
    if (activeActor?.speakerState !== SpeakerState.active) {
      activeActor?.activate();
      inactiveActors
        .filter(a => a.speakerState === SpeakerState.active)
        .forEach(a => a.deactivate());
    }
    if (line.actions?.includes(ActionId.hide)) {
      activeActor?.hide();
      textBoxVisible = false;
    }

    if( activeActor?.config.position === SpeakerPosition.rightBottom ) {
      this.textBox!.setCharacterNameRight(this.script.actor(line.actorId).name);
      this.textBox!.setLeftCharacterNameVisible(false);
      this.textBox!.setRightCharacterNameVisible(true);
    } else {
      // by default make at least left name visible
      this.textBox!.setCharacterNameLeft(this.script.actor(line.actorId).name);
      this.textBox!.setLeftCharacterNameVisible(true);
      this.textBox!.setRightCharacterNameVisible(false);
    }

    this.textBox!.setText(getLocalizedText(line.text, this.game.registry.get(LanguageRegistryKey) ?? 'cz'));
    this.textBox!.setVisible(textBoxVisible);

    if (line.actions?.includes(ActionId.playAnimation)) {
      this.backgroundAnimBuilder.startAnimation(this);
    }
    if (line.actions?.includes(ActionId.continueAnimation)) {
      this.backgroundAnimBuilder.playNext();
    }
    if (line.actions?.includes(ActionId.finishAnimation)) {
      this.backgroundAnimBuilder.stopAnimation();
    }
    if (line.actions?.includes(ActionId.enableAnimationSync)) {
      this.waitForAnimation = true;
    }
    if (line.actions?.includes(ActionId.disableAnimationSync)) {
      this.waitForAnimation = false;
    }
  }

  private startTransition() {
    this.ignoreInput = true;
    this.cameras.main.fadeOut(1000, 0, 0, 0);
  }

  private createLoadingProgressBar() {
    const progressBar = this.add.graphics();
    const progressBox = this.add.graphics();
    progressBox.fillStyle(0x222222, 0.8);
    progressBox.fillRect(this.game.scale.width - 330, this.game.scale.height - 60, 320, 50);

    this.load.on('progress', (value: number) => {
      progressBar.clear();
      progressBar.fillStyle(0xffffff, 1);
      progressBar.fillRect(this.game.scale.width - 320, this.game.scale.height - 50, 300 * value, 30);
    });

    this.load.on('complete', () => {
      progressBar.destroy();
      progressBox.destroy();
    });
  }
}
