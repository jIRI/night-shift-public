import { LocalizedText } from "../types/lang";

export enum ActorId {
  left,
  right,
  dummy,
}

export enum ActionId {
  playAnimation,
  continueAnimation,
  finishAnimation,
  show,
  hide,
  enableAnimationSync,
  disableAnimationSync,
}

export type ActorConfig = {
  id: ActorId;
  name: string;
  textEncoder?: (text:LocalizedText) => LocalizedText;
};

export type SelectItem = {
  text: string;
  gotoLabel: string;
};

export type Line = {
  actorId: ActorId;
  text: LocalizedText;
  visited?: boolean;
  actions?: Array<ActionId>;
  label?: string;
  select?: Array<SelectItem>;
}

export function getLocalizedText(localizedText: LocalizedText, lang: keyof LocalizedText): string {
  return localizedText[lang];
}

const telepathicEncoderReplacers: Array<(s: string) => string> = [
  s => s.replace(/[AÁ]/g, 'GR'),
  s => s.replace(/[EÉĚ]/g, 'RR'),
  s => s.replace(/[IÍ]/g, 'QR'),
  s => s.replace(/[OÓ]/g, 'RX'),
  s => s.replace(/[UÚŮ]/g, 'WR'),
  s => s.replace(/[YÝ]/g, 'TQ'),
  s => s.replace(/[ŠČŘŽĎŇ]/g, 'WT'),
];

export function applyReplacers(text: string, replacers: Array<(s: string) => string>): string {
  let result = text.toUpperCase();
  for(const replacer of replacers) {
    result = replacer(result);
  }
  return result;
}

export class Script {
  currentLineIndex = 0;
  lines: Array<Line>;
  actorMap = new Map<ActorId, ActorConfig>();

  constructor() {
    this.lines = Script.lines();
    Script.actors().forEach(actor => this.actorMap.set(actor.id, actor));
  }

  actor(id: ActorId): ActorConfig {
    return this.actorMap.get(id) ?? Script.actors()[0];
  }

  reset() {
    this.currentLineIndex = -1;
    this.lines.forEach(line => line.visited = false);
  }

  nextLine(input?: string): Line | null {
    if (this.currentLineIndex < 0) {
      // first invocation, just return first line
      this.currentLineIndex = 0;
    } else {
      if (input != null ) {
        // we got input, try find index of the label
        const nextLineIndex = this.lines.findIndex(line => line.label === input);
        if (nextLineIndex >= 0) {
          // label found, jump to label
          this.currentLineIndex = nextLineIndex;
        } else {
          // label not found, well, continue on next line...
          this.currentLineIndex++;
        }
      } else {
        // no input, just continue on next line
        this.currentLineIndex++;
      }
    }

    if (this.currentLineIndex >= this.lines.length) {
      return null;
    }

    this.lines[this.currentLineIndex].visited = true;
    const line = Object.assign({}, this.lines[this.currentLineIndex]);
    const textEncoder = this.actor(line.actorId).textEncoder;
    if( textEncoder != null) {
      line.text = textEncoder(line.text);
    }
    return line;
  }

  //
  // Actors and script definition follows
  //

  static actors(): Array<ActorConfig>  {
    return [
      { id: ActorId.left, name: 'Kaliaraphra' },
      { id: ActorId.right, name: 'Fiolderan' },
    ];
  }

  static telepathicEncoder(text: LocalizedText): LocalizedText {
    if(text.cz.startsWith('[') || text.en.startsWith('[')) {
      return text;
    }

    let cz = applyReplacers(`<${text.cz}>`, telepathicEncoderReplacers);
    let en = applyReplacers(`<${text.en}>`, telepathicEncoderReplacers);

    return {
      cz,
      en,
    };
  }

  static droidEncoder(text: LocalizedText): LocalizedText {
    if(text.cz.startsWith('[') || text.en.startsWith('[')) {
      return text;
    }

    let cz = `{ ${text.cz} }`;
    let en = `{ ${text.en} }`;

    return {
      cz,
      en,
    };
  }


  static lines(): Array<Line> {
    return [
      { actorId: ActorId.left,
        text: {
          cz: '[...]',
          en: '',
        },
        actions: [ActionId.playAnimation],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Tak tohle je ten tenep?',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Mhm.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'A naše elitní denní směna neví co to je?',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Mn, mn.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'A dostali to sem <WRNQRLRRKTQRV>?',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Hmm? Ne, ne.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: '<MRXZKRXWRR> prosákli do baráku nějaké uměligence třídy C v mechatronických tělech a zbabrali u jedné přenos do karantény.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Kluci z provozního pak tyhle věcičky našli dole v suterénu tři, když tam kontrolovali energetické výboje způsobené tou UI.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Podle dokumentace se poslední kompletní inventura dole prováděla třicet cyklů zpátky, ale víš jako to chodí. V podstatě nikdo nedokáže říct, jak dlouho to tam bylo.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Uh, klasika. Kolik jich našli?',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Zatím celkem sedm, ale kdo ví, jestli je to všechno.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Hloubkový sken neprováděli?',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Prováděli, ale nebylo na něm vidět ani těch sedm co máme. Museli prolézt celé to sklepení osobně, aby je našli.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Odráží to fotony a nějak na to působí gravitace. Pro všechny ostatní prostředky tyhle kufříky prostě neexistují.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Ah. Takže není vidět dovnitř?',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Ne. Bez ohledu na energii nic nedokáže projít skz povrch. Ale má to váhu. Jeden kilogram. Přesně. Všech sedm kusů.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Denní směna si myslí, že je uvnitř nějaká aktivní kompenzace, takže je možné, že ve skutečnosti je v tom kufru hmota třeba celé neutronové hvězdy, ale navenek se to tváří jako přesně jedno kilo.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Lepší a lepší.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Je to jak říkáš -- technologie neznámého původu.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Dobrá, co s tím tedy budeme dělat?',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: '<MRXZKRXWRR> navrhují abychom zkusili kvantovou vlivaci.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Eh. Tedy zcela upřímně, od té doby, co se od nich odpojil Umakvacelok, se s nimi nedá vydržet a mám pocit, že mají trochu sebedestruktivní sklony.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Chm. Ano. A ano, je to riziko. Nevíme, co je uvnitř, a je možné, že vlivace způsobí nějakou fatální reakci. Ale jaké máme další možnosti?',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.right,
        text: {
          cz: '[vzdych]',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'No jo, no. Když to tu jen tak necháme ležet, může se stát cokoliv tak jako tak.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Souhlas. Takže začneme?',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Směle do toho.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.right,
        text: {
          cz: '[vliv]',
          en: '',
        },
        actions: [ActionId.continueAnimation],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Nic?',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Nic.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Uh. To bude dlouhá směna.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Alespoň nějaké zajímavé ilegalitky?',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.right,
        text: {
          cz: '[vliv]',
          en: '',
        },
        actions: [ActionId.continueAnimation],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Ah, no když se ptáš, dočetla jsem Culture sérii.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Ale? A jak se ti to líbilo?',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Hele, je to skvělé. Je to deset knih, plus jsem si pak přidala jednu analytickou práci nakonec pro kontext.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.right,
        text: {
          cz: '[vliv]',
          en: '',
        },
        actions: [ActionId.continueAnimation],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Tak povídej.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Hmm. Jak to shrnout, abych ti nezkazila zážitek, kdyby ses k tomu někdy dostal.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Řekněme takhle.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Každá kniha si zaslouží ocenění za nějaký aspekt v rámci celé série.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Takže Consider Phlebas dostane cenu za nejslabší začátek skvělé série. Je to hodně cestování po galaxii, hodně akce, která sice směřuje k nějakému cíli, ale není úplně zřejmé proč se dějí věci které se dějí.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Hlavně v prostřední třetině knihy.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Nakonec to vyústí v uspokojivý závěr, ale mezi tím je to trochu chaos.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Pozitivní je, že velkou galaktickou komunitu kniha osahává jaksi z vnějšku, z pohledu galaktické spodiny, takže si uděláš nějaký obrázek, ale nikdy nevidíš celou situaci a potenciální možnosti rostou nade všecky meze.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Občas ovšem narazíš na pasáže, kdy kroutíš hlavou, proč jsme se s postavami ocitli právě v tomhle konkrétním prostředí. Ale -- celkově Consider Phlebas dobře nastaví celý prostor možností, který série otevírá.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Následuje The Player of Games a cena za nejvíc napínavý popis hraní her v doslovném i metaforickém smyslu.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.right,
        text: {
          cz: '[vliv]',
          en: '',
        },
        actions: [ActionId.continueAnimation],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Eh, promiň. Pokračuj.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Takže druhá kniha. Silné morální poselství, hlubší pohled na Culture jako galaktickou civilizaci s prakticky neomezenými zdroji.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Všechny vymoženosti a ještě něco navrch: Velmi fluidní přístup k pohlaví, uspořádání společnosti bez explicitní hierarchie, silný důraz na etičnost. Na první pohled utopie jako z katalogu.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Ale přitom jsou některé okraje trochu umazané, zejména tam, kde reagují se světem mimo Culture. A hry se hrají všude: jako prostředek k seberealizaci, jako metoda distribuce moci, ale i jaksi skrytě, uvnitř Culture samotné.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Možná moje vůbec nejoblíbenější kniha z celé série.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Potom je tu The State of the Art. Několik krátkých povídek a jedna noveleta.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Ze začátku jakoby trochu drhne, ale pak se zase překlopí ve velmi zajímavé zkoumání hranic osobní svobody a malinko okatou kritiku pozemské společnosti v určitém místě její historie.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Cena za odvahu neudělat z Culture antropocentrickou oslavu pokroku, nebo tak něco, protože se ukáže, že Culture není něco, co vzniklo z pozemšťanů, ale něco, co existuje vedle nich.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Dále Use of Weapons. Další zkoumání toho, jak utopická společnost přenáší pro sebe morálně neakceptovatelné akce na méně vyvinuté civilizace, aby si udržela relevanci.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Hlavně v situacích, kdy je v kontaktu se světem, který není na stejné úrovni a je potřeba operovat mimo rámec vlastních pravidel.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Zajímavá struktura, solidně předznamenaný zvrat v závěru, který se možná dá odhadnout, ale i tak má sílu.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Cena za zahrnutí násílí bez glorifikace a prezentace jeho skutečné ceny v traumatických dopadech a určité nesmyslnosti a zbytečnosti.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Tím končí první třetina série, která zkoumá Culture hlavně z venku, jako když v temné místnosti hmatem osaháváš sochu.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'A následuje Excession, kde se pěkně zostra ponoříme do pohledu zevnitř.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Hmmm, zajímavé, zajímavé.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Momentíček...',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.right,
        text: {
          cz: '[vliv]',
          en: '',
        },
        actions: [ActionId.continueAnimation],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Pokračuj, prosím.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Jak říkám, Excession tě vpustí do vnitřního fungování Culture.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Vidíš jak superuměligence vestavěné do superluminálních vesmírných plavidel řídí lidskou společnost, protože mají být morálnější a rozumnější, a jak reagují, když se setkají s jevem, který překračuje i jejich chápání a možnosti.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Haha, to je dobré.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Chci říct, že je zajímavé, jak jsou některé situace v metavesmíru univerzální.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Přesně. Plus udělená cena je jednoznačně za nejzábavnější knihu v celé sérii. Samozřejmě, že i tady jsou poměrně komplikované situace a morální dilemata, ale je to čistá nezkalená radost ze čtení.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Skvělá kniha.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'O to komplikovanější je pak číst Inversions.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Žádné hašteříčí UI, žádné technologické vymyšlenosti, nic.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Je to pohled odspoda, tedy jak k pokroku směřující vliv nadřazené kultury působí na civilizaci na o několik řádu nižším stupni.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Cena za Culture knihu schovanou v alternativně historickém románu. Za mě asi nejslabší kniha série, ale ne proto, že je špatná -- je vlastně asi dost dobrá.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Ale: přišla jsem pro kosmické pletichy, poletování po galaxii a vybuchování hvězd a místo toho jsem dostala z většiny depresivní historický román.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.right,
        text: {
          cz: '[vliv]',
          en: '',
        },
        actions: [ActionId.continueAnimation],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Pokračuj, pokračuj.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Druhý cyklus celé série končí Look to Windward, kde se zkoumá traumatický vliv nepovedené snahy o nenápadné postrkování technologicky podřazené civilizace.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Kniha je věnovaná veteránům nějakého asi ne tak docela jednoznačně oprávněného konfliktu, který někdy v tom období probíhal na Zemi a v tomto smyslu vlastně predikuje co se na Zemi stalo až nepříjemně přesně.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Silná kniha, silný konec.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Cena za schopnost kultivovaně komunikovat skutečnost, že silnější a vyvinutější společnost nemá vždycky pravdu, a nedokáže vždycky dohlédnout všechny důsledky svých akcí a sjednat pak jejich nápravu. A že export pokroku má svou cenu.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.right,
        text: {
          cz: '[vliv]',
          en: '',
        },
        actions: [ActionId.continueAnimation],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'No a následuje třetí cyklus o třech knihách.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'První je Matter, kde se zkoumá jak "matter" jako hmota, kterou živé bytosti z větší části jsou, tak "matter" jako podstata věci.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Civilizace která si je vědomá své technologické nedovyvinutosti, systémy, které zabraňují intervencím k jejímu posunu kupředu, důvody existence a morální pasti celého takového systému.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Culture jako hráč na poli, kde je srovnatelně silný hráč s vlastními zájmy a pravidly a do toho pozůstatky starodávných civilizací, které v některých ohledech byly zřejmě mocnější než cokoliv, co je v galaxii v současnosti.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Měla jsem pocit, že v některých místech kniha trochu škobrtá, světotvorba mohla někdy být střídmější a tak podobně.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Ale celkově pořád dobrá kniha. Cena za úspěšné obrácení čtenářského strádání v uspokojivý závěr a některé mimořádně zajímavé pasáže.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.right,
        text: {
          cz: '[vliv]',
          en: '',
        },
        actions: [ActionId.continueAnimation],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Jsem jako na jehlách. Ještě dva kusy?',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Ah, věci se komplikují u Surface Detail.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Máme tu nejméně čtyři dějové linky, které se nakonec nějak sejdou, ale pro mě osobně některé byly mnohem méně zajímavé než jiné.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Jsou tu civilizace toužící po uznání, souboj dvou názorů na to, jestli je simulované posmrtné utrpení trvající věčnost nutné k udržení chování společnosti, příběh osobní pomsty a Culture snažící se do toho všeho nenechat příliš namočit.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'A nejvíc charismatická psychopatická UI v celé sérii.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Dovedu si představit, že by kniha byla kompaktnější, kdyby se některé linky prořezaly. Ale cena za opravdu důkladné rozjímání o nutnosti a oprávněnosti institutu trestu, maskované za zkoumání simulační teorie, je bez diskuse.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.right,
        text: {
          cz: '[vliv]',
          en: '',
        },
        actions: [ActionId.continueAnimation],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Takže, poslední kniha?',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Ano, The Hydrogen Sonata. Zabývá se konceptem sublimace, kdy se dostatečně pokročilá civilizace rozhodne přejít na další úroveň existence mimo fyzický vesmír, a co tomu v poslední chvíli předchází.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Opulentní popis různých aspektů a osobních selhání uvnitř posledních dní takové civilizace a jak na celou věc reaguje Culture, která z morálních důvodů zatím nesublimuje ale cítí se angažovaná.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Plus moc pěkná analogie se sonátou z názvu.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Celé to má takovou smířlivě melancholickou patinu, možná i proto, že autor při dokončování věděl, že je smrtelně nemocný a je to nejspíše jeho poslední kniha vůbec.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Konec mě nechal v takovém posmutněle nostalgickém rozpoložení, ale s nadějí na lepší příští. Skvělý konec celé série.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'A tudíž poslední cena za nejlepší knihu, kde na konci nic z toho, co se stalo mezi začátkem a koncem vlastně nic nezmění, ale přitom na tom vlastně vůbec nezáleží a všechno skončí tak jak má.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Hmm. Co ještě? Všechny knihy jsou nabité různými náhodnými nápady, jak by taková Culture mohla vypadat, co by tam dělali lidé a jaké by byly jiné civilizace, že se z toho někdy jednomu jde hlava kolem.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Ne že by to někdy přešlo za mez, kdy te takové extravagantní rozhazování nápadů snesitelné, ale trochu to občas ubírá na soustředěnosti knih.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Ale. Na druhou stranu. O nic z toho bych nechtěla v těch knihách přijít, protože to pomáhá definovat takovou tu jemnou strukturu světa a tak.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'No prostě: drobné výhrady tady či onde, ale celkově -- asi nejlepší série co se mi zatím dostala do rukou.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.right,
        text: {
          cz: '[vliv]',
          en: '',
        },
        actions: [ActionId.continueAnimation],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Ah, skvěle. Pokud mi to můžeš půjčit, rozhodně si to přečtu.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Jasně.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: '[šoup]',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.right,
        text: {
          cz: '[chmát]',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'A co ta analytická věc, co jsi zmiňovala?',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Aha, vidíš.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'The Culture Series of Iain M. Banks: A Critical Introduction.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Dobré shrnutí o autorovi, jeho politických názorech, historii vzniku série, trochu o jeho ostatních dílech a jejich vlivu na Culture sérii a tak dále.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Doporučuji přečíst pro širší kontext věcí a tak.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.right,
        text: {
          cz: '[vliv]',
          en: '',
        },
        actions: [ActionId.continueAnimation],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Děkuji. Přidám do fronty.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'A pokud se nemýlím, tady jsme taky hotoví.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Žádná reakce, předpokládám.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Hmm. Ne.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Pořád nic.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Dobře. Tak si zajděme do kantýny a pak sepíšeme zprávu pro denní směnu.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Souhlas. Můžeme.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Tak pojďme.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.right,
        text: {
          cz: '',
          en: '',
        },
        actions: [ActionId.hide],
      },
      { actorId: ActorId.left,
        text: {
          cz: '',
          en: '',
        },
        actions: [ActionId.hide],
      },
      { actorId: ActorId.dummy,
        text: {
          cz: '',
          en: '',
        },
        actions: [ActionId.hide],
      },
      { actorId: ActorId.dummy,
        text: {
          cz: '',
          en: '',
        },
        actions: [ActionId.hide, ActionId.enableAnimationSync, ActionId.continueAnimation],
      },
    ];
  }
}