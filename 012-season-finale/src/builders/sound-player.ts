export class Sounds {
  noise: Phaser.Sound.BaseSound | null = null;
  noiseSuitcase: Phaser.Sound.BaseSound | null = null;
  explosion: Phaser.Sound.BaseSound | null = null;
  directorSpell: Phaser.Sound.BaseSound | null = null;
  robotPowerdown: Phaser.Sound.BaseSound | null = null;
  robotBeep: Phaser.Sound.BaseSound | null = null;
  robotStep: Phaser.Sound.BaseSound | null = null;
  robotPowershotCharge: Phaser.Sound.BaseSound | null = null;
  robotPowershot: Phaser.Sound.BaseSound | null = null;
}

export type AvailableSounds = keyof Sounds;

export class SoundPlayerBuilder extends Sounds {
  preloadAssets(scene: Phaser.Scene) {
    scene.load.audio('noise', 'audio/noise.mp3');
    scene.load.audio('noise-suitcase', 'audio/noise2.mp3');
    scene.load.audio('explosion', 'audio/explosion.mp3');
    scene.load.audio('director-spell', 'audio/director-spell.mp3');
    scene.load.audio('robot-powerdown', 'audio/powerdown.ogg');
    scene.load.audio('robot-powershot-charge', 'audio/powershot-charge.mp3');
    scene.load.audio('robot-powershot', 'audio/robot-powershot.ogg');
    scene.load.audio('robot-step', 'audio/robot-step.ogg');
    scene.load.audio('robot-beep', 'audio/robot-beep.ogg');
  }

  createObjects(scene: Phaser.Scene) {
    this.noise = scene.sound.add('noise', {
      volume: 0.05,
      rate: 1,
    });
    this.noiseSuitcase = scene.sound.add('noise-suitcase', {
      volume: 0.05,
      rate: 1,
      loop: true,
    });
    this.explosion = scene.sound.add('explosion', {
      volume: 0.05,
      rate: 1,
    });
    this.robotPowerdown = scene.sound.add('robot-powerdown', {
      volume: 0.05,
      rate: 1,
    });
    this.directorSpell = scene.sound.add('director-spell', {
      volume: 0.2,
      rate: 1,
    });
    this.robotPowershotCharge = scene.sound.add('robot-powershot-charge', {
      volume: 0.2,
      rate: 2,
      loop: true,
    });
    this.robotPowershot = scene.sound.add('robot-powershot', {
      volume: 0.2,
      rate: 2,
    });
    this.robotStep = scene.sound.add('robot-step', {
      volume: 0.01,
      rate: 3.5,
      loop: true,
    });
    this.robotBeep = scene.sound.add('robot-beep', {
      volume: 0.05,
      rate: 2,
    });
  }

  play(key: AvailableSounds) {
    switch(key) {
      case 'noise':
        this.noise?.play();
        return;
      case 'noiseSuitcase':
        this.noiseSuitcase?.play();
        return;
      case 'explosion':
        this.explosion?.play();
        return;
      case 'directorSpell':
        this.directorSpell?.play();
        return;
      case 'robotPowerdown':
        this.robotPowerdown?.play();
        return;
      case 'robotPowershotCharge':
        this.robotPowershotCharge?.play();
        return;
      case 'robotPowershot':
        this.robotPowershot?.play();
        return;
      case 'robotStep':
        this.robotStep?.play();
        return;
      case 'robotBeep':
        this.robotBeep?.play();
        return;
      default:
        return;
    }
  }

  stop(key: AvailableSounds) {
    switch(key) {
      case 'noise':
        this.noise?.stop();
        return;
      case 'noiseSuitcase':
        this.noiseSuitcase?.stop();
        return;
      case 'explosion':
        this.explosion?.stop();
        return;
      case 'directorSpell':
        this.directorSpell?.stop();
        return;
      case 'robotPowerdown':
        this.robotPowerdown?.stop();
        return;
      case 'robotPowershotCharge':
        this.robotPowershotCharge?.stop();
        return;
      case 'robotPowershot':
        this.robotPowershot?.stop();
        return;
      case 'robotStep':
        this.robotStep?.stop();
        return;
      case 'robotBeep':
        this.robotBeep?.stop();
        return;
      default:
        return;
    }
  }
}