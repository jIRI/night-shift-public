import { GameObjects } from "phaser";
import { SoundPlayerBuilder } from "./sound-player";

export class MainAnimationBuilder {
  soundPlayer: SoundPlayerBuilder | null = null;

  currentTweenIndex = 0;
  animationTweens = new Array<Phaser.Tweens.Tween>();
  activeTween: Phaser.Tweens.Tween | null = null;
  stopped = false;

  background1a: Phaser.GameObjects.Image | null = null;
  background1ba: Phaser.GameObjects.Image | null = null;
  background1bb: Phaser.GameObjects.Image | null = null;
  background2a: Phaser.GameObjects.Image | null = null;
  background2b: Phaser.GameObjects.Image | null = null;
  background2c: Phaser.GameObjects.Image | null = null;
  background3: Phaser.GameObjects.Image | null = null;
  background4: Phaser.GameObjects.Image | null = null;
  background5: Phaser.GameObjects.Image | null = null;

  droneSprite: Phaser.GameObjects.Sprite | null = null;

  robotSprite: Phaser.GameObjects.Sprite | null = null;
  robotSpriteHot: Phaser.GameObjects.Sprite | null = null;

  suitcase: Phaser.GameObjects.Image | null = null;
  suitcaseSound: Phaser.Sound.BaseSound | null = null;

  teunogSprite: Phaser.GameObjects.Sprite | null = null;
  teunogSprite2: Phaser.GameObjects.Sprite | null = null;

  director: Phaser.GameObjects.Image | null = null;
  directorHead1: Phaser.GameObjects.Image | null = null;
  directorHead2: Phaser.GameObjects.Image | null = null;
  directorHead3: Phaser.GameObjects.Image | null = null;
  engineer: Phaser.GameObjects.Image | null = null;
  manager: Phaser.GameObjects.Image | null = null;
  managerSpellSprite: Phaser.GameObjects.Sprite | null = null;
  managerSpellSound: Phaser.Sound.BaseSound | null = null;

  preloadAssets(scene: Phaser.Scene) {
    scene.load.image('background-1-1', 'images/background-1-1.png');
    scene.load.image('background-1-2-1', 'images/background-1-2-1.png');
    scene.load.image('background-1-2-2', 'images/background-1-2-2.png');
    scene.load.image('background-2-1', 'images/background-2-1.png');
    scene.load.image('background-2-2', 'images/background-2-2.png');
    scene.load.image('background-2-3', 'images/background-2-3.png');
    scene.load.image('background-3', 'images/background-3.png');
    scene.load.image('background-4', 'images/background-4.png');
    scene.load.image('background-5', 'images/background-5.png');

    scene.load.spritesheet('robot-idle', 'images/robot-idle.png', { frameWidth: 192, frameHeight: 180 });
    scene.load.spritesheet('robot-walk', 'images/robot-walk.png', { frameWidth: 220, frameHeight: 193 });
    scene.load.spritesheet('robot-powershot-full', 'images/robot-powershot-full.png', { frameWidth: 685, frameHeight: 191 });
    scene.load.spritesheet('robot-shutdown', 'images/robot-shutdown.png', { frameWidth: 210, frameHeight: 200 });

    scene.load.spritesheet('drone-idle', 'images/drone-idle.png', { frameWidth: 203, frameHeight: 126 });
    scene.load.spritesheet('drone-fly', 'images/drone-fly.png', { frameWidth: 200, frameHeight: 164 });
    scene.load.spritesheet('drone-turn', 'images/drone-turn.png', { frameWidth: 220, frameHeight: 204 });

    scene.load.image('suitcase', 'images/suitcase.png');
    scene.load.spritesheet('teunog', 'images/teunog-anim.png', { frameWidth: 192, frameHeight: 192 });

    scene.load.image('director', 'images/director-2.png');
    scene.load.image('directorHead1', 'images/head-1.png');
    scene.load.image('directorHead2', 'images/head-2.png');
    scene.load.image('directorHead3', 'images/head-3.png');

    scene.load.image('engineer', 'images/engineer.png');
    scene.load.image('manager', 'images/manager.png');

  }

  createObjects(scene: Phaser.Scene, soundPlayer: SoundPlayerBuilder) {
    this.soundPlayer = soundPlayer;

    const speedModifier = 1;

    const actorsY = 850;

    this.background1a = scene.add.image(scene.game.scale.width / 2, scene.game.scale.height / 2, 'background-1-1').setDepth(-9).setVisible(false);
    this.background1ba = scene.add.image(scene.game.scale.width / 2, scene.game.scale.height / 2, 'background-1-2-1').setDepth(-5).setVisible(false);
    this.background1bb = scene.add.image(scene.game.scale.width / 2, 0, 'background-1-2-2').setOrigin(0.5, 0).setDepth(-9).setVisible(false);
    this.background2a = scene.add.image(scene.game.scale.width / 2, 0, 'background-2-1').setOrigin(0.5, 0).setDepth(-10).setVisible(false);
    this.background2b = scene.add.image(scene.game.scale.width / 2, 0, 'background-2-2').setOrigin(0.5, 0).setDepth(-10).setVisible(false);
    this.background2c = scene.add.image(scene.game.scale.width / 2, scene.game.scale.height, 'background-2-3').setOrigin(0.5, 1).setDepth(-9).setVisible(false);
    this.background3 = scene.add.image(scene.game.scale.width / 2, scene.game.scale.height / 2, 'background-3').setDepth(-10).setVisible(false);
    this.background4 = scene.add.image(scene.game.scale.width / 2, scene.game.scale.height / 2, 'background-4').setDepth(-10).setVisible(false);
    this.background5 = scene.add.image(scene.game.scale.width / 2, scene.game.scale.height / 2, 'background-5').setDepth(-10).setVisible(false);

    this.robotSprite = scene.add.sprite(200, actorsY, 'robot').setScale(1, 1).setOrigin(0.5, 1).setDepth(0).setVisible(false);
    scene.anims.create({
      key: 'robot-idle',
      frames: scene.anims.generateFrameNames('robot-idle', { start: 0, end: 11 }),
      frameRate: 15,
      repeat: -1,
    });
    scene.anims.create({
      key: 'robot-walk',
      frames: scene.anims.generateFrameNames('robot-walk', { start: 0, end: 11 }),
      frameRate: 15,
      repeat: -1,
    });
    scene.anims.create({
      key: 'robot-powershot-part-1',
      frames: scene.anims.generateFrameNames('robot-powershot-full', { start: 0, end: 15 }),
      frameRate: 15,
    });
    scene.anims.create({
      key: 'robot-powershot-part-2',
      frames: scene.anims.generateFrameNames('robot-powershot-full', { start: 16, end: 19 }),
      frameRate: 15,
      repeat: -1,
    });
    scene.anims.create({
      key: 'robot-powershot-part-3',
      frames: scene.anims.generateFrameNames('robot-powershot-full', { start: 20, end: 39 }),
      frameRate: 15,
    });
    scene.anims.create({
      key: 'robot-shutdown',
      frames: scene.anims.generateFrameNames('robot-shutdown', { start: 0, end: 19 }),
      frameRate: 15,
    });

    this.robotSpriteHot = scene.add.sprite(400, actorsY, 'robot-hot').setScale(1, 1).setOrigin(0.5, 1).setDepth(0).setTintFill(0xff0000).setAlpha(0).setVisible(false);
    scene.anims.create({
      key: 'robot-powershot-part-2-hot',
      frames: scene.anims.generateFrameNames('robot-powershot-full', { start: 16, end: 19 }),
      frameRate: 15,
      repeat: -1,
    });

    const offScreenRobotPosition = scene.game.scale.width + 4 * this.robotSprite?.width;

    this.droneSprite = scene.add.sprite(scene.game.scale.width - 200, actorsY - 200, 'drone').setScale(1, 1).setOrigin(0.5, 1).setScale(0.7).setDepth(0).setFlipX(true).setVisible(false);
    scene.anims.create({
      key: 'drone-idle',
      frames: scene.anims.generateFrameNames('drone-idle', { start: 0, end: 11 }),
      frameRate: 12,
      repeat: -1,
    });
    scene.anims.create({
      key: 'drone-fly',
      frames: scene.anims.generateFrameNames('drone-fly', { start: 0, end: 11 }),
      frameRate: 15,
      repeat: -1,
    });
    scene.anims.create({
      key: 'drone-turn',
      frames: scene.anims.generateFrameNames('drone-turn', { start: 0, end: 15 }),
      frameRate: 15,
    });

    this.teunogSprite = scene.add.sprite(scene.game.scale.width / 2, actorsY - 110, 'teunog').setScale(1.2).setOrigin(0.5, 0.5).setDepth(1).setVisible(false);
    scene.anims.create({
      key: 'teunog-anim',
      frames: scene.anims.generateFrameNames('teunog', { start: 0, end: 29 }),
      frameRate: 30,
      repeat: -1,
    });
    this.teunogSprite2 = scene.add.sprite(scene.game.scale.width / 2, actorsY - 110, 'teunog').setScale(1.2).setOrigin(0.5, 0.5).setDepth(0).setVisible(false);
    scene.anims.create({
      key: 'teunog2-anim',
      frames: scene.anims.generateFrameNames('teunog', { start: 12, end: 23 }),
      frameRate: 15,
      yoyo: true,
      repeat: -1,
    });


    this.suitcase = scene.add.image(1500, actorsY - 10, 'suitcase').setOrigin(0.5, 1).setScale(0.6, 0.6).setDepth(5).setVisible(false);

    this.engineer = scene.add.image(0, 920, 'engineer').setOrigin(0.5, 1).setScale(0.6, 0.6).setVisible(false);
    this.manager = scene.add.image(0, 920, 'manager').setOrigin(0.5, 1).setScale(0.77, 0.77).setVisible(false);

    this.director = scene.add.image(0, 920, 'director').setOrigin(0.5, 1).setScale(0.7, 0.7).setVisible(false);
    this.directorHead1 = scene.add.image(0, 920, 'directorHead1').setOrigin(0.5, 0.5).setScale(0.7, 0.7).setVisible(false);
    this.directorHead2 = scene.add.image(0, 920, 'directorHead2').setOrigin(0.5, 0.5).setScale(0.7, 0.7).setVisible(false);
    this.directorHead3 = scene.add.image(0, 920, 'directorHead3').setOrigin(0.5, 0.5).setScale(0.7, 0.7).setVisible(false);

    this.buildScreen1Animation(scene, speedModifier, actorsY, offScreenRobotPosition);
    this.buildScreen2Animation(scene, speedModifier, offScreenRobotPosition);
    this.buildScreen3Animation(scene, speedModifier, offScreenRobotPosition);
    this.buildScreen4Animation(scene, speedModifier, actorsY);
  }

  private buildScreen1Animation(scene: Phaser.Scene, speedModifier: number, actorsY: number, offScreenRobotPosition: number) {
    // start with robot idling
    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.robotSprite,
      alpha: 1,
      duration: speedModifier * 2000,
      onStart: () => {
        this.robotSprite?.play('robot-idle');
        this.droneSprite?.play('drone-idle');
      },
      onComplete: () => {
      },
    }));

    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: [this.background1a, this.background1ba, this.background1bb],
      alpha: 0,
      duration: 500,
      onStart: () => {
        this.background2a?.setVisible(true);
        this.background2c?.setVisible(true);
        this.robotSprite?.setVisible(true);
        this.droneSprite?.setVisible(true);
      },
      onComplete: () => {
        this.background1a?.setVisible(false);
        this.background1ba?.setVisible(false);
        this.background1bb?.setVisible(false);
      },
    }));

    // flash the background
    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.background2a,
      alpha: { from: 1, to: 0 },
      yoyo: true,
      duration: speedModifier * 50,
      repeat: 5,
      onStart: () => {
        this.playNext();
        this.soundPlayer?.play('noise');
      },
      onComplete: () => {
      },
    }));
    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.background2b,
      alpha: { from: 0, to: 1 },
      yoyo: true,
      duration: speedModifier * 50,
      repeat: 5,
      onStart: () => {
        this.background2b?.setVisible(true);
      },
      onComplete: () => {
        this.background2a?.setVisible(false);
        this.background2b?.setAlpha(1);
        this.background2b?.setVisible(true);
        this.playNext();
      },
    }));
    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.background2a,
      alpha: { from: 1, to: 0 },
      yoyo: true,
      delay: speedModifier * 500,
      duration: speedModifier * 50,
      repeat: 3,
      onStart: () => {
        this.soundPlayer?.play('noise');
        this.playNext();
      },
      onComplete: () => {
      },
    }));
    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.background2b,
      alpha: { from: 0, to: 1 },
      yoyo: true,
      duration: speedModifier * 50,
      repeat: 3,
      onStart: () => {
        this.background2b?.setVisible(true);
      },
      onComplete: () => {
        this.background2a?.setVisible(true);
        this.background2a?.setAlpha(1);
        this.background2b?.setVisible(false);
      },
    }));

    // show the door disappearing
    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: [this.background1a, this.background1ba, this.background1bb],
      alpha: 1,
      duration: speedModifier * 300,
      onStart: () => {
        this.robotSprite?.setVisible(false);
        this.droneSprite?.setVisible(false);
        this.background1a?.setVisible(true);
        this.background1ba?.setVisible(true).setDepth(1);
        this.background1bb?.setVisible(true).setDepth(1);
        this.background2c?.setVisible(false);
      },
      onComplete: () => {
        this.background2a?.setVisible(false);
        this.background2b?.setVisible(false);
        this.playNext();
      },
    }));
    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.background1bb,
      alpha: { from: 1, to: 0 },
      delay: 400,
      yoyo: true,
      repeat: 3,
      duration: speedModifier * 50,
      onStart: () => {
        this.soundPlayer?.play('noise');
        this.playNext();
      },
      onComplete: () => {
        this.background1bb?.setVisible(false);
      },
    }));

    // robot runs to middle of screen
    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.robotSprite,
      x: scene.game.scale.width / 2 - 100,
      duration: speedModifier * 1500,
      onStart: () => {
        this.robotSprite?.setVisible(true);
        this.robotSprite?.setY(scene.game.scale.height / 3);
        this.robotSprite?.setScale(0.5);
        this.robotSprite?.play('robot-walk');
        this.soundPlayer?.play('robotStep');
        this.playNext();
      },
      onComplete: () => {
        this.soundPlayer?.stop('robotStep');
        this.robotSprite?.play('robot-idle');
      },
    }));
    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.droneSprite,
      x: scene.game.scale.width / 2 + 100,
      duration: speedModifier * 1500,
      onStart: () => {
        this.droneSprite?.setVisible(true);
        this.droneSprite?.setY(scene.game.scale.height / 3 - 66);
        this.droneSprite?.setScale(0.5);
        this.droneSprite?.play('drone-fly');
      },
      onComplete: () => {
        this.droneSprite?.play('drone-idle');
        this.droneSprite?.setDepth(2);
      },
    }));


    // robot moves to foreground
    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.robotSprite,
      y: actorsY,
      scale: 1,
      duration: speedModifier * 1000,
      onStart: () => {
        this.robotSprite?.play('robot-walk');
        this.soundPlayer?.play('robotStep');
      },
      onComplete: () => {
        this.soundPlayer?.stop('robotStep');
        this.robotSprite?.play('robot-idle');
        this.playNext();
      },
    }));

    // robot moves to right to the next scene
    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.robotSprite,
      x: offScreenRobotPosition,
      duration: speedModifier * 1000,
      onStart: () => {
        this.robotSprite?.play('robot-walk');
        this.soundPlayer?.play('robotStep');
      },
      onComplete: () => {
        this.soundPlayer?.stop('robotStep');
      },
    }));

    // drone follows
    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.droneSprite,
      y: actorsY - 200,
      scale: 0.7,
      duration: speedModifier * 1000,
      onStart: () => {
        this.droneSprite?.setFlipX(false);
      },
      onComplete: () => {
        this.droneSprite?.play('drone-idle');
        this.playNext();
      },
    }));
    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.droneSprite,
      x: offScreenRobotPosition,
      duration: speedModifier * 1000,
      onStart: () => {
        this.droneSprite?.play('drone-fly');
      },
      onComplete: () => {
        this.droneSprite?.play('drone-idle');
        this.playNext();
      },
    }));
  }

  private buildScreen2Animation(scene: Phaser.Scene, speedModifier: number, offScreenRobotPosition: number) {
    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: [this.background1a, this.background1ba, this.background1bb],
      alpha: 0,
      duration: speedModifier * 300,
      onStart: () => {
        this.background3?.setVisible(true);
      },
      onComplete: () => {
        this.background1a?.setVisible(false);
        this.background1ba?.setVisible(false);
        this.background1bb?.setVisible(false);
        this.playNext();
      },
    }));

    // robot and drone enters the scene
    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.robotSprite,
      x: { from: -3 * this.robotSprite!.width, to: 300 },
      duration: speedModifier * 600,
      onStart: () => {
        this.robotSprite?.play('robot-walk');
        this.soundPlayer?.play('robotStep');
      },
      onComplete: () => {
        this.robotSprite?.play('robot-idle');
        this.soundPlayer?.stop('robotStep');
        this.playNext();
      },
    }));
    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.droneSprite,
      x: { from: -3 * this.droneSprite!.width, to: 150 },
      duration: speedModifier * 600,
      onStart: () => {
        this.droneSprite?.play('drone-fly');
        this.playNext();
      },
      onComplete: () => {
        this.droneSprite?.play('drone-idle');
      },
    }));

    // manager enters the scene
    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.manager,
      x:  { from: scene.game.scale.width + this.manager!.width, to: scene.game.scale.width - 200 },
      duration: speedModifier * 800,
      onStart: () => {
        this.manager?.setVisible(true);
      },
      onComplete: () => {
      },
    }));

    // manager leaves the scene
    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.manager,
      x:  scene.game.scale.width + 2 * this.manager!.width,
      duration: speedModifier * 1000,
      onStart: () => {
        this.playNext();
      },
      onComplete: () => {
        this.manager?.setVisible(false);
      },
    }));
    // robot and drone leave the scene
    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: [this.robotSprite, this.droneSprite],
      x: offScreenRobotPosition,
      duration: speedModifier * 1500,
      onStart: () => {
        this.robotSprite?.play('robot-walk');
        this.droneSprite?.play('drone-fly');
        this.soundPlayer?.play('robotStep');
      },
      onComplete: () => {
        this.robotSprite?.play('robot-idle');
        this.droneSprite?.play('drone-idle');
        this.soundPlayer?.stop('robotStep');
        this.playNext();
      },
    }));
  }


  private buildScreen3Animation(scene: Phaser.Scene, speedModifier: number, offScreenRobotPosition: number) {
    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: [this.background3],
      alpha: 0,
      duration: speedModifier * 300,
      onStart: () => {
        this.background4?.setVisible(true);
      },
      onComplete: () => {
        this.background3?.setVisible(false);
        this.playNext();
      },
    }));

    // robot and drone enters the scene
    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.robotSprite,
      x: { from: scene.game.scale.width + 3 * this.robotSprite!.width, to: scene.game.scale.width - 300 },
      duration: speedModifier * 600,
      onStart: () => {
        this.robotSprite?.setFlipX(true);
        this.robotSprite?.play('robot-walk');
        this.soundPlayer?.play('robotStep');
      },
      onComplete: () => {
        this.robotSprite?.play('robot-idle');
        this.soundPlayer?.stop('robotStep');
        this.playNext();
      },
    }));
    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.droneSprite,
      x: { from: scene.game.scale.width + 3 * this.droneSprite!.width, to: scene.game.scale.width - 150 },
      duration: speedModifier * 600,
      onStart: () => {
        this.droneSprite?.setFlipX(true);
        this.droneSprite?.play('drone-fly');
        this.playNext();
      },
      onComplete: () => {
        this.droneSprite?.play('drone-idle');
      },
    }));

    // engineer enters the scene
    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.engineer,
      x:  { from: -this.engineer!.width, to: 200 },
      duration: speedModifier * 800,
      onStart: () => {
        this.engineer?.setVisible(true);
      },
      onComplete: () => {
      },
    }));

    // engineer leaves the scene
    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.engineer,
      x: -2 * this.engineer!.width,
      duration: speedModifier * 1000,
      onStart: () => {
        this.playNext();
      },
      onComplete: () => {
        this.engineer?.setVisible(false);
      },
    }));
    // robot and drone leave the scene
    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: [this.robotSprite, this.droneSprite],
      x: -3 * this.robotSprite!.width,
      duration: speedModifier * 1500,
      onStart: () => {
        this.robotSprite?.play('robot-walk');
        this.droneSprite?.play('drone-fly');
        this.soundPlayer?.play('robotStep');
      },
      onComplete: () => {
        this.robotSprite?.play('robot-idle');
        this.droneSprite?.play('drone-idle');
        this.soundPlayer?.stop('robotStep');
        this.robotSprite?.setFlipX(false);
        this.droneSprite?.setFlipX(false);
        this.playNext();
      },
    }));
  }

  private buildScreen4Animation(scene: Phaser.Scene, speedModifier: number, actorsY: number) {
    const headsBob = [
      scene.add.tween({
        paused: true,
        targets: this.directorHead1,
        y: { from: 300, to: 295 },
        duration: speedModifier * 900,
        yoyo: true,
        repeat: -1
      }),
      scene.add.tween({
        paused: true,
        targets: this.directorHead2,
        y: { from: 490, to: 500 },
        duration: speedModifier * 900,
        yoyo: true,
        repeat: -1
      }),
      scene.add.tween({
        paused: true,
        targets: this.directorHead3,
        y: { from: 525, to: 515 },
        duration: speedModifier * 900,
        yoyo: true,
        repeat: -1
      }),
    ];

    const suitcaseX = scene.game.scale.width / 2;
    const suitcaseY = actorsY - 110;
    const suitcaseShake = scene.tweens.addCounter({
      paused: true,
      from: 255,
      to: 100,
      duration: speedModifier * 500,
      repeat: -1,
      onUpdate: (tween) => {
          const value = Math.floor(tween.getValue());
          this.suitcase?.setTint(Phaser.Display.Color.GetColor(value, 255, 255));
          this.suitcase?.setX(Phaser.Math.Between(suitcaseX - 5, suitcaseX + 5));
          this.suitcase?.setY(Phaser.Math.Between(suitcaseY - 5, suitcaseY + 5));
      },
    });


    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: [this.background4],
      alpha: 0,
      duration: speedModifier * 300,
      onStart: () => {
        this.background5?.setVisible(true);
      },
      onComplete: () => {
        this.background4?.setVisible(false);
        this.suitcase?.setVisible(true);
        this.director?.setX(scene.game.scale.width - 200);
        this.directorHead1?.setPosition(scene.game.scale.width - 348, 305);
        this.directorHead2?.setPosition(scene.game.scale.width - 378, 495);
        this.directorHead3?.setPosition(scene.game.scale.width - 50, 520);
        this.director?.setVisible(true);
        this.directorHead1?.setVisible(true);
        this.directorHead2?.setVisible(true);
        this.directorHead3?.setVisible(true);
        headsBob.forEach(bob => bob.play());
        suitcaseShake.play();
        this.soundPlayer?.play('noiseSuitcase');
        this.playNext();
      },
    }));


    // robot and drone enters the scene
    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.robotSprite,
      x: { from: -3 * this.robotSprite!.width, to: 300 },
      duration: speedModifier * 600,
      onStart: () => {
        this.robotSprite?.play('robot-walk');
        this.soundPlayer?.play('robotStep');
      },
      onComplete: () => {
        this.robotSprite?.play('robot-idle');
        this.soundPlayer?.stop('robotStep');
        this.playNext();
      },
    }));
    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.droneSprite,
      x: { from: -3 * this.droneSprite!.width, to: 150 },
      duration: speedModifier * 600,
      onStart: () => {
        this.droneSprite?.play('drone-fly');
      },
      onComplete: () => {
        this.droneSprite?.play('drone-idle');
      },
    }));

    // director fires heads
    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.director,
      x: scene.game.scale.width - 220,
      duration: speedModifier * 60,
      onStart: () => {
        headsBob.forEach(bob => bob.stop());
        this.soundPlayer?.play('directorSpell');
        this.playNext();
      },
      onComplete: () => {
      },
    }));
    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: [ this.directorHead1, this.directorHead2, this.directorHead3 ],
      x: scene.game.scale.width / 2,
      y: actorsY - 110,
      duration: speedModifier * 300,
      onStart: () => {
      },
      onComplete: () => {
        this.playNext();
        this.directorHead1?.setVisible(false);
        this.directorHead2?.setVisible(false);
        this.directorHead3?.setVisible(false);
      },
    }));

    // suitcase transforms into cloud
    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.suitcase,
      alpha: 0,
      duration: speedModifier * 300,
      onStart: () => {
        suitcaseShake.stop();
        scene.cameras.main.shakeEffect.start(700, 0.05);
      },
      onComplete: () => {
        this.playNext();
      },
    }));
    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: [this.teunogSprite, this.teunogSprite2],
      alpha: 1,
      duration: 300,
      onStart: () => {
        this.teunogSprite?.setAlpha(0);
        this.teunogSprite?.setVisible(true);
        this.teunogSprite?.play('teunog-anim');
        this.teunogSprite2?.setAlpha(0);
        this.teunogSprite2?.setVisible(true);
        this.teunogSprite2?.play('teunog2-anim');
        this.playNext();
      },
      onComplete: () => {
        this.teunogSprite2?.setAlpha(0.8);
      },
    }));

    // director shrinks away
    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.director,
      x: scene.game.scale.width + this.director!.width,
      alpha: 0.3,
      scale: 0.1,
      duration: speedModifier * 400,
    }));

    // robot powershots at the teunog the scene
    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.robotSprite,
      alpha: 1,
      duration: speedModifier * 1066,
      onStart: () => {
        this.robotSprite?.setVisible(true);
        this.robotSprite?.setX(400);
        this.robotSprite?.play('robot-powershot-part-1');
        this.soundPlayer?.play('robotPowershotCharge');
      },
      onComplete: () => {
        this.playNext();
      },
    }));
    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.robotSpriteHot,
      alpha: 1,
      duration: speedModifier * 3 * 1080,
      onStart: () => {
        this.robotSpriteHot?.setVisible(true);
        this.robotSpriteHot?.play('robot-powershot-part-2-hot');
        this.robotSprite?.play('robot-powershot-part-2');
        this.playNext();
      },
      onComplete: () => {
        this.robotSpriteHot?.setVisible(false);
        this.soundPlayer?.stop('robotPowershotCharge');
        this.soundPlayer?.play('robotPowershot');
      },
    }));
    // teunog grows
    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: [this.teunogSprite, this.teunogSprite2],
      scale: 3,
      duration: speedModifier * 3 * 1080,
      onStart: () => {
      },
      onComplete: () => {
        this.soundPlayer?.stop('noiseSuitcase');
        this.playNext();
      },
    }));
    // teunog explodes
    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: [this.teunogSprite, this.teunogSprite2],
      scale: 100,
      alpha: 0,
      duration: speedModifier * 500,
      onStart: () => {
        this.soundPlayer?.play('explosion');
        this.playNext();
      },
      onComplete: () => {
        this.teunogSprite?.setVisible(false);
        this.teunogSprite2?.setVisible(false);
      },
    }));
    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.robotSprite,
      alpha: 1,
      duration: speedModifier * 1273,
      onStart: () => {
        this.robotSprite?.play('robot-powershot-part-3');
      },
      onComplete: () => {
        this.playNext();
      },
    }));

    // robot shutdowns
    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.robotSprite,
      alpha: 1,
      duration: speedModifier * 600,
      onStart: () => {
        this.robotSprite?.setX(220);
        this.robotSprite?.play('robot-shutdown');
        this.soundPlayer?.play('robotPowerdown');
      },
      onComplete: () => {
      },
    }));

    // drone moves in front of robot
    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.droneSprite,
      x: 400,
      duration: speedModifier * 500,
      onStart: () => {
        this.droneSprite?.play('drone-fly');
      },
      onComplete: () => {
        this.droneSprite?.setFlipX(true);
        this.droneSprite?.play('drone-idle');
      },
    }));

    // drone knocks on the robot
    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.droneSprite,
      x: 320,
      y: actorsY - 80,
      duration: speedModifier * 400,
      onStart: () => {
        this.droneSprite?.play('drone-fly');
      },
      onComplete: () => {
        this.playNext();
        this.soundPlayer?.play('robotBeep');
      },
    }));
    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.droneSprite,
      x: 500,
      duration: speedModifier * 300,
      onStart: () => {
        this.droneSprite?.play('drone-fly');
      },
      onComplete: () => {
        this.droneSprite?.play('drone-idle');
      },
    }));

    // drone knocks on the robot
    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.droneSprite,
      x: 320,
      duration: speedModifier * 300,
      onStart: () => {
        this.droneSprite?.play('drone-fly');
      },
      onComplete: () => {
        this.playNext();
        this.soundPlayer?.play('robotBeep');
      },
    }));
    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.droneSprite,
      x: 500,
      duration: speedModifier * 300,
      onStart: () => {
        this.droneSprite?.play('drone-fly');
      },
      onComplete: () => {
        this.droneSprite?.play('drone-idle');
      },
    }));

    // drone knocks on the robot
    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.droneSprite,
      x: 320,
      duration: speedModifier * 300,
      onStart: () => {
        this.droneSprite?.play('drone-fly');
      },
      onComplete: () => {
        this.playNext();
        this.soundPlayer?.play('robotBeep');
      },
    }));
    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.droneSprite,
      x: 500,
      duration: speedModifier * 300,
      onStart: () => {
        this.droneSprite?.play('drone-fly');
      },
      onComplete: () => {
        this.droneSprite?.play('drone-idle');
      },
    }));
  }


  startAnimation(scene: Phaser.Scene) {
    this.stopped = false;
    this.currentTweenIndex = 0;
    this.background1a?.setVisible(true);
    this.background1ba?.setVisible(true);
    this.background1bb?.setVisible(true);
    this.robotSprite?.play('robot-idle').setVisible(false);
    this.animationTweens.forEach(t => t.setTimeScale(1));

    this.playNext();
  }


  stopAnimation() {
    this.stopped = true;
    this.animationTweens.forEach(tween => {
      tween.stop();
      tween.setTimeScale(1);
    });
  }

  playNext() {
    if (this.stopped) {
      return;
    }

    if (this.currentTweenIndex < this.animationTweens.length) {
      this.activeTween = this.animationTweens[this.currentTweenIndex++];
      this.activeTween?.play();
    }
  }

  isAnimationPlaying() {
    return this.activeTween?.isPlaying() ?? false;
  }

  skip() {
    if (this.stopped) {
      return;
    }

    this.activeTween?.setTimeScale(100);
  }

}