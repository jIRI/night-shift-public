import { MainAnimationBuilder } from '../builders/main-animation';
import { SoundPlayerBuilder } from '../builders/sound-player';
import { DialogBox } from '../objects/dialog-box';
import { Speaker, SpeakerPosition, SpeakerState } from '../objects/speaker';
import { ActionConfig, ActionId, ActorId, Line, Script } from '../scripts/script';
import { getLocalizedText, LanguageRegistryKey } from '../types/lang';

export class MainScene extends Phaser.Scene {
  textBox: DialogBox | null = null;

  speakers = new Map<ActorId, Speaker>();
  robotLeftSpeaker: Speaker | null = null;
  robotRightSpeaker: Speaker | null = null;
  droneLeftSpeaker: Speaker | null = null;
  droneRightSpeaker: Speaker | null = null;
  managerSpeaker: Speaker | null = null;
  engineerSpeaker: Speaker | null = null;
  directorSpeaker: Speaker | null = null;
  dummySpeaker: Speaker | null = null;

  mouseDown: boolean = false;
  mainAnim = new MainAnimationBuilder();
  soundPlayer = new SoundPlayerBuilder();
  script = new Script();
  ignoreInput = false;
  waitForAnimation = false;

  constructor() {
    super({ key: 'MainScene', active: false });
  }

  preload(): void {
    this.createLoadingProgressBar();

    this.load.image('dialogContinue', 'images/dialog_continue.png');
    this.load.image('dialogContinueHighlight', 'images/dialog_continue_hl.png');
    this.load.image('dialogPanel', 'images/dialog_panel.png');
    this.load.image('dialogNameLeft', 'images/dialog_name_left.png');
    this.load.image('dialogNameRight', 'images/dialog_name_right.png');
    this.load.image('empty', 'images/empty.png');

    this.soundPlayer.preloadAssets(this);
    this.mainAnim.preloadAssets(this);
   }

  create(): void {
    const suitcaseY = 850;

    this.soundPlayer.createObjects(this);
    this.mainAnim.createObjects(this, this.soundPlayer);

    this.robotLeftSpeaker = new Speaker({
      scene: this,
      textureName: 'empty',
      initialState: SpeakerState.hidden,
      position: SpeakerPosition.leftBottom,
      activeScale: 1,
      inactiveScale: 1,
      originOffset: {x: 0, y: 0},
    });
    this.speakers.set(ActorId.robotLeft, this.robotLeftSpeaker);

    this.robotRightSpeaker = new Speaker({
      scene: this,
      textureName: 'empty',
      initialState: SpeakerState.hidden,
      position: SpeakerPosition.rightBottom,
      activeScale: 1,
      inactiveScale: 1,
      originOffset: {x: 0, y: 0},
    });
    this.speakers.set(ActorId.robotRight, this.robotRightSpeaker);

    this.droneLeftSpeaker = new Speaker({
      scene: this,
      initialState: SpeakerState.hidden,
      textureName: 'empty',
      position: SpeakerPosition.leftBottom,
      activeScale: 1,
      inactiveScale: 1,
      originOffset: {x: 0, y: 0},
    });
    this.speakers.set(ActorId.droneLeft, this.droneLeftSpeaker);

    this.droneRightSpeaker = new Speaker({
      scene: this,
      initialState: SpeakerState.hidden,
      textureName: 'empty',
      position: SpeakerPosition.rightBottom,
      activeScale: 1,
      inactiveScale: 1,
      originOffset: {x: 0, y: 0},
    });
    this.speakers.set(ActorId.droneRight, this.droneRightSpeaker);

    this.managerSpeaker = new Speaker({
      scene: this,
      initialState: SpeakerState.hidden,
      textureName: 'empty',
      position: SpeakerPosition.rightBottom,
      activeScale: 1,
      inactiveScale: 1,
      originOffset: {x: 0, y: 0},
    });
    this.speakers.set(ActorId.manager, this.managerSpeaker);

    this.engineerSpeaker = new Speaker({
      scene: this,
      initialState: SpeakerState.hidden,
      textureName: 'empty',
      position: SpeakerPosition.leftBottom,
      activeScale: 1,
      inactiveScale: 1,
      originOffset: {x: 0, y: 0},
    });
    this.speakers.set(ActorId.engineer, this.engineerSpeaker);

    this.directorSpeaker = new Speaker({
      scene: this,
      initialState: SpeakerState.hidden,
      textureName: 'empty',
      position: SpeakerPosition.rightBottom,
      activeScale: 1,
      inactiveScale: 1,
      originOffset: {x: 0, y: 0},
    });
    this.speakers.set(ActorId.director, this.directorSpeaker);


    this.textBox = new DialogBox({
      scene: this,
      textureNames: {
        nameLeft: 'dialogNameLeft',
        nameRight: 'dialogNameRight',
        box: 'dialogPanel',
        continue: 'dialogContinue',
        continueHighlighted: 'dialogContinueHighlight',
      }
    });

    this.registerInputHandlers();

    this.script.reset();
    this.tryGetNextLine();

    this.cameras.main.fadeIn(1000, 0, 0, 0)

    this.cameras.main.once(Phaser.Cameras.Scene2D.Events.FADE_OUT_COMPLETE, () => {
      this.mainAnim.stopAnimation();
      this.scene.start('OutroScene');
      this.scene.stop('MainScene');
    })
  }

  private registerInputHandlers() {
    this.ignoreInput = this.game.scene.isActive('MainScene');
    this.input.on('pointerup', () => {
      if (this.ignoreInput || (this.waitForAnimation && this.mainAnim.isAnimationPlaying())) {
        return;
      }
      this.mainAnim.skip();
      this.tryGetNextLine();
    }, this);

    this.input.keyboard.on('keydown', (event: any) => {
      if (this.ignoreInput || (this.waitForAnimation && this.mainAnim.isAnimationPlaying())) {
        return;
      }

      switch (event.keyCode) {
        case Phaser.Input.Keyboard.KeyCodes.SPACE:
        case Phaser.Input.Keyboard.KeyCodes.ENTER:
          this.mainAnim.skip();
          this.tryGetNextLine();
          break;
        default:
          break;
      }
    });
  }

  private tryGetNextLine() {
    const line = this.script.nextLine();
    if (line != null) {
      this.applyLine(line);
    } else {
      this.startTransition();
    }
  }

  private applyLine(line: Line) {
    let textBoxVisible = true;
    const activeActor = this.speakers.get(line.actorId);
    const inactiveActors = Array.from(this.speakers.values()).filter(i => i !== activeActor);

    if (line.actions?.find(action => action.do === ActionId.show)) {
      activeActor?.show(SpeakerState.inactive);
    }
    if (activeActor?.speakerState !== SpeakerState.active) {
      activeActor?.activate();
      inactiveActors
        .filter(a => a.speakerState === SpeakerState.active)
        .forEach(a => a.deactivate());
    }
    if (line.actions?.find(action => action.do === ActionId.hide)) {
      activeActor?.hide();
      textBoxVisible = false;
    }

    if( activeActor?.config.position === SpeakerPosition.rightBottom ) {
      this.textBox!.setCharacterNameRight(this.script.actor(line.actorId).name);
      this.textBox!.setLeftCharacterNameVisible(false);
      this.textBox!.setRightCharacterNameVisible(true);
    } else {
      // by default make at least left name visible
      this.textBox!.setCharacterNameLeft(this.script.actor(line.actorId).name);
      this.textBox!.setLeftCharacterNameVisible(true);
      this.textBox!.setRightCharacterNameVisible(false);
    }

    this.textBox!.setText(getLocalizedText(line.text, this.game.registry.get(LanguageRegistryKey) ?? 'cz'));
    this.textBox!.setVisible(textBoxVisible);

    if (line.actions != null && line.actions.length !== 0) {
      this.handleActions(line.actions);
    }
  }

  private handleActions(actions: Array<ActionConfig>) {
    let actionConfig: ActionConfig | undefined;
    actionConfig = actions.find(action => action.do === ActionId.playAnimation);
    if (actionConfig != null && actionConfig.do === ActionId.playAnimation) {
      this.mainAnim.startAnimation(this);
    }

    actionConfig = actions.find(action => action.do === ActionId.continueAnimation);
    if (actionConfig != null && actionConfig.do === ActionId.continueAnimation) {
      this.mainAnim.playNext();
    }

    actionConfig = actions.find(action => action.do === ActionId.finishAnimation);
    if (actionConfig != null && actionConfig.do === ActionId.finishAnimation) {
      this.mainAnim.stopAnimation();
    }

    actionConfig = actions.find(action => action.do === ActionId.enableAnimationSync);
    if (actionConfig != null && actionConfig.do === ActionId.enableAnimationSync) {
      this.waitForAnimation = true;
    }

    actionConfig = actions.find(action => action.do === ActionId.disableAnimationSync);
    if (actionConfig != null && actionConfig.do === ActionId.disableAnimationSync) {
      this.waitForAnimation = false;
    }

    actionConfig = actions.find(action => action.do === ActionId.playSound);
    if (actionConfig != null && actionConfig.do === ActionId.playSound) {
      this.soundPlayer.play(actionConfig.sound);
    }
  }

  private startTransition() {
    this.ignoreInput = true;
    this.cameras.main.fadeOut(1000, 0, 0, 0);
  }

  private createLoadingProgressBar() {
    const progressBar = this.add.graphics();
    const progressBox = this.add.graphics();
    progressBox.fillStyle(0x222222, 0.8);
    progressBox.fillRect(this.game.scale.width - 330, this.game.scale.height - 60, 320, 50);

    this.load.on('progress', (value: number) => {
      progressBar.clear();
      progressBar.fillStyle(0xffffff, 1);
      progressBar.fillRect(this.game.scale.width - 320, this.game.scale.height - 50, 300 * value, 30);
    });

    this.load.on('complete', () => {
      progressBar.destroy();
      progressBox.destroy();
    });
  }
}
