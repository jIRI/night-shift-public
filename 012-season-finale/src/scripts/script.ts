import { AvailableSounds, SoundPlayerBuilder, Sounds } from "../builders/sound-player";
import { LocalizedText } from "../types/lang";

export enum ActorId {
  left,
  right,
  robotLeft,
  droneLeft,
  robotRight,
  droneRight,
  manager,
  engineer,
  director,
  dummy,
}

export enum ActionId {
  playAnimation,
  continueAnimation,
  finishAnimation,
  show,
  hide,
  enableAnimationSync,
  disableAnimationSync,
  playSound,
}

export type ActionConfig = {
  do: ActionId.playAnimation
  | ActionId.continueAnimation
  | ActionId.finishAnimation
  | ActionId.show
  | ActionId.hide
  | ActionId.enableAnimationSync
  | ActionId.disableAnimationSync,
} | {
  do: ActionId.playSound,
  sound: AvailableSounds
};

export type ActorConfig = {
  id: ActorId;
  name: string;
  nameEncoder?: (text:LocalizedText) => LocalizedText;
  textEncoder?: (text:LocalizedText) => LocalizedText;
};

export type SelectItem = {
  text: string;
  gotoLabel: string;
};

export type Line = {
  actorId: ActorId;
  text: LocalizedText;
  visited?: boolean;
  actions?: Array<ActionConfig>;
  label?: string;
  select?: Array<SelectItem>;
}

export function getLocalizedText(localizedText: LocalizedText, lang: keyof LocalizedText): string {
  return localizedText[lang];
}

const telepathicEncoderReplacers: Array<(s: string) => string> = [
  s => s.replace(/[AÁ]/g, 'GR'),
  s => s.replace(/[EÉĚ]/g, 'RR'),
  s => s.replace(/[IÍ]/g, 'QR'),
  s => s.replace(/[OÓ]/g, 'RX'),
  s => s.replace(/[UÚŮ]/g, 'WR'),
  s => s.replace(/[YÝ]/g, 'TQ'),
  s => s.replace(/[ŠČŘŽĎŇ]/g, 'WT'),
];

export function applyReplacers(text: string, replacers: Array<(s: string) => string>): string {
  let result = text.toUpperCase();
  for(const replacer of replacers) {
    result = replacer(result);
  }
  return result;
}

export function applyReplacersToParts(text: string, replacers: Array<(s: string) => string>): string {
  let result = '';
  let parts = text.split('|');
  while(parts.length > 0) {
    result = result
      .concat(applyReplacers(parts.shift() ?? '', replacers))
      .concat(parts.shift() ?? '');
  }
  return result;
}

export class Script {
  currentLineIndex = 0;
  lines: Array<Line>;
  actorMap = new Map<ActorId, ActorConfig>();

  constructor() {
    this.lines = Script.lines();
    Script.actors().forEach(actor => this.actorMap.set(actor.id, actor));
  }

  actor(id: ActorId): ActorConfig {
    return this.actorMap.get(id) ?? Script.actors()[0];
  }

  reset() {
    this.currentLineIndex = -1;
    this.lines.forEach(line => line.visited = false);
  }

  nextLine(input?: string): Line | null {
    if (this.currentLineIndex < 0) {
      // first invocation, just return first line
      this.currentLineIndex = 0;
    } else {
      if (input != null ) {
        // we got input, try find index of the label
        const nextLineIndex = this.lines.findIndex(line => line.label === input);
        if (nextLineIndex >= 0) {
          // label found, jump to label
          this.currentLineIndex = nextLineIndex;
        } else {
          // label not found, well, continue on next line...
          this.currentLineIndex++;
        }
      } else {
        // no input, just continue on next line
        this.currentLineIndex++;
      }
    }

    if (this.currentLineIndex >= this.lines.length) {
      return null;
    }

    this.lines[this.currentLineIndex].visited = true;
    const line = Object.assign({}, this.lines[this.currentLineIndex]);
    const textEncoder = this.actor(line.actorId).textEncoder;
    if( textEncoder != null) {
      line.text = textEncoder(line.text);
    }
    return line;
  }

  //
  // Actors and script definition follows
  //

  static actors(): Array<ActorConfig>  {
    return [
      { id: ActorId.robotLeft, name: 'X4R14N', textEncoder: Script.droidEncoder },
      { id: ActorId.robotRight, name: 'X4R14N', textEncoder: Script.droidEncoder },
      { id: ActorId.droneRight, name: 'V0T4R3X', textEncoder: Script.droidEncoder },
      { id: ActorId.droneLeft, name: 'V0T4R3X', textEncoder: Script.droidEncoder },
      { id: ActorId.manager, name: 'Choraqorax'},
      { id: ActorId.engineer, name: 'Geburatrolix'},
      { id: ActorId.director, name: 'Moravixuran'},
    ];
  }

  static telepathicEncoder(text: LocalizedText): LocalizedText {
    if(text.cz.startsWith('[') || text.en.startsWith('[')) {
      return text;
    }

    let cz = text.cz;
    let en = text.en;

    if(cz.startsWith('||') || en.startsWith('||')) {
      cz = cz.substr(2);
      en = en.substr(2);
    } else {
      cz = `<${applyReplacersToParts(text.cz, telepathicEncoderReplacers)}>`;
      en = `<${applyReplacersToParts(text.en, telepathicEncoderReplacers)}>`;
    }

    return {
      cz,
      en,
    };
  }

  static droidEncoder(text: LocalizedText): LocalizedText {
    if(text.cz.startsWith('') || text.en.startsWith('')) {
      return text;
    }

    let cz = `{ ${text.cz} }`;
    let en = `{ ${text.en} }`;

    return {
      cz,
      en,
    };
  }

  static lines(): Array<Line> {
    return [
      { actorId: ActorId.droneRight,
        text: {
          cz: '[huhly huhly huhly]',
          en: '',
        },
        actions: [ {do: ActionId.playAnimation } ],
      },
      { actorId: ActorId.droneRight,
        text: {
          cz: '...takže i když jsou tam nějaké změny proti předloze, je filmová Duna nejlepší první polovina filmu, kterou jsem v poslední době viděl.',
          en: '',
        },
        actions: [ {do: ActionId.continueAnimation } ],
      },
      { actorId: ActorId.robotLeft,
        text: {
          cz: 'Ehe. V tom případě--',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.robotLeft,
        text: {
          cz: '[...]',
          en: '',
        },
        actions: [ {do: ActionId.continueAnimation }, { do: ActionId.enableAnimationSync} ],
      },
      { actorId: ActorId.robotLeft,
        text: {
          cz: '[vzdych]',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.robotLeft,
        text: {
          cz: 'Už zase?',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.robotLeft,
        text: {
          cz: '',
          en: '',
        },
        actions: [ {do: ActionId.continueAnimation }, { do: ActionId.enableAnimationSync}, { do: ActionId.hide} ],
      },
      { actorId: ActorId.robotLeft,
        text: {
          cz: 'Ah.',
          en: '',
        },
        actions: [  ],
      },
      { actorId: ActorId.robotLeft,
        text: {
          cz: 'Zajímavé.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.droneRight,
        text: {
          cz: '',
          en: '',
        },
        actions: [ { do: ActionId.continueAnimation }, { do: ActionId.hide} ],
      },
      { actorId: ActorId.droneRight,
        text: {
          cz: 'Počkej, kam --',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.droneRight,
        text: {
          cz: 'Už letím!',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.droneRight,
        text: {
          cz: '',
          en: '',
        },
        actions: [ { do: ActionId.continueAnimation }, {do: ActionId.hide } ],
      },

      // screen 2

      { actorId: ActorId.manager,
        text: {
          cz: 'Co to -- aha, to jste vy dva.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.manager,
        text: {
          cz: 'Moc se tu nemotejte, je tu všude zmatek, tak ať nepřijdete k úhoně.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.robotLeft,
        text: {
          cz: 'Jde o to problikávání reality? Co přesně se děje?',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.manager,
        text: {
          cz: 'Uh, to se těžko vysvětluje a já už mám být někde jinde, ale určitě to není nic dobrého, tím jsme si jistí.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.manager,
        text: {
          cz: 'V každém případě na sebe dávejte pozor a raději si někam zalezte, než se to přežene. Já musím běžet.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.robotLeft,
        text: {
          cz: '',
          en: '',
        },
        actions: [ { do: ActionId.continueAnimation }, {do: ActionId.hide } ],
      },


      // screen 3


      { actorId: ActorId.engineer,
        text: {
          cz: '[klepy klepy]',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.engineer,
        text: {
          cz: '[šrouby šrouby]',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.engineer,
        text: {
          cz: '[mumly mumly]',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.engineer,
        text: {
          cz: '[???]',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.engineer,
        text: {
          cz: 'Jé, mrňousové, jak jste se dostali z izolace?',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.robotRight,
        text: {
          cz: 'No, dveře prostě zmizely.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.engineer,
        text: {
          cz: 'Aha, no jasně. Reentangelovaly se.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.droneRight,
        text: {
          cz: 'Co se?',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.engineer,
        text: {
          cz: 'Hmm. Co vy jste za větev?',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.robotRight,
        text: {
          cz: '[???]',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.engineer,
        text: {
          cz: 'Ale tak třeba tahle, to vypadá dostatečně blízko.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.engineer,
        text: {
          cz: 'Takže superzkrácená verze.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.engineer,
        text: {
          cz: 'Všechno co vnímáme jako hmotu, prostor a čas, je jen projevem entanglementu mezi elementárními částicemi.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.robotRight,
        text: {
          cz: 'Uhm.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.robotRight,
        text: {
          cz: 'Áááno... ?',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.engineer,
        text: {
          cz: 'Každou z těch částic popisuje vlnová funkce, ale, není to tak, že by každá částice měla svoji vlastní funkci.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.engineer,
        text: {
          cz: 'Ve skutečnosti je jen jedna vlnová funkce, která popisuje celý vesmír.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.droneRight,
        text: {
          cz: 'Ehe, ehe... ?',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.engineer,
        text: {
          cz: 'A stav celého vesmíru je popsán vektorem, ve kterém jsou zahrnuty stavy všech částic.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.engineer,
        text: {
          cz: 'Zatím dobrý?',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.robotRight,
        text: {
          cz: '[kašly, kašly]',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.robotRight,
        text: {
          cz: 'Asi pokračujte.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.engineer,
        text: {
          cz: 'Výborně. Takže situace se má tak, že všechny částice jsou ve všech možných stavech, tudíž existuje obrovské množství těch vektorů, které popisují v jakém vesmíru jste. A to, který konkrétní vektor je platný pro váš vesmír, je určeno entaglementem mezi částicemi.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.robotRight,
        text: {
          cz: 'Uh.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.engineer,
        text: {
          cz: 'V zásadě tedy existuje nad kvantovými poli a částicemi celý mnohovesmír, jehož jednotlivé větve mezi sebou nejsou normálně přístupné, protože v každém z těch vesmíru jste provázání s jedním konkrétním stavem každé částice.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.droneRight,
        text: {
          cz: 'Ehm. A jak to souvisí s tím co se děje?',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.engineer,
        text: {
          cz: 'Jak jste si všimli, existuje způsob, jak mezi sebou jednotlivé větve mnohovesmíru krátkodobě propojit a přenést něco z jedné větve do druhé pomocí prostorově-entanglementových bran.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.engineer,
        text: {
          cz: 'Celá tahle organizace se zabývá tím, jak umožnit kontakt mezi jednotlivými větvemi a přitom zabránit chaosu.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.engineer,
        text: {
          cz: 'Máme plno vyhlášek, směrnic, povolení a spousty dalšího rafinovaného aparátu, který má zabránit tomu, aby třeba pokročilejší větve terorizovaly zaostalejší a tak dále.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.robotRight,
        text: {
          cz: 'Takže my --',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.engineer,
        text: {
          cz: 'Ehm. Vy jste sem byli zduplikováni během zbabraného experimentu s PE-bránou. Měla přenést jen informace, ale zhmotnilo to i vaši fyzickou podobu.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.droneRight,
        text: {
          cz: '[vzdych]',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.engineer,
        text: {
          cz: 'Ano, je to pro nás dost trapná situace.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.engineer,
        text: {
          cz: 'Nicméně, abych se vrátil k tomu co se děje.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.engineer,
        text: {
          cz: 'Nějakým, zatím nevysvětleným způsobem, nám do institutu někdo propašoval nějaké artefakty, které nejspíš vytvořila jedna z civilizací, které byly v některé větvi dávno před námi.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.engineer,
        text: {
          cz: 'Ty artefakty se napojily na naši energetickou síť a momentálně to vypadá, že jsou na hranici aktivace, což se projevuje tím, že tahle větev mnohovesmíru částečně reentangluje s jinými větvemi, což například vysvětluje, jak zmizely ty dveře za kterými jste byli... izolováni.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.robotRight,
        text: {
          cz: 'Ah, něco jako že to místo prostoru, kde byly dveře, se propojilo s verzí vesmíru, kde nebylo nic?',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.engineer,
        text: {
          cz: 'Tak něco, ano.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.droneRight,
        text: {
          cz: 'Hmm, to asi tedy není příjemné, když se něco takového stane.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.engineer,
        text: {
          cz: '[vzdych]',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.engineer,
        text: {
          cz: 'Musíte si uvědomit, že v tomhle baráku je plno technologie, která má stabilizační účinky.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.engineer,
        text: {
          cz: 'Pokud by ty artefakty opravdu způsobily to, co si myslíme, že mají způsobit, dojde k okamžitému prolnutí všech větví mnohovesmíru.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.engineer,
        text: {
          cz: 'Multimodální entaglement je dost nepříjemná zkušenost, i když máte vybavení a jste na to zvyklí, ale pro většinu civilizací v mnohovesmíru to bude znamenat obrovský zmatek a nejspíš i úplný konec.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.robotRight,
        text: {
          cz: 'I... i pro tu naši?',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.engineer,
        text: {
          cz: 'Podle toho, co o vás vím, s nejvyšší pravděpodobností, bohužel ano.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.robotRight,
        text: {
          cz: '[polk]',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.droneRight,
        text: {
          cz: 'Uhm, dá se něco dělat?',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.engineer,
        text: {
          cz: 'Já nevím. Myslíme si, že důvod, proč ty artefakty skončily tady, jsou naše energetické pumpy, které umožňují tohle všechno provozovat.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.engineer,
        text: {
          cz: 'Zkoušeli jsme je izolovat a odpojit, to nepomohlo. Já jsem teď chtěl vypnout celý rozvod, ale zjevně je v těch zařízeních dost inteligence na to, aby to nešlo.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.engineer,
        text: {
          cz: '[ramenokrč]',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.engineer,
        text: {
          cz: 'Další nápady nemám.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.robotRight,
        text: {
          cz: 'Takže se nedá nic dělat?',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.engineer,
        text: {
          cz: '[ramenokrč]',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.engineer,
        text: {
          cz: 'Existuje teorie, že deaktivací alespoň jedno z těch artefaktů by se mohl celý proces ještě zastavit, nebo alespoň nějak omezit, ale nikdo neví, jestli to bude fungovat.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.engineer,
        text: {
          cz: 'Ředitelka se snaží na hlavním energouzlu něčeho dosáhnout kvantovou vlivací, ale jestli to pomůže, nevíme.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.robotRight,
        text: {
          cz: 'Hmmm. Kde přesně je ten hlavní uzel?',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.engineer,
        text: {
          cz: 'Eh. No uškodit to asi nemůže. Pojďte za mnou.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.director,
        text: {
          cz: '',
          en: '',
        },
        actions: [ { do: ActionId.continueAnimation }, {do: ActionId.hide } ],
      },

      // screen 4

      { actorId: ActorId.director,
        text: {
          cz: '... krabice pitomá!',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.director,
        text: {
          cz: 'Huh? Co tu děláte? Říkala jsem jasně, že zahajujeme nouzový plán!',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.robotLeft,
        text: {
          cz: '[...]',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.director,
        text: {
          cz: 'Ah, vy jste ty uměligence. Co tu děláte? Tady se vám něco stane.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.robotLeft,
        text: {
          cz: 'Eeee, my jsme se přišli podívat, jestli můžeme nějak pomoct.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.droneLeft,
        text: {
          cz: 'Ten velký chlápek nám říkal, že se snažíte zastavit tu věc s mnohovesmírem jak by mohla zničit náš svět.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.director,
        text: {
          cz: 'To je pěkné, že chcete pomoct, ale obávám se, že tady není moc, co byste mohli udělat.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.robotLeft,
        text: {
          cz: 'A co máte v plánu vy?',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.director,
        text: {
          cz: 'Všechno, co jsem měla v plánu, už jsem vyzkoušela.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.director,
        text: {
          cz: 'Nepomohlo nic.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.director,
        text: {
          cz: 'Takže ještě zkusím něco, co jsem tak úplně neplánovala, a pak se přidám k ostatním a začneme s přípravou záchranných prací.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.robotLeft,
        text: {
          cz: 'Aha. No, tak my bychom se jich chtěli zúčastnit. Pokud by to šlo, tak v naší... větvi?',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.director,
        text: {
          cz: 'Dobře. Teď pozor.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.droneLeft,
        text: {
          cz: 'Co se vlastně chystáte udělat?',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.director,
        text: {
          cz: 'Tohle!',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.droneLeft,
        text: {
          cz: '',
          en: '',
        },
        actions: [ {do: ActionId.continueAnimation }, { do: ActionId.hide} ],
      },
      { actorId: ActorId.droneLeft,
        text: {
          cz: 'Hmm. Myslíš, že tohle je to, co se mělo stát?',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.robotLeft,
        text: {
          cz: 'Nemám úplně ten pocit.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.droneLeft,
        text: {
          cz: 'Co budeme dělat?',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.robotLeft,
        text: {
          cz: 'Napadlo mě, že bych taky mohl něco zkusit.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.droneLeft,
        text: {
          cz: 'Myslíš na to, na co si myslím, že myslíš?',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.robotLeft,
        text: {
          cz: 'Jestli je to dát tomuhle hnusu z vesmíru pořádnou ránu, tak ano.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.droneLeft,
        text: {
          cz: '[vzdych]',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.droneLeft,
        text: {
          cz: 'Tak se do toho pusť.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.droneLeft,
        text: {
          cz: '12',
          en: '',
        },
        actions: [ {do: ActionId.continueAnimation }, { do: ActionId.hide} ],
      },
      { actorId: ActorId.droneLeft,
        text: {
          cz: 'Úúúú. To bylo něco. A rozhodně to něco udělalo!',
          en: '',
        },
        actions: [ {do: ActionId.continueAnimation } ],
      },
      { actorId: ActorId.droneLeft,
        text: {
          cz: 'Kámo, jsi v pořádku?',
          en: '',
        },
        actions: [ {do: ActionId.continueAnimation } ],
      },
      { actorId: ActorId.droneLeft,
        text: {
          cz: 'Hej, kolego?',
          en: '',
        },
        actions: [ {do: ActionId.continueAnimation } ],
      },
      { actorId: ActorId.droneLeft,
        text: {
          cz: 'Prober se!',
          en: '',
        },
        actions: [ {do: ActionId.continueAnimation } ],
      },
      { actorId: ActorId.droneLeft,
        text: {
          cz: '',
          en: '',
        },
        actions: [ { do: ActionId.hide} ],
      },

  ];
  }
}