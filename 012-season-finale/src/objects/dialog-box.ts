
export type DialogBoxConfig = {
  scene: Phaser.Scene;
  textureNames: {
    box: string;
    nameLeft: string;
    nameRight: string;
    continue: string;
    continueHighlighted: string;
  };
};

export class DialogBox extends Phaser.GameObjects.Container {
  config: DialogBoxConfig;
  characterNameLeft: Phaser.GameObjects.Text;
  characterNameRight: Phaser.GameObjects.Text;
  text: Phaser.GameObjects.Text;
  imageBox: Phaser.GameObjects.Image;
  imageNameLeft: Phaser.GameObjects.Image;
  imageNameRight: Phaser.GameObjects.Image;
  imageContinue: Phaser.GameObjects.Image;
  imageContinueHighlight: Phaser.GameObjects.Image;
  revealTextTween: Phaser.Tweens.Tween;
  isLeftNameVisible: boolean = false;
  revealLeftNameTween: Phaser.Tweens.Tween;
  hideLeftNameTween: Phaser.Tweens.Tween;
  isRightNameVisible: boolean = false;
  revealRightNameTween: Phaser.Tweens.Tween;
  hideRightNameTween: Phaser.Tweens.Tween;
  continueTween: Phaser.Tweens.Tween;

  constructor(config: DialogBoxConfig) {
    super(config.scene);
    this.config = config;

    this.imageNameLeft = new Phaser.GameObjects.Image(this.scene, 0, 0, this.config.textureNames.nameLeft).setOrigin(0, 0).setAlpha(0);
    this.imageBox = new Phaser.GameObjects.Image(this.scene, 0.05 * this.imageNameLeft.width, 0.75 * this.imageNameLeft.height, this.config.textureNames.box).setOrigin(0, 0);
    this.imageNameRight = new Phaser.GameObjects.Image(this.scene, 1.008 * this.imageBox.width, 0, this.config.textureNames.nameRight).setOrigin(1, 0).setAlpha(0);
    this.imageContinue = new Phaser.GameObjects.Image(this.scene, this.imageBox.width, 0.84 * (this.imageNameLeft.height + this.imageBox.height), this.config.textureNames.continue).setOrigin(1, 1);
    this.imageContinueHighlight = new Phaser.GameObjects.Image(this.scene, this.imageBox.width, this.imageBox.height, this.config.textureNames.continueHighlighted).setOrigin(1, 1).setVisible(false);

    this.text = new Phaser.GameObjects.Text(
      this.scene,
      3 * this.imageBox.x, 1.1 * this.imageBox.y,
      '',
      {
        fontFamily: 'serif',
        fontSize: '3.4em',
        color: '#14EAE8',
        maxLines: 4,
        wordWrap: {
          width: 0.9 * this.imageBox.width,
        }
    }).setOrigin(0, 0);

    this.characterNameLeft = new Phaser.GameObjects.Text(
      this.scene,
      this.imageNameLeft.width / 2, 0.4 * this.imageNameLeft.height,
      '',
      {
        fontFamily: 'serif',
        fontSize: '4em',
        color: '#000000',
        maxLines: 1,
    }).setOrigin(0.5, 0.5).setAlpha(0);

    this.characterNameRight = new Phaser.GameObjects.Text(
      this.scene,
      this.imageNameRight.x - this.imageNameRight.width / 2, 0.4 * this.imageNameRight.height,
      '',
      {
        fontFamily: 'serif',
        fontSize: '4em',
        color: '#000000',
        maxLines: 1,
    }).setOrigin(0.5, 0.5).setAlpha(0);

    this.revealTextTween = this.scene.tweens.add({
      targets: this.text,
      alpha: { from: 0, to: 1 },
      ease: Phaser.Math.Easing.Cubic.In,
      duration: 200,
    }).on('complete', () => this.continueTween.restart());

    this.revealLeftNameTween = this.scene.tweens.add({
      paused: true,
      targets: [this.characterNameLeft, this.imageNameLeft],
      alpha: { from: 0, to: 1 },
      ease: Phaser.Math.Easing.Cubic.In,
      duration: 200,
    });

    this.hideLeftNameTween = this.scene.tweens.add({
      paused: true,
      targets: [this.characterNameLeft, this.imageNameLeft],
      alpha: { from: 1, to: 0 },
      ease: Phaser.Math.Easing.Cubic.Out,
      duration: 150,
    });

    this.revealRightNameTween = this.scene.tweens.add({
      paused: true,
      targets: [this.characterNameRight, this.imageNameRight],
      alpha: { from: 0, to: 1 },
      ease: Phaser.Math.Easing.Cubic.In,
      duration: 200,
    });

    this.hideRightNameTween = this.scene.tweens.add({
      paused: true,
      targets: [this.characterNameRight, this.imageNameRight],
      alpha: { from: 1, to: 0 },
      ease: Phaser.Math.Easing.Cubic.Out,
      duration: 150,
    });

    this.continueTween = this.scene.tweens.add({
      targets: this.imageContinue,
      alpha: { from: 0, to: 1 },
      ease: Phaser.Math.Easing.Linear,
      repeat: -1,
      duration: 1000,
      yoyo: true,
    });

    this.add(this.imageNameLeft);
    this.add(this.imageNameRight);
    this.add(this.imageBox);
    this.add(this.imageContinueHighlight);
    this.add(this.imageContinue);
    this.add(this.characterNameLeft);
    this.add(this.characterNameRight);
    this.add(this.text);

    this.setSize(this.imageBox.width, 0.95 * (this.imageNameLeft.height + this.imageBox.height));
    this.setScale(1, 1);

    this.scene.add.container((this.scene.game.scale.width - this.width ) / 2, (this.scene.game.scale.height - this.height), this);
  }

  setCharacterNameLeft(name: string) {
    this.characterNameLeft.setText(name);
  }

  setCharacterNameRight(name: string) {
    this.characterNameRight.setText(name);
  }

  setLeftCharacterNameVisible(visible: boolean) {
    if( this.isLeftNameVisible === visible ) {
      return;
    }

    this.isLeftNameVisible = visible;

    if( visible ) {
      if (this.revealLeftNameTween.paused === true) {
         this.revealLeftNameTween.resume();
      }
      this.revealLeftNameTween.restart();
    } else {
      if (this.hideLeftNameTween.paused === true) {
        this.hideLeftNameTween.resume();
      }
      this.hideLeftNameTween.restart();
    }
  }

  setRightCharacterNameVisible(visible: boolean) {
    if( this.isRightNameVisible === visible ) {
      return;
    }

    this.isRightNameVisible = visible;

    if( visible ) {
      if (this.revealRightNameTween.paused === true) {
        this.revealRightNameTween.resume();
     }
     this.revealRightNameTween.restart();
    } else {
      if (this.hideRightNameTween.paused === true) {
        this.hideRightNameTween.resume();
      }
      this.hideRightNameTween.restart();
    }
  }

  setText(text: string) {
    this.continueTween.stop();
    this.imageContinue.setAlpha(0);
    this.text.setText(text);
    this.revealTextTween.restart();
  }
}