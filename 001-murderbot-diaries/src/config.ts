import { IntroScene } from './scenes/intro-scene';
import { MainScene } from './scenes/main-scene';
import { OutroScene } from './scenes/outro-scene';
import WebFontLoaderPlugin from 'phaser3-rex-plugins/plugins/webfontloader-plugin.js';

export const GameConfig: Phaser.Types.Core.GameConfig = {
  title: 'NOČNÍ ŠICHTA -- EP.1: PRŮSAK',
  url: 'https://jiribrossmann.com',
  version: '1.0.0',
  width: 1459,
  height: 1080,
  backgroundColor: 0x000000,
  type: Phaser.AUTO,
  parent: 'game',
  physics: {
    default: 'arcade',
    arcade: {
      gravity: { y: 200 }
    }
  },
  scale: {
    autoCenter: Phaser.Scale.Center.CENTER_BOTH,
    mode: Phaser.Scale.FIT,
  },
  plugins: {
    global: [{
      key: 'rexWebFontLoader',
      plugin: WebFontLoaderPlugin,
      start: true
    },]
  },
  //scene: [OutroScene],
  scene: [IntroScene, MainScene, OutroScene],
};
