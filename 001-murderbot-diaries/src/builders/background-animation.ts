import { GameObjects } from "phaser";

export class BackgroundAnimationBuilder {
  monster: Phaser.GameObjects.Image | null = null;
  robotSprite: Phaser.GameObjects.Sprite | null = null;
  spell1Sprite: Phaser.GameObjects.Sprite | null = null;
  currentTweenIndex = 0;
  animationTweens = new Array<Phaser.Tweens.Tween>();
  robotStepSound: Phaser.Sound.BaseSound | null = null;
  robotBeepSound: Phaser.Sound.BaseSound | null = null;
  robotFiresSound: Phaser.Sound.BaseSound | null = null;
  monsterSpellSound: Phaser.Sound.BaseSound | null = null;
  stopped = false;


  preloadAssets(scene: Phaser.Scene) {
    scene.load.image('mid', 'images/mid.png');
    scene.load.atlas('robot', 'images/black_robot.png', 'images/black_robot.json');
    scene.load.atlas('spell1', 'images/spell1.png', 'images/spell1.json');
    scene.load.audio('robotStep', 'audio/robot-step.ogg');
    scene.load.audio('robotBeep', 'audio/robot-beep.ogg');
    scene.load.audio('robotFires', 'audio/robot-fires.ogg');
    scene.load.audio('monsterSpell', 'audio/monster-spell.mp3');
  }

  createObjects(scene: Phaser.Scene) {
    const tintColor = 0x707070;
    const actorsY = 480;
    const speedModifier = 1;

    this.spell1Sprite = scene.add.sprite(0, actorsY, 'spell1').setScale(1.2, 1.2).setOrigin(0.5, 0.75).setVisible(false);
    scene.anims.create({
      key: 'cast',
      frames: scene.anims.generateFrameNames('spell1', { prefix: 'spell1_', start: 0, end: 24 }),
      frameRate: 15,
    });

    this.robotSprite = scene.add.sprite(0, actorsY, 'robot').setScale(0.18, 0.18).setTint(tintColor).setOrigin(0.5, 1).setVisible(false);
    scene.anims.create({
      key: 'idle',
      frames: scene.anims.generateFrameNames('robot', { prefix: 'Idle_', start: 0, end: 11 }),
      frameRate: 12,
      repeat: -1,
    });
    scene.anims.create({
      key: 'walk',
      frames: scene.anims.generateFrameNames('robot', { prefix: 'Walk_', start: 0, end: 11 }),
      frameRate: 12,
      repeat: -1,
    });
    scene.anims.create({
      key: 'singleShot',
      frames: scene.anims.generateFrameNames('robot', { prefix: 'SingleShot_', start: 0, end: 15 }),
      frameRate: 16,
    });

    this.robotStepSound = scene.sound.add('robotStep', {
      volume: 0.01,
      loop: true,
      rate: 3  ,
    });
    this.robotBeepSound = scene.sound.add('robotBeep', {
      volume: 0.08
    });
    this.robotFiresSound = scene.sound.add('robotFires', {
      volume: 0.1
    });
    this.monsterSpellSound = scene.sound.add('monsterSpell', {
      volume: 0.07
    });

    this.monster = scene.add.image(0, actorsY, 'mid').setScale(0.25, 0.25).setOrigin(0.5, 1).setTint(tintColor).setVisible(false);
    // monster's vertical wobble
    scene.add.tween({
      targets: this.monster,
      y: { from: 480, to: 483 },
      ease: Phaser.Math.Easing.Sine.InOut,
      repeat: -1,
      duration: 2000,
      yoyo: true,
    });

    // monster moves from left to right
    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.monster,
      x: scene.scale.width + this.monster.width,
      duration: speedModifier * 10000,
      onStart: () => this.playNext(),
    }));
    // robot moves from left to right
    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.robotSprite,
      x: scene.scale.width + this.monster.width,
      delay: 2000,
      duration: speedModifier * 10000,
      onStart: () => {
        this.robotSprite?.play('walk');
        this.robotStepSound?.play();
      },
      onComplete: () => {
        this.robotStepSound?.stop();
        this.playNext();
      },
    }));
    // monster moves from right to left
    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.monster,
      x: -this.monster.width,
      duration: speedModifier * 12000,
      onStart: () => {
        this.monster?.setFlipX(true);
        this.playNext();
      },
    }));
    // robot moves from right to left
    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.robotSprite,
      x: -this.monster.width,
      delay: 2000,
      duration: speedModifier * 12000,
      onStart: () => {
        this.robotSprite?.setFlipX(true);
        this.robotStepSound?.play();
      },
      onComplete: () => {
        this.robotStepSound?.stop();
        this.playNext();
      },
    }));
    // monster moves half way from right to left
    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.monster,
      x: 0.66 * scene.scale.width,
      duration: speedModifier * 8000,
      onStart: () => {
        this.monster?.setFlipX(false);
        this.playNext();
      },
    }));
    // robot moves half way from right to left
    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.robotSprite,
      x: 0.4 * scene.scale.width,
      delay: 2000,
      duration: speedModifier * 6000,
      onStart: () => {
        this.robotSprite?.setFlipX(false);
        this.robotStepSound!.play();
      },
      onComplete: () => {
        this.monster?.setFlipX(true);
        this.robotStepSound?.stop();
        this.robotSprite?.play('idle');
        this.robotBeepSound?.play();
        this.playNext();
      },
    }));
    // robot fires a shot at monster
    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.robotSprite,
      x: 0.4 * scene.scale.width,
      delay: 2000,
      duration: speedModifier * 1000,
      onStart: () => {
        this.robotFiresSound?.play();
        this.robotSprite?.play({
          key: 'singleShot',
          delay: 100,
        });
      },
      onComplete: () => {
        this.robotSprite?.play('idle');
        this.playNext();
      },
    }));
    // monster casts a spell
    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.monster,
      x: { from: 0.66 * scene.scale.width - 10, to:  0.66 * scene.scale.width + 10},
      ease: Phaser.Math.Easing.Sine.InOut,
      delay: 700,
      repeat: 10,
      duration: speedModifier * 50,
      yoyo: true,
      onStart: () => this.monsterSpellSound?.play(),
      onComplete: () => this.playNext(),
    }));
    // spell plays under the robot
    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.robotSprite,
      alpha: 0,
      delay: 600,
      duration: speedModifier * 2500,
      ease: Phaser.Math.Easing.Expo.Out,
      onStart: () => this.spell1Sprite?.setX(0.4 * scene.scale.width).setVisible(true).play('cast'),
      onComplete: () => this.playNext(),
    }));
    // monster moves from position to the right
    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.monster,
      x: scene.scale.width + this.monster.width,
      duration: speedModifier * 6000,
      onStart: () => this.monster?.setFlipX(false),
    }));
 }

  startAnimation() {
    this.stopped = false;
    this.monster?.setX(-2 * this.robotSprite!.width).setVisible(true);
    this.robotSprite?.setX(-2 * this.robotSprite?.width).setVisible(true);
    this.currentTweenIndex = 0;
    this.playNext();
  }

  stopAnimation() {
    this.stopped = true;
    this.animationTweens.forEach(t => t.stop());
    this.robotStepSound?.stop();
    this.robotBeepSound?.stop();
    this.robotFiresSound?.stop();
    this.monsterSpellSound?.stop();
  }

  playNext() {
    if( this.stopped ) {
      return;
    }

    this.animationTweens[this.currentTweenIndex++].play();
  }
}