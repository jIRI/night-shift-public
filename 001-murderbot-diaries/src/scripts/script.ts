import { LocalizedText } from "../types/lang";

export enum ActorId {
  left,
  right
}

export enum ActionId {
  playBackgroundAnimation,
  show,
  hide,
}

export type ActorConfig = {
  id: ActorId;
  name: string;
};

export type SelectItem = {
  text: string;
  gotoLabel: string;
};

export type Line = {
  actorId: ActorId;
  text: LocalizedText;
  visited?: boolean;
  actions?: Array<ActionId>;
  label?: string;
  select?: Array<SelectItem>;
}

export class Script {
  currentLineIndex = 0;
  lines: Array<Line>;
  actorMap = new Map<ActorId, ActorConfig>();

  constructor() {
    this.lines = Script.lines();
    Script.actors().forEach(actor => this.actorMap.set(actor.id, actor));
  }

  actor(id: ActorId): ActorConfig {
    return this.actorMap.get(id) ?? Script.actors()[0];
  }

  reset() {
    this.currentLineIndex = -1;
    this.lines.forEach(line => line.visited = false);
  }

  nextLine(input?: string): Line | null {
    if (this.currentLineIndex < 0) {
      // first invocation, just return first line
      this.currentLineIndex = 0;
    } else {
      if (input != null ) {
        // we got input, try find index of the label
        const nextLineIndex = this.lines.findIndex(line => line.label === input);
        if (nextLineIndex >= 0) {
          // label found, jump to label
          this.currentLineIndex = nextLineIndex;
        } else {
          // label not found, well, continue on next line...
          this.currentLineIndex++;
        }
      } else {
        // no input, just continue on next line
        this.currentLineIndex++;
      }
    }

    if (this.currentLineIndex >= this.lines.length) {
      return null;
    }

    const line = this.lines[this.currentLineIndex];
    line.visited = true;
    return line;
  }

  //
  // Actors and script definition follows
  //

  static actors(): Array<ActorConfig>  {
    return [
      { id: ActorId.left, name: 'Yoraggoraxuros' },
      { id: ActorId.right, name: 'Zarxerogarus' },
    ];
  }

   static lines(): Array<Line> {
    return [
      { actorId: ActorId.left,
        text: {
          cz: 'Ah, buď zdráv.',
          en: '',
      }},
      { actorId: ActorId.left,
        text: {
          cz: 'Taky na noční?',
          en: '',
      }},
      { actorId: ActorId.right,
        text: {
          cz: 'Tak, tak. Jako obvykle.',
          en: '',
      }},
      { actorId: ActorId.right,
        text: {
          cz: 'Co ty? Jak u vás v archívu?',
          en: '',
      }},
      { actorId: ActorId.right,
        text: {
          cz: 'Prý vám zase něco prosáklo z pátého.',
          en: '',
      }},
      { actorId: ActorId.left,
        text: {
          cz: 'Je to tak.',
          en: '',
      }},
      { actorId: ActorId.left,
        text: {
          cz: 'Ti unosomatičtí chytráci z výzkumu si zase hráli s PE-bránami a z druhé strany jim prolezly nějaké mobilní uměligence.',
          en: '',
      }},
      { actorId: ActorId.left,
        text: {
          cz: 'Něco se dostalo až k nám a odpolední směna se z toho mohla zbláznit.',
          en: '',
      }},
      { actorId: ActorId.left,
        text: {
          cz: 'Ale už je klid.',
          en: '',
      }},
      { actorId: ActorId.right,
        text: {
          cz: 'Takže jste to pochytali všechno?',
          en: '',
      }},
      { actorId: ActorId.left,
        text: {
          cz: 'Ó, to né. Pár kousků pořád běhá po baráku a ti moulové se tu nimi honí.',
          en: '',
        },
        actions: [ActionId.playBackgroundAnimation],
      },
      { actorId: ActorId.left,
        text: {
          cz: '[...]',
          en: '',
      }},
      { actorId: ActorId.right,
        text: {
          cz: '[...]',
          en: '',
      }},
      { actorId: ActorId.right,
        text: {
          cz: 'Co jinak? Nějaké novinky? Však víš...',
          en: '',
      }},
      { actorId: ActorId.left,
        text: {
          cz: 'Když se ptáš, revidoval jsem teď nějaké nové vzorky, a narazil jsem na zajímavé kousky.',
          en: '',
      }},
      { actorId: ActorId.right,
        text: {
          cz: 'Ale?',
          en: '',
      }},
      { actorId: ActorId.left,
        text: {
          cz: 'Třeba tyhle Murderbot Diaries.',
          en: '',
      }},
      { actorId: ActorId.right,
        text: {
          cz: 'Hmm, neznám, povídej.',
          en: '',
      }},
      { actorId: ActorId.left,
        text: {
          cz: 'Takže: vezmeš pořádkového droida, jako máte u vás na dvanáctém.',
          en: '',
      }},
      { actorId: ActorId.left,
        text: {
          cz: 'Odstraníš mu motivátor.',
          en: '',
      }},
      { actorId: ActorId.left,
        text: {
          cz: 'Přidáš nesmyslně velké množství výpočetního výkonu.',
          en: '',
      }},
      { actorId: ActorId.left,
        text: {
          cz: 'A takový jako velmi svérázný, sebeironický smysl pro humor.',
          en: '',
      }},
      { actorId: ActorId.left,
        text: {
          cz: 'A pak napíšeš sérii krátkých, svižných a místy opravdu vtipných knih.',
          en: '',
      }},
      { actorId: ActorId.right,
        text: {
          cz: 'Vtipný pořádkový droid bez motivátoru? Když jsme čtyři cykly zpátky sanovali tu vadnou sérii, moc jsme se nenasmáli.',
          en: '',
      }},
      { actorId: ActorId.left,
        text: {
          cz: 'Jasně, ale to je to kouzlo té jejich fikce -- i úplnou blbost dokáží prodat jako potenciálně možnou.',
          en: '',
      }},
      { actorId: ActorId.right,
        text: {
          cz: 'Uh, dobře. Takže jsou to nějaké komické romány?',
          en: '',
      }},
      { actorId: ActorId.left,
        text: {
          cz: 'Nene, rozsah je v takovém to ideálním bodě mezi dlouhou povídkou a novelou, a snad kromě pátého dílu jsou všechny do 200 stran.',
          en: '',
      }},
      { actorId: ActorId.left,
        text: {
          cz: 'A neřekl bych přímo komické, spíš jako na správných místech velmi vtipné.',
          en: '',
      }},
      { actorId: ActorId.left,
        text: {
          cz: 'Jinak samozřejmě je to takové to klasické "na pozadí akčního příběhu kritizujeme společenské uspořádání a rozkrýváme v čem spočívá lidství".',
          en: '',
      }},
      { actorId: ActorId.left,
        text: {
          cz: 'Taky trochu "krutý, neregulovaný, super-korporátní kapitalismus versus utopie každý-podle-svých-možností, každému-podle-jeho-potřeb".',
          en: '',
      }},
      { actorId: ActorId.left,
        text: {
          cz: 'A spousta referencí na xenofobii a otrokářství a tak podobně.',
          en: '',
      }},
      { actorId: ActorId.left,
        text: {
          cz: 'Ale. Nic z toho nikdy nepřejde do otravného mentorování a moralizování, spíš je to jako varování.',
          en: '',
      }},
      { actorId: ActorId.left,
        text: {
          cz: 'Pokud si nedáte pozor, takhle dopadnete. Ale s přiměřenou vrstvou sebeironie navrch. Tak něco.',
          en: '',
      }},
      { actorId: ActorId.right,
        text: {
          cz: 'Chm. No nevím, nevím. Moc to nezní jako můj šálek čaje.',
          en: '',
      }},
      { actorId: ActorId.left,
        text: {
          cz: 'Hele, to jsem se taky bál, ale jsou to rychlovky, odsejpá to a podle mě to v zásadě vždycky končí na optimistickou notu. Doporučuji zkusit třeba první dva díly a uvidíš.',
          en: '',
      }},
      { actorId: ActorId.left,
        text: {
          cz: 'Já jsem tím prokládal vyplňování kvartálního hlášení, a po třetím dílu jsem se rozhodl to dočíst najednou celé. Kašlat na kvartální hlášení.',
          en: '',
      }},
      { actorId: ActorId.right,
        text: {
          cz: 'Dobrá, dobrá. Tak mi to nějak nenápadně pošoupni. Zkusím, uvidím.',
          en: '',
      }},
      { actorId: ActorId.left,
        text: {
          cz: '[šoup]',
          en: '',
      }},
      { actorId: ActorId.right,
        text: {
          cz: '[chmát]',
          en: '',
      }},
      { actorId: ActorId.right,
        text: {
          cz: '[...]',
          en: '',
      }},
      { actorId: ActorId.right,
        text: {
          cz: 'No hele, musím běžet. Tak teda zatím.',
          en: '',
      }},
      { actorId: ActorId.left,
        text: {
          cz: 'Jasně. Měj se.',
          en: '',
      }},
      { actorId: ActorId.left,
        text: {
          cz: 'Ať ti to ubíhá.',
          en: '',
      }},
      { actorId: ActorId.right,
        text: {
          cz: '',
          en: '',
        },
        actions: [ActionId.hide],
      },
      { actorId: ActorId.left,
        text: {
          cz: '',
          en: '',
        },
        actions: [ActionId.hide],
      },
    ];
  }

}