export class Sounds {
  teleportSpell: Phaser.Sound.BaseSound | null = null;
  seGateCycle: Phaser.Sound.BaseSound | null = null;
}

export type AvailableSounds = keyof Sounds;

export class SoundPlayerBuilder extends Sounds {
  preloadAssets(scene: Phaser.Scene) {
    scene.load.audio('spell-teleport-sound', 'audio/spell-teleport.mp3');
    scene.load.audio('se-gate-sound', 'audio/se-gate.mp3');
  }

  createObjects(scene: Phaser.Scene) {
    this.teleportSpell = scene.sound.add('spell-teleport-sound', {
      volume: 0.05,
      rate: 1,
    });
    this.seGateCycle = scene.sound.add('se-gate-sound', {
      volume: 0.05,
      rate: 1.1,
    });
  }

  play(key: AvailableSounds) {
    switch(key) {
      case 'teleportSpell':
        this.teleportSpell?.play();
        return;
      case 'seGateCycle':
        this.seGateCycle?.play();
        return;
      default:
        return;
    }
  }
}