import { SoundPlayerBuilder } from "./sound-player";

export class BackgroundAnimationBuilder {
  soundPlayer: SoundPlayerBuilder | null = null;

  currentTweenIndex = 0;
  animationTweens = new Array<Phaser.Tweens.Tween>();
  activeTween: Phaser.Tweens.Tween | null = null;
  stopped = false;

  backgroundOverride: Phaser.GameObjects.Image | null = null;

  swordsmanSprite: Phaser.GameObjects.Sprite | null = null;
  teleportSpellSprite: Phaser.GameObjects.Sprite | null = null;

  preloadAssets(scene: Phaser.Scene) {
    scene.load.image('background-override', 'images/background-2.png');

    scene.load.spritesheet('swordsman-hurt', 'images/swordsman-hurt.png', { frameWidth: 154.5, frameHeight: 122 });

    scene.load.spritesheet('spell-teleport', 'images/spell-teleport.png', { frameWidth: 192, frameHeight: 192 });

  }

  createObjects(scene: Phaser.Scene, soundPlayer: SoundPlayerBuilder) {
    this.soundPlayer = soundPlayer;

    const tintColor = 0xaa0000;
    const speedModifier = 1;

    const swordsmanY = 420;
    const swordsmanLevitatingY = 310;
    const swordsmanX = 730;

    this.backgroundOverride = scene.add.image(0, 0, 'background-override').setOrigin(0, 0).setDepth(-5).setVisible(false);

    this.swordsmanSprite = scene.add.sprite(swordsmanX, swordsmanY, 'swordsman').setScale(0.6, 0.6).setOrigin(0.5, 1).setDepth(0).setTint(tintColor).setVisible(false);
    scene.anims.create({
      key: 'swordsman-hurt',
      frames: scene.anims.generateFrameNames('swordsman-hurt', { start: 0, end: 14 }),
      frameRate: 15,
      repeat: -1,
    });

    this.teleportSpellSprite = scene.add.sprite(swordsmanX, swordsmanY - 50, 'spell-teleport').setScale(1.5, 1.5).setOrigin(0.5, 0.5).setDepth(-1).setVisible(false);
    scene.anims.create({
      key: 'spell-teleport',
      frames: scene.anims.generateFrameNames('spell-teleport', { start: 0, end: 47 }),
      frameRate: 30,
      repeat: 0,
    });


    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.swordsmanSprite,
      alpha: { from: 0, to: 1},
      ease: Phaser.Math.Easing.Quadratic.In,
      duration: speedModifier * 2000,
      onStart: () => {
        soundPlayer.play('teleportSpell');
        this.teleportSpellSprite?.setVisible(true);
        this.teleportSpellSprite?.play('spell-teleport');
        this.swordsmanSprite?.play('swordsman-hurt');
        this.swordsmanSprite?.setVisible(true);
      },
      onComplete: () => {
        this.teleportSpellSprite?.setVisible(false);
      },
    }));

    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.backgroundOverride,
      alpha: { from: 0, to: 1},
      duration: speedModifier * 800,
      onStart: () => {
        soundPlayer.play('seGateCycle');
        this.backgroundOverride?.setVisible(true);
        this.playNext();
      },
      onComplete: () => {
      },
    }));

    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.swordsmanSprite,
      alpha: { from: 1, to: 0},
      y: swordsmanLevitatingY,
      ease: Phaser.Math.Easing.Quadratic.Out,
      duration: speedModifier * 800,
      onStart: () => {
        this.swordsmanSprite?.play('swordsman-hurt');
      },
      onComplete: () => {
        this.swordsmanSprite?.setVisible(false);
        this.playNext();
      },
    }));


    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.backgroundOverride,
      alpha: { from: 1, to: 0},
      duration: speedModifier * 800,
      delay: speedModifier * 200,
      onStart: () => {
      },
      onComplete: () => {
        this.backgroundOverride?.setVisible(false);
      },
    }));
  }

  startAnimation(scene: Phaser.Scene) {
    this.stopped = false;
    this.currentTweenIndex = 0;
    this.animationTweens.forEach(t => t.setTimeScale(1));

    this.playNext();
  }


  stopAnimation() {
    this.stopped = true;
    this.animationTweens.forEach(tween => {
      tween.stop();
      tween.setTimeScale(1);
    });
  }

  playNext() {
    if (this.stopped) {
      return;
    }

    if (this.currentTweenIndex < this.animationTweens.length) {
      this.activeTween = this.animationTweens[this.currentTweenIndex++];
      this.activeTween?.play();
    }
  }

  isAnimationPlaying() {
    return this.activeTween?.isPlaying() ?? false;
  }

  skip() {
    if (this.stopped) {
      return;
    }

    this.activeTween?.setTimeScale(100);
  }

}