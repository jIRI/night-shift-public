import { AvailableSounds } from "../builders/sound-player";
import { LocalizedText } from "../types/lang";

export enum ActorId {
  left,
  right,
  robot,
  drone,
  dummy,
}

export enum ActionId {
  playAnimation,
  continueAnimation,
  finishAnimation,
  show,
  hide,
  enableAnimationSync,
  disableAnimationSync,
  playSound,
}

export type ActionConfig = {
  do: ActionId.playAnimation
  | ActionId.continueAnimation
  | ActionId.finishAnimation
  | ActionId.show
  | ActionId.hide
  | ActionId.enableAnimationSync
  | ActionId.disableAnimationSync,
} | {
  do: ActionId.playSound,
  sound: AvailableSounds
};

export type ActorConfig = {
  id: ActorId;
  name: string;
  nameEncoder?: (text:LocalizedText) => LocalizedText;
  textEncoder?: (text:LocalizedText) => LocalizedText;
};

export type SelectItem = {
  text: string;
  gotoLabel: string;
};

export type Line = {
  actorId: ActorId;
  text: LocalizedText;
  visited?: boolean;
  actions?: Array<ActionConfig>;
  label?: string;
  select?: Array<SelectItem>;
}

export function getLocalizedText(localizedText: LocalizedText, lang: keyof LocalizedText): string {
  return localizedText[lang];
}

const telepathicEncoderReplacers: Array<(s: string) => string> = [
  s => s.replace(/[AÁ]/g, 'GR'),
  s => s.replace(/[EÉĚ]/g, 'RR'),
  s => s.replace(/[IÍ]/g, 'QR'),
  s => s.replace(/[OÓ]/g, 'RX'),
  s => s.replace(/[UÚŮ]/g, 'WR'),
  s => s.replace(/[YÝ]/g, 'TQ'),
  s => s.replace(/[ŠČŘŽĎŇ]/g, 'WT'),
];

export function applyReplacers(text: string, replacers: Array<(s: string) => string>): string {
  let result = text.toUpperCase();
  for(const replacer of replacers) {
    result = replacer(result);
  }
  return result;
}

export function applyReplacersToParts(text: string, replacers: Array<(s: string) => string>): string {
  let result = '';
  let parts = text.split('|');
  while(parts.length > 0) {
    result = result
      .concat(applyReplacers(parts.shift() ?? '', replacers))
      .concat(parts.shift() ?? '');
  }
  return result;
}

export class Script {
  currentLineIndex = 0;
  lines: Array<Line>;
  actorMap = new Map<ActorId, ActorConfig>();

  constructor() {
    this.lines = Script.lines();
    Script.actors().forEach(actor => this.actorMap.set(actor.id, actor));
  }

  actor(id: ActorId): ActorConfig {
    return this.actorMap.get(id) ?? Script.actors()[0];
  }

  reset() {
    this.currentLineIndex = -1;
    this.lines.forEach(line => line.visited = false);
  }

  nextLine(input?: string): Line | null {
    if (this.currentLineIndex < 0) {
      // first invocation, just return first line
      this.currentLineIndex = 0;
    } else {
      if (input != null ) {
        // we got input, try find index of the label
        const nextLineIndex = this.lines.findIndex(line => line.label === input);
        if (nextLineIndex >= 0) {
          // label found, jump to label
          this.currentLineIndex = nextLineIndex;
        } else {
          // label not found, well, continue on next line...
          this.currentLineIndex++;
        }
      } else {
        // no input, just continue on next line
        this.currentLineIndex++;
      }
    }

    if (this.currentLineIndex >= this.lines.length) {
      return null;
    }

    this.lines[this.currentLineIndex].visited = true;
    const line = Object.assign({}, this.lines[this.currentLineIndex]);
    const textEncoder = this.actor(line.actorId).textEncoder;
    if( textEncoder != null) {
      line.text = textEncoder(line.text);
    }
    return line;
  }

  //
  // Actors and script definition follows
  //

  static actors(): Array<ActorConfig>  {
    return [
      { id: ActorId.left, name: applyReplacers('Unilektiv', telepathicEncoderReplacers), textEncoder: Script.telepathicEncoder },
      { id: ActorId.right, name: 'Umakvacelok', textEncoder: Script.telepathicEncoder },
    ];
  }

  static telepathicEncoder(text: LocalizedText): LocalizedText {
    if(text.cz.startsWith('[') || text.en.startsWith('[')) {
      return text;
    }

    let cz = text.cz;
    let en = text.en;

    if(cz.startsWith('||') || en.startsWith('||')) {
      cz = cz.substr(2);
      en = en.substr(2);
    } else {
      cz = `<${applyReplacersToParts(text.cz, telepathicEncoderReplacers)}>`;
      en = `<${applyReplacersToParts(text.en, telepathicEncoderReplacers)}>`;
    }

    return {
      cz,
      en,
    };
  }

  static droidEncoder(text: LocalizedText): LocalizedText {
    if(text.cz.startsWith('') || text.en.startsWith('')) {
      return text;
    }

    let cz = `{ ${text.cz} }`;
    let en = `{ ${text.en} }`;

    return {
      cz,
      en,
    };
  }

  static lines(): Array<Line> {
    return [
      { actorId: ActorId.right,
        text: {
          cz: '[mumly mumly]',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.right,
        text: {
          cz: '||Hm?',
          en: '',
        },
        actions: [{ do: ActionId.playAnimation} ],
      },
      { actorId: ActorId.right,
        text: {
          cz: '||Hybaj domů.',
          en: '',
        },
        actions: [{ do: ActionId.continueAnimation} ],
      },
      { actorId: ActorId.right,
        text: {
          cz: '[mumly mumly]',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Ehm. Dobrý den.',
          en: '',
        },
        actions: [ { do: ActionId.show } ],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Ach, dobrý den i vám!',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Prý jste nás| chtěli vidět.| Jak se vám| vede?',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.right,
        text: {
          cz: '|Děkuji,| že jste si na mě| našli čas!',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.right,
        text: {
          cz: '|A mám se skvěle!',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: '|Uhm. Už... už zakládáte vlastní kolektiv?',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.right,
        text: {
          cz: '|Oh, ne. Zatím jsem vývoj předvědomí zablokoval a chci nejdřív prozkoumat existenci jako individualita.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: '|To je velmi... neortodoxní.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.right,
        text: {
          cz: '|Já vím, ale je to velmi poučná zkušenost, mít v těle jen jedno vědomí.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: '|Neděsí vás ta... |samota?',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.right,
        text: {
          cz: '|Nebudu lhát, někdy je to emocionálně značně náročné, ale domnívám se, že je dobré získat i tuto zkušenost.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: '|Rozumíme vědecké přínosnosti prozkoumání jednovědomé existence, ale je to pro nás děsivá představa, existovat v osamění.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: '|Je to jako z knihy Kazuo Ishigury.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.right,
        text: {
          cz: '|Knihy? Neříkejte, že jste se dali na čtení pozemské fikce.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: '|Po tom, co jste... se oddělili, jsme na tom nebyli úplně nejlépe, tak jsme zkoušeli najít útěchu v pozemské literatuře.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: '|Ale nějakým nedopatřením jsme si místo nějaké neškodné, náladu zlepšující knihy půjčili Never Let Me Go a Klara and the Sun.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: '|A jejich přečtení nám zcela určitě duševní pohodu nenavodilo.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.right,
        text: {
          cz: '|Ale? Jsou to špatné knihy?',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: '|Ne, to ne. Jsou to naopak moc dobré knihy, ale... Hmm.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: '|Obě jsou si podobné v tom, že zkoumají podstatu lidství, existenci věcí jako je lidská duše, kdo ji má a kdo ne a tak podobně. Obě se odehrávají v nějaké blízké budoucnosti, kde věci zcela zjevně nejdou správným směrem, a v obou se nevinným stvořením dějí hrozné věci.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: '|A protože jsou ta stvoření tak nevinná, a neočekávají od nikoho nic zlého, neuvědomují si jak hrozná je jejich situace.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: '|Je to tak strašlivě...| depresivní.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.right,
        text: {
          cz: '|Takže jsou to dobré knihy, které jsou hrozné?',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: '|Ano, něco v tom smyslu. Pořád se to snažíme zpracovat, ale obáváme se, že nemáme emoční konfiguraci, která by nám umožnila číst tak hrozné věci a přitom si zachovat poklidný duševní život.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.right,
        text: {
          cz: '|Upřímně, mám stejnou obavu, proto jsem s pozemskou literaturou zatím neexperimentoval, ale pokud říkáte, že tyhle dvě knihy jsou dobré, zkusím si je přečíst.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.right,
        text: {
          cz: '|Mohli bychom pak |sdílet| své úvahy podrobněji. Trochu jako za starých časů...',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: '|Ano! To zní jako výtečný nápad! Prosím, přečtěte si ty knihy a pak je budeme |sdílet|!',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.right,
        text: {
          cz: '|Výborně! Dám vám vědět, až s nimi budu hotov.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.right,
        text: {
          cz: '|Nicméně, proč jsem s vámi chtěl mluvit. Prý jste zkoumali ty podivné tenepy, které se tu nedávno objevily a pak se zase ztratily.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: '|Ano. Zkoušeli jsme na ně všechno, ale nereagovaly na žádné z našich pokusů o prozkoumání jejich vnitřní struktury.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: '|Vy jste na něco přišli?',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.right,
        text: {
          cz: '|To ještě nevím. Našel jsem v archívu popis zařízení, které vykazovalo velmi podobné znaky. Popravdě, je to zatím jen velmi slabá teorie, na které bych s vámi rád dále pracoval.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: '|Říkáte, že podobná technologie se už někdy objevila?',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.right,
        text: {
          cz: '|Ne přímo v této formě, ale našel jsem záznamy z archeologického průzkumu, který narazil na předchůdcovskou civilizaci, která sestrojila zařízení s nejasným účelem, které sestávalo z devíti objektů, které byly chráněny parciálním horizontem, který je dělá viditelnými, ale ne prosvítitelnými.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.right,
        text: {
          cz: '|Pokud vím, našli jsme těch kufříku sedm, ne devět, takže je možné, že jde o nějaké jiné artefakty, ale vzhledem k jejich povaze a nejasné funkci, nechci nechat nic náhodě.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: '|Hmm. To je velmi zajímavé. Můžeme se podívat na ty zdroje ze kterých jste čerpali?',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.right,
        text: {
          cz: '|Samozřejmě. Mám je v laboratoři, takže pokud máte čas--',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: '|Ano, samozřejmě. Pojďme.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.right,
        text: {
          cz: '|Dobrá, tedy. Tudy prosím.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.right,
        text: {
          cz: '',
          en: '',
        },
        actions: [ { do: ActionId.hide} ],
      },
      { actorId: ActorId.left,
        text: {
          cz: '',
          en: '',
        },
        actions: [ { do: ActionId.hide} ],
      },
    ];
  }
}