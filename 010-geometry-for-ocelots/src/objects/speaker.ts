
export enum SpeakerPosition {
  leftBottom,
  rightBottom,
}

export enum SpeakerState {
  active,
  inactive,
  hidden,
}

export type SpeakerConfig = {
  scene: Phaser.Scene;
  initialState: SpeakerState,
  name?: string;
  textureName: string;
  position: SpeakerPosition;
  activeScale?: number;
  inactiveScale?: number;
  originOffset?: {x: number, y: number};
};

export class Speaker extends Phaser.GameObjects.Image {
  config: SpeakerConfig;
  activateTween: Phaser.Tweens.Tween;
  deactivateTween: Phaser.Tweens.Tween;
  showTween: Phaser.Tweens.Tween;
  hideTween: Phaser.Tweens.Tween;

  public speakerState: SpeakerState;

  constructor(config: SpeakerConfig) {
    const [x, y] = Speaker.getPosition(config.scene, config.position);
    super(config.scene, x, y, config.textureName);

    this.config = config;
    this.speakerState = this.config.initialState;

    this.setName(this.config.name ?? this.config.textureName);

    const isHidden = this.config.initialState === SpeakerState.hidden;
    this.setVisible(!isHidden).setAlpha(isHidden === true ? 0 : 1);
    this.setOrigin(...Speaker.getOrigin(this.config.position, this.config.originOffset));

    const [scaleX, scaleY] = Speaker.getScale(this.speakerState, this.config.activeScale ?? 1, this.config.inactiveScale ?? 1);
    this.setScale(scaleX, scaleY);

    this.activateTween = this.scene.tweens.add({
      targets: this,
      scaleX: { from: this.config.inactiveScale ?? 0.5, to: this.config.activeScale ?? 1 },
      scaleY: { from: this.config.inactiveScale ?? 0.5, to: this.config.activeScale ?? 1 },
      ease: Phaser.Math.Easing.Cubic.In,
      duration: 100,
      paused: true,
    });

    this.deactivateTween = this.scene.tweens.add({
      targets: this,
      scaleX: { from: this.config.activeScale ?? 1, to: this.config.inactiveScale ?? 0.5 },
      scaleY: { from: this.config.activeScale ?? 1, to: this.config.inactiveScale ?? 0.5 },
      ease: Phaser.Math.Easing.Cubic.Out,
      duration: 100,
      paused: true,
    });

    this.showTween = this.scene.tweens.add({
      targets: this,
      alpha: { from: 0, to: 1 },
      ease: Phaser.Math.Easing.Cubic.In,
      duration: 500,
      paused: true,
    });

    this.hideTween = this.scene.tweens.add({
      targets: this,
      alpha: { from: 1, to: 0 },
      ease: Phaser.Math.Easing.Cubic.Out,
      duration: 500,
      paused: true,
    });

    this.scene.add.existing(this);
  }

  activate() {
    if (this.speakerState === SpeakerState.active) {
      return;
    }

    this.speakerState = SpeakerState.active;
    this.playTween(this.activateTween);
  }

  deactivate() {
    if (this.speakerState === SpeakerState.inactive) {
      return;
    }

    this.speakerState = SpeakerState.inactive;
    this.playTween(this.deactivateTween);
  }

  show(state: SpeakerState) {
    if (this.speakerState !== SpeakerState.hidden) {
      return;
    }

    this.setVisible(true);
    this.playTween(this.showTween);
    switch (state) {
      case SpeakerState.active:
        this.activate();
        break;
      default:
        this.deactivate();
        break;
      }
  }

  hide() {
    if (this.speakerState === SpeakerState.hidden) {
      return;
    }

    this.speakerState = SpeakerState.hidden;
    this.playTween(this.hideTween);
  }


  private static getScale(state: SpeakerState, activeScale: number, inactiveScale: number) {
    switch (state) {
      case SpeakerState.active:
        return [activeScale, activeScale];
      default:
        return [inactiveScale, inactiveScale];
    }
  }

  private static getOrigin(position: SpeakerPosition, offset?: {x: number, y: number}) {
    switch (position) {
      case SpeakerPosition.leftBottom:
        return [0 + (offset?.x ?? 0), 1 + (offset?.y ?? 0)];
      case SpeakerPosition.rightBottom:
        return [1 + (offset?.x ?? 0), 1 + (offset?.y ?? 0)];
      default:
        return [0 + (offset?.x ?? 0), 0 + (offset?.y ?? 0)];
    }
  }

  private static getPosition(scene: Phaser.Scene, position: SpeakerPosition) {
    switch (position) {
      case SpeakerPosition.leftBottom:
        return [0, scene.game.scale.height];
      case SpeakerPosition.rightBottom:
        return [scene.game.scale.width, scene.game.scale.height];
      default:
        return [0, 0];
    }
  }

  private playTween(tween: Phaser.Tweens.Tween) {
    if (tween.paused) {
      tween.play();
    } else {
      tween.restart();
    }
  }
}