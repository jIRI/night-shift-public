import { AvailableSounds, SoundPlayerBuilder, Sounds } from "../builders/sound-player";
import { LocalizedText } from "../types/lang";

export enum ActorId {
  left,
  right,
  robot,
  drone,
  dummy,
}

export enum ActionId {
  playAnimation,
  continueAnimation,
  finishAnimation,
  show,
  hide,
  enableAnimationSync,
  disableAnimationSync,
  playSound,
}

export type ActionConfig = {
  do: ActionId.playAnimation
  | ActionId.continueAnimation
  | ActionId.finishAnimation
  | ActionId.show
  | ActionId.hide
  | ActionId.enableAnimationSync
  | ActionId.disableAnimationSync,
} | {
  do: ActionId.playSound,
  sound: AvailableSounds
};

export type ActorConfig = {
  id: ActorId;
  name: string;
  nameEncoder?: (text:LocalizedText) => LocalizedText;
  textEncoder?: (text:LocalizedText) => LocalizedText;
};

export type SelectItem = {
  text: string;
  gotoLabel: string;
};

export type Line = {
  actorId: ActorId;
  text: LocalizedText;
  visited?: boolean;
  actions?: Array<ActionConfig>;
  label?: string;
  select?: Array<SelectItem>;
}

export function getLocalizedText(localizedText: LocalizedText, lang: keyof LocalizedText): string {
  return localizedText[lang];
}

const telepathicEncoderReplacers: Array<(s: string) => string> = [
  s => s.replace(/[AÁ]/g, 'GR'),
  s => s.replace(/[EÉĚ]/g, 'RR'),
  s => s.replace(/[IÍ]/g, 'QR'),
  s => s.replace(/[OÓ]/g, 'RX'),
  s => s.replace(/[UÚŮ]/g, 'WR'),
  s => s.replace(/[YÝ]/g, 'TQ'),
  s => s.replace(/[ŠČŘŽĎŇ]/g, 'WT'),
];

export function applyReplacers(text: string, replacers: Array<(s: string) => string>): string {
  let result = text.toUpperCase();
  for(const replacer of replacers) {
    result = replacer(result);
  }
  return result;
}

export function applyReplacersToParts(text: string, replacers: Array<(s: string) => string>): string {
  let result = '';
  let parts = text.split('|');
  while(parts.length > 0) {
    result = result
      .concat(applyReplacers(parts.shift() ?? '', replacers))
      .concat(parts.shift() ?? '');
  }
  return result;
}

export class Script {
  currentLineIndex = 0;
  lines: Array<Line>;
  actorMap = new Map<ActorId, ActorConfig>();

  constructor() {
    this.lines = Script.lines();
    Script.actors().forEach(actor => this.actorMap.set(actor.id, actor));
  }

  actor(id: ActorId): ActorConfig {
    return this.actorMap.get(id) ?? Script.actors()[0];
  }

  reset() {
    this.currentLineIndex = -1;
    this.lines.forEach(line => line.visited = false);
  }

  nextLine(input?: string): Line | null {
    if (this.currentLineIndex < 0) {
      // first invocation, just return first line
      this.currentLineIndex = 0;
    } else {
      if (input != null ) {
        // we got input, try find index of the label
        const nextLineIndex = this.lines.findIndex(line => line.label === input);
        if (nextLineIndex >= 0) {
          // label found, jump to label
          this.currentLineIndex = nextLineIndex;
        } else {
          // label not found, well, continue on next line...
          this.currentLineIndex++;
        }
      } else {
        // no input, just continue on next line
        this.currentLineIndex++;
      }
    }

    if (this.currentLineIndex >= this.lines.length) {
      return null;
    }

    this.lines[this.currentLineIndex].visited = true;
    const line = Object.assign({}, this.lines[this.currentLineIndex]);
    const textEncoder = this.actor(line.actorId).textEncoder;
    if( textEncoder != null) {
      line.text = textEncoder(line.text);
    }
    return line;
  }

  //
  // Actors and script definition follows
  //

  static actors(): Array<ActorConfig>  {
    return [
      { id: ActorId.left, name: 'Geburatrolix' },
      { id: ActorId.right, name: 'Qluterutrinog' },
    ];
  }

  static telepathicEncoder(text: LocalizedText): LocalizedText {
    if(text.cz.startsWith('[') || text.en.startsWith('[')) {
      return text;
    }

    let cz = text.cz;
    let en = text.en;

    if(cz.startsWith('||') || en.startsWith('||')) {
      cz = cz.substr(2);
      en = en.substr(2);
    } else {
      cz = `<${applyReplacersToParts(text.cz, telepathicEncoderReplacers)}>`;
      en = `<${applyReplacersToParts(text.en, telepathicEncoderReplacers)}>`;
    }

    return {
      cz,
      en,
    };
  }

  static droidEncoder(text: LocalizedText): LocalizedText {
    if(text.cz.startsWith('') || text.en.startsWith('')) {
      return text;
    }

    let cz = `{ ${text.cz} }`;
    let en = `{ ${text.en} }`;

    return {
      cz,
      en,
    };
  }

  static lines(): Array<Line> {
    return [
      { actorId: ActorId.left,
        text: {
          cz: 'Hmmm. Zvláštní.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Co? Že se ty kufříky objevily zrovna na energopumpách a je jich devět, jak varoval Umakvacelok?',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Hmm?',
          en: '',
        },
        actions: [  ],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Jo to taky. Ale četl jsem teď knihu a jsem z toho vážně zmatený.',
          en: '',
        },
        actions: [  ],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Z knihy? Jak to?',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'No, Zarxerogarus mi doporučil knihu, že prý je to zajímavé čtení. Tak jsem si ji od něj půjčil. A v podstatě ano, je to zajímavá kniha.',
          en: '',
        },
        actions: [  ],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Ehe?',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Je to trochu taková magickovědecká fantastika.',
          en: '',
        },
        actions: [ { do: ActionId.playAnimation } ],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Jsou tam jednak běžní Pozemšťané, kteří posluhují vyšším bytostem z pozemšťanů vyvinutých, které konzumují nějakou v podstatě kouzelnou substanci, která jim umožňuje prostupovat čtyřrozměrným prostorem a měnit svou vnější formu dle libosti a tak dále.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Z toho vznikají různé třenice jak mezi nadlidmi a lidmi, tak i mezi nadlidmi samotnými, podle toho, jaké kdo vyznává morální zásady. A celé to skončí dost tragickou válkou mezi těmi dvěma skupinami.',
          en: '',
        },
        actions: [  ],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Takže taková ta klasická pozemská situace.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Ano, nepochybně je to celé metafora na sociální a jiné rozdíly v pozemské civilizaci, plus nějaké více či méně originální zápletky a tak dále.',
          en: '',
        },
        actions: [  ],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Není to úplně špatné, není to úplně zázračné, ve výsledku jsem byl rád, že jsem si to přečetl.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Byl?',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Ano, byl.',
          en: '',
        },
        actions: [  ],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Protože mě to zaujalo, zkoušel jsem hledat na pozemském Internetu, jestli ten autor nenapsal něco dalšího, co by třeba bylo ještě lepší.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Internetu?',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'No, Yoraggoraxuros mi dal přístup ke kopii pozemské globální informační sítě, kterou teď nedávno pořídili u nich v archívu.',
          en: '',
        },
        actions: [  ],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Aha, a jak ten Internet vypadá?',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Řekněme, že momentálně je ve fázi, kdy je lepší předstírat, že neexistuje.',
          en: '',
        },
        actions: [  ],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Haha, takže obvyklá situace.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Ano. Nicméně na tom Internetu jsem se po chvíli hledání dočetl, že je docela dobře možné, že autor té knihy je dost divná osoba, a že možná způsobil nějaké netriviální trauma jiné osobě, a že pokud jsou tvrzení té postižené strany pravdivá, je ten autor dost nechutná osoba.',
          en: '',
        },
        actions: [  ],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Rozumím.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'No a můj problém teď je, že nevím co s tím.',
          en: '',
        },
        actions: [  ],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Jak to myslíš? Země zatím nebyla kontaktována, takže zasahovat do jejich záležitostí nemůžeme.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Myslím z hlediska mého názoru na tu knihu.',
          en: '',
        },
        actions: [  ],
      },
      { actorId: ActorId.right,
        text: {
          cz: '[...]',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Ah, ovšem.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Přesně. Můžu ostatním doporučovat dílo, jehož autor je potencionálně bytost pochybných kvalit?',
          en: '',
        },
        actions: [  ],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Eh, ale nemůžou i hrozní lidé tvořit skvělá díla a skvělí lidé tvořit bezcenný brak?',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'To samozřejmě můžou, v tom problém není. Otázka je, co s tím mám dělat já, když se mě někdo zeptá, jestli si má tu knihu přečíst?',
          en: '',
        },
        actions: [  ],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Hmm. V podstatě by dílo mělo stát samostatně. Předpokládám, že historicky se spousta lidí, kteří napsali něco, co se nám líbí, chovala... nepřístojně.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Souhlasím, ale pokud ti tu knihu doporučím jako stojící za přečtení, nebudu se podílet na normalizaci chování autora?',
          en: '',
        },
        actions: [  ],
      },
      { actorId: ActorId.right,
        text: {
          cz: '[...]',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Naše analytická morální logika říká, že ne. Lidé jsou odpovědní za své chování, a dobrý skutek nepřestane být dobrým skutkem proto, že ho provedla zlá osoba.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'To je ovšem náš pohled, a podle mě, toho víme o lidech pořád příliš málo na to, abychom na ně aplikovali naše normy.',
          en: '',
        },
        actions: [  ],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Eh, ano, chápu to dilema.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'A nemyslím, že bych se měl zbavit odpovědnosti tím, že ti řeknu, aby sis tu knihu přečetla, pak si načetla v čem spočívá to údajné problematické chování autora a rozhodla se sama. To mi přijde jako trochu alibistické řešení.',
          en: '',
        },
        actions: [  ],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Dobrá, co když ti řeknu, že jsem si toho všeho vědoma, a že přesto si chci udělat názor sama?',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Hmm. To zní jako nejméně problematické řešení problému.',
          en: '',
        },
        actions: [  ],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Takže, nech mě o tom chvilku přemýšlet...',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.right,
        text: {
          cz: '[...]',
          en: '',
        },
        actions: [  ],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Ach, co to tam ten Trumorediton pořád dělá? Říkala jsem mu jasně, aby na to nesahal!',
          en: '',
        },
        actions: [  { do: ActionId.continueAnimation } ],
      },
      { actorId: ActorId.right,
        text: {
          cz: '[...]',
          en: '',
        },
        actions: [  ],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Aaaah.',
          en: '',
        },
        actions: [  { do: ActionId.continueAnimation } ],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Promiň, musím běžet.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.right,
        text: {
          cz: '',
          en: '',
        },
        actions: [ { do: ActionId.hide } ],
      },
      { actorId: ActorId.left,
        text: {
          cz: '[...]',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Ech.',
          en: '',
        },
        actions: [  ],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Co já teď s tím?',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: '[...]',
          en: '',
        },
        actions: [  ],
      },
      { actorId: ActorId.left,
        text: {
          cz: '[???]',
          en: '',
        },
        actions: [  ],
      },
      { actorId: ActorId.left,
        text: {
          cz: '[!!!]',
          en: '',
        },
        actions: [  ],
      },
   ];
  }
}