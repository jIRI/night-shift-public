import { SoundPlayerBuilder } from "./sound-player";

export class BackgroundAnimationBuilder {
  soundPlayer: SoundPlayerBuilder | null = null;

  currentTweenIndex = 0;
  animationTweens = new Array<Phaser.Tweens.Tween>();
  activeTween: Phaser.Tweens.Tween | null = null;
  stopped = false;

  technician: Phaser.GameObjects.Image | null = null;
  suitcase: Phaser.GameObjects.Image | null = null;

  preloadAssets(scene: Phaser.Scene) {
    scene.load.image('technician', 'images/technician.png');
    scene.load.image('suitcase', 'images/suitcase.png');
  }

  createObjects(scene: Phaser.Scene, soundPlayer: SoundPlayerBuilder) {
    this.soundPlayer = soundPlayer;

    const speedModifier = 1;
    const suitcaseX = scene.scale.width / 2;
    const suitcaseY = scene.scale.height / 5;
    const technicianX = 0.35 * scene.scale.width;
    const technicianY = suitcaseY - 40;
    const suitcaseTint = 0x707070;

    const technicianTouchX = technicianX + 50;

    this.technician = scene.add.image(technicianX, technicianY, 'technician').setOrigin(0.5, 0.5).setScale(0.6).setDepth(-2).setVisible(true);

    this.suitcase = scene.add.image(suitcaseX, suitcaseY, 'suitcase').setOrigin(0.5, 0.5).setScale(0.4, 0.4).setTint(suitcaseTint).setDepth(-5).setVisible(true);

    // ongoing anims
    scene.add.tween({
      targets: this.suitcase,
      y: { from: suitcaseY - 10, to: suitcaseY + 10 },
      yoyo: true,
      repeat: -1,
      duration: speedModifier * 1500,
    });
    scene.add.tween({
      targets: this.technician,
      y: { from: technicianY - 10, to: technicianY + 10 },
      yoyo: true,
      repeat: -1,
      duration: speedModifier * 750,
    });


    //on demand anims
    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.technician,
      x: { from: technicianX, to: technicianTouchX },
      duration: speedModifier * 300,
      onComplete: _ => {
        this.soundPlayer?.play("touch");
        this.playNext();
      },
    }));
    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.technician,
      x: { from: technicianTouchX, to: technicianX },
      duration: speedModifier * 100,
    }));
    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.technician,
      x: { from: technicianX, to: technicianTouchX },
      duration: speedModifier * 300,
      onComplete: _ => {
        this.soundPlayer?.play("touch");
        this.playNext();
      },
    }));
    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.technician,
      x: { from: technicianTouchX, to: technicianX },
      duration: speedModifier * 100,
    }));

    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.technician,
      x: { from: technicianX, to: technicianTouchX },
      duration: speedModifier * 300,
      onComplete: _ => {
        this.soundPlayer?.play("suitcase");
        this.playNext();
      }
    }));
    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.suitcase,
      x: { from: suitcaseX - 7, to: suitcaseX + 7 },
      yoyo: true,
      repeat: 7,
      duration: speedModifier * 20,
      onStart: _ => {
        this.suitcase?.setTint(0xffffff);
        this.playNext();
      },
      onComplete: _ => {
        this.suitcase?.setTint(suitcaseTint);
      }
    }));
    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.technician,
      angle: { from: 0, to: 360 },
      duration: speedModifier * 750,
      onStart: _ => {
        this.suitcase?.setTint(0xffffff);
        this.playNext()
      },
      onComplete: _ => {
        this.suitcase?.setTint(suitcaseTint);
        this.suitcase?.setX(suitcaseX);
      }
    }));
    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.technician,
      x: { from: technicianTouchX, to: -this.technician.width },
      duration: speedModifier * 1000,
    }));
  }

  startAnimation(scene: Phaser.Scene) {
    this.stopped = false;
    this.currentTweenIndex = 0;
    this.animationTweens.forEach(t => t.setTimeScale(1));

    this.playNext();
  }

  stopAnimation() {
    this.stopped = true;
    this.animationTweens.forEach(tween => {
      tween.stop();
      tween.setTimeScale(1);
    });
  }

  playNext() {
    if (this.stopped) {
      return;
    }

    if (this.currentTweenIndex < this.animationTweens.length) {
      this.activeTween = this.animationTweens[this.currentTweenIndex++];
      this.activeTween?.play();
    }
  }

  isAnimationPlaying() {
    return this.activeTween?.isPlaying() ?? false;
  }

  skip() {
    if (this.stopped) {
      return;
    }

    this.activeTween?.setTimeScale(100);
  }

}