export class Sounds {
  suitcase: Phaser.Sound.BaseSound | null = null;
  touch: Phaser.Sound.BaseSound | null = null;
}

export type AvailableSounds = keyof Sounds;

export class SoundPlayerBuilder extends Sounds {
  preloadAssets(scene: Phaser.Scene) {
    scene.load.audio('suitcase-sound', 'audio/suitcase.ogg');
    scene.load.audio('touch-sound', 'audio/touch.ogg');
  }

  createObjects(scene: Phaser.Scene) {
    this.suitcase = scene.sound.add('suitcase-sound', {
      volume: 0.1,
      rate: 1,
    });
    this.touch = scene.sound.add('touch-sound', {
      volume: 0.06,
      rate: 2,
    });
  }

  play(key: AvailableSounds) {
    switch(key) {
      case 'suitcase':
        this.suitcase?.play();
        return;
      case 'touch':
        this.touch?.play();
        return;
      default:
        return;
    }
  }
}