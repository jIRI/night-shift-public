import { GameObjects } from "phaser";
import { SoundPlayerBuilder } from "./sound-player";

export class MainAnimationBuilder {
  soundPlayer: SoundPlayerBuilder | null = null;

  currentTweenIndex = 0;
  animationTweens = new Array<Phaser.Tweens.Tween>();
  activeTween: Phaser.Tweens.Tween | null = null;
  stopped = false;

  background1: Phaser.GameObjects.Image | null = null;
  background2: Phaser.GameObjects.Image | null = null;

  robotSprite: Phaser.GameObjects.Sprite | null = null;
  droneSprite: Phaser.GameObjects.Sprite | null = null;

  robotBeepSound: Phaser.Sound.BaseSound | null = null;
  explosionSound: Phaser.Sound.BaseSound | null = null;

  preloadAssets(scene: Phaser.Scene) {
    scene.load.image('background-01', 'images/background-01.png');
    scene.load.image('background-02', 'images/background-02.png');

    scene.load.spritesheet('robot-idle', 'images/robot-idle.png', { frameWidth: 192, frameHeight: 180 });
    scene.load.spritesheet('robot-walk', 'images/robot-walk.png', { frameWidth: 220, frameHeight: 193 });

    scene.load.spritesheet('drone-idle', 'images/drone-idle.png', { frameWidth: 203, frameHeight: 126 });
    scene.load.spritesheet('drone-fly', 'images/drone-fly.png', { frameWidth: 200, frameHeight: 164 });
    scene.load.spritesheet('drone-turn', 'images/drone-turn.png', { frameWidth: 220, frameHeight: 204 });

    scene.load.audio('robot-beep-sound', 'audio/robot-beep.ogg');
    scene.load.audio('explosion-sound', 'audio/explosion.mp3');
  }

  createObjects(scene: Phaser.Scene, soundPlayer: SoundPlayerBuilder) {
    this.soundPlayer = soundPlayer;

    const speedModifier = 1;

    const actorsY = 800;
    const droneOffsetY = 230;

    this.background1 = scene.add.image(0, 0, 'background-01').setOrigin(0, 0).setDepth(-1).setVisible(false);
    this.background2 = scene.add.image(0, 0, 'background-02').setOrigin(0, 0).setDepth(-2).setVisible(false);

    this.robotSprite = scene.add.sprite(200, actorsY, 'robot').setScale(1, 1).setOrigin(0.5, 1).setDepth(10).setVisible(false);
    scene.anims.create({
      key: 'robot-idle',
      frames: scene.anims.generateFrameNames('robot-idle', { start: 0, end: 11 }),
      frameRate: 15,
      repeat: -1,
    });
    scene.anims.create({
      key: 'robot-walk',
      frames: scene.anims.generateFrameNames('robot-walk', { start: 0, end: 11 }),
      frameRate: 15,
      repeat: -1,
    });

    this.droneSprite = scene.add.sprite(1200, actorsY - droneOffsetY, 'drone').setScale(1, 1).setOrigin(0.5, 1).setScale(0.7).setFlipX(true).setVisible(false);
    scene.anims.create({
      key: 'drone-idle',
      frames: scene.anims.generateFrameNames('drone-idle', { start: 0, end: 11 }),
      frameRate: 12,
      repeat: -1,
    });
    scene.anims.create({
      key: 'drone-fly',
      frames: scene.anims.generateFrameNames('drone-fly', { start: 0, end: 11 }),
      frameRate: 15,
      repeat: -1,
    });
    scene.anims.create({
      key: 'drone-turn',
      frames: scene.anims.generateFrameNames('drone-turn', { start: 0, end: 15 }),
      frameRate: 15,
    });

    this.robotBeepSound = scene.sound.add('robot-beep-sound', {
      volume: 0.08,
      rate: 1,
    });

    this.explosionSound = scene.sound.add('explosion-sound', {
      volume: 0.2,
      rate: 0.9,
    });


    // start, robot supercomputes
    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.robotSprite,
      scale: { from: 0.95, to: 1 },
      repeat: 12,
      ease: Phaser.Math.Easing.Sine.InOut,
      duration: speedModifier * 50,
      yoyo: true,
      onStart: () => {
        this.robotSprite?.setVisible(true);
        this.robotSprite?.play('robot-idle');
        this.droneSprite?.setVisible(true);
        this.droneSprite?.play('drone-idle');
        this.robotBeepSound?.play();
        this.robotSprite?.setTint(0xff0000);
        this.robotSprite?.stop();
      },
      onComplete: () => {
        this.robotSprite?.setTint(0xffffff);
        this.robotSprite?.play('robot-idle');
      },
    }));

    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.background1,
      alpha: { from: 1, to: 0},
      duration: speedModifier * 1200,
      onStart: () => {
        scene.cameras.main.shake(1200, 0.005);
        this.explosionSound?.play();
        this.background1?.setTint(0xff0000);
        this.playNext();
      },
      onComplete: () => {
      },
    }));

    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.background2,
      alpha: { from: 0, to: 1},
      duration: speedModifier * 1200,
      onStart: () => {
        this.background2?.setVisible(true);
      },
      onComplete: () => {
        this.background1?.setVisible(false);
      },
    }));
  }

  startAnimation(scene: Phaser.Scene) {
    this.stopped = false;
    this.currentTweenIndex = 0;
    this.animationTweens.forEach(t => t.setTimeScale(1));

    this.background1?.setVisible(true);

    this.playNext();
  }


  stopAnimation() {
    this.stopped = true;
    this.animationTweens.forEach(tween => {
      tween.stop();
      tween.setTimeScale(1);
    });
  }

  playNext() {
    if (this.stopped) {
      return;
    }

    if (this.currentTweenIndex < this.animationTweens.length) {
      this.activeTween = this.animationTweens[this.currentTweenIndex++];
      this.activeTween?.play();
    }
  }

  isAnimationPlaying() {
    return this.activeTween?.isPlaying() ?? false;
  }

  skip() {
    if (this.stopped) {
      return;
    }

    this.activeTween?.setTimeScale(100);
  }

}