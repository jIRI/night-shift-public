import { GameObjects } from "phaser";

export class Sounds {
}

export type AvailableSounds = keyof Sounds;

export class SoundPlayerBuilder extends Sounds{
  preloadAssets(scene: Phaser.Scene) {
  }

  createObjects(scene: Phaser.Scene) {
  }

  play(key: AvailableSounds) {
    switch(key) {
      case "":
        return;
      default:
        return;
    }
   }
}