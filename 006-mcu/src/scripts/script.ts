import { AvailableSounds } from "../builders/sound-player";
import { LocalizedText } from "../types/lang";

export enum ActorId {
  robot,
  drone,
  dummy,
}

export enum ActionId {
  playAnimation,
  continueAnimation,
  finishAnimation,
  show,
  hide,
  enableAnimationSync,
  disableAnimationSync,
  playSound,
}

export type ActionConfig = {
  do: ActionId.playAnimation
  | ActionId.continueAnimation
  | ActionId.finishAnimation
  | ActionId.show
  | ActionId.hide
  | ActionId.enableAnimationSync
  | ActionId.disableAnimationSync,
} | {
  do: ActionId.playSound,
  sound: AvailableSounds
};

export type ActorConfig = {
  id: ActorId;
  name: string;
  textEncoder?: (text:LocalizedText) => LocalizedText;
};

export type SelectItem = {
  text: string;
  gotoLabel: string;
};

export type Line = {
  actorId: ActorId;
  text: LocalizedText;
  visited?: boolean;
  actions?: Array<ActionConfig>;
  label?: string;
  select?: Array<SelectItem>;
}

export function getLocalizedText(localizedText: LocalizedText, lang: keyof LocalizedText): string {
  return localizedText[lang];
}

const telepathicEncoderReplacers: Array<(s: string) => string> = [
  s => s.replace(/[AÁ]/g, 'GR'),
  s => s.replace(/[EÉĚ]/g, 'RR'),
  s => s.replace(/[IÍ]/g, 'QR'),
  s => s.replace(/[OÓ]/g, 'RX'),
  s => s.replace(/[UÚŮ]/g, 'WR'),
  s => s.replace(/[YÝ]/g, 'TQ'),
  s => s.replace(/[ŠČŘŽĎŇ]/g, 'WT'),
];

export function applyReplacers(text: string, replacers: Array<(s: string) => string>): string {
  let result = text.toUpperCase();
  for(const replacer of replacers) {
    result = replacer(result);
  }
  return result;
}

export class Script {
  currentLineIndex = 0;
  lines: Array<Line>;
  actorMap = new Map<ActorId, ActorConfig>();

  constructor() {
    this.lines = Script.lines();
    Script.actors().forEach(actor => this.actorMap.set(actor.id, actor));
  }

  actor(id: ActorId): ActorConfig {
    return this.actorMap.get(id) ?? Script.actors()[0];
  }

  reset() {
    this.currentLineIndex = -1;
    this.lines.forEach(line => line.visited = false);
  }

  nextLine(input?: string): Line | null {
    if (this.currentLineIndex < 0) {
      // first invocation, just return first line
      this.currentLineIndex = 0;
    } else {
      if (input != null ) {
        // we got input, try find index of the label
        const nextLineIndex = this.lines.findIndex(line => line.label === input);
        if (nextLineIndex >= 0) {
          // label found, jump to label
          this.currentLineIndex = nextLineIndex;
        } else {
          // label not found, well, continue on next line...
          this.currentLineIndex++;
        }
      } else {
        // no input, just continue on next line
        this.currentLineIndex++;
      }
    }

    if (this.currentLineIndex >= this.lines.length) {
      return null;
    }

    this.lines[this.currentLineIndex].visited = true;
    const line = Object.assign({}, this.lines[this.currentLineIndex]);
    const textEncoder = this.actor(line.actorId).textEncoder;
    if( textEncoder != null) {
      line.text = textEncoder(line.text);
    }
    return line;
  }

  //
  // Actors and script definition follows
  //

  static actors(): Array<ActorConfig>  {
    return [
      { id: ActorId.robot, name: 'X4R14N', textEncoder: Script.droidEncoder },
      { id: ActorId.drone, name: 'V0T4R3X', textEncoder: Script.droidEncoder },
    ];
  }

  static telepathicEncoder(text: LocalizedText): LocalizedText {
    if(text.cz.startsWith('[') || text.en.startsWith('[')) {
      return text;
    }

    let cz = applyReplacers(`<${text.cz}>`, telepathicEncoderReplacers);
    let en = applyReplacers(`<${text.en}>`, telepathicEncoderReplacers);

    return {
      cz,
      en,
    };
  }

  static droidEncoder(text: LocalizedText): LocalizedText {
    if(text.cz.startsWith('[') || text.en.startsWith('[')) {
      return text;
    }

    let cz = `{ ${text.cz} }`;
    let en = `{ ${text.en} }`;

    return {
      cz,
      en,
    };
  }


  static lines(): Array<Line> {
    return [
      { actorId: ActorId.robot,
        text: {
          cz: '[...]',
          en: '',
        },
        actions: [ { do: ActionId.playAnimation } ],
      },
      { actorId: ActorId.robot,
        text: {
          cz: 'Aaaah.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.drone,
        text: {
          cz: 'Copak?',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.robot,
        text: {
          cz: 'Ále, jsem zpruzenej.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.drone,
        text: {
          cz: '[???]',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.drone,
        text: {
          cz: 'Že nás tu zavřeli?',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.robot,
        text: {
          cz: 'Eh?',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.robot,
        text: {
          cz: 'Jo, to taky. Ale hlavně: z těch mediálních souborů, co jsi mi nasdílel, jsem se podíval na nějaké věci ze složky Marvel Cinematic Universe, a cítím se... Hmm. Rozpolcený?',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.drone,
        text: {
          cz: 'Ale? Mně to bavilo. Co je za problém?',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.robot,
        text: {
          cz: 'Zaprvé: pochopil jsem to správně, že tahle Země produkuje fikci jak na běžícím páse?',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.drone,
        text: {
          cz: 'Jojo. Zjevně to u nich nakonec není jak u nás, kde to stojí spoustu práce, vyprodukovat nějaký fiktivní příběh.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.drone,
        text: {
          cz: 'Asi nějaká druhová deviace, nebo co.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.robot,
        text: {
          cz: 'Hmm. Zajímavé.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.robot,
        text: {
          cz: 'Nicméně. Zpracoval jsem těch 24 filmů co jsi tam měl a říkal jsem si -- dobrá, vidím, co se děje, to mi stačí.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.robot,
        text: {
          cz: 'Pak jsem si všiml, že u dvou seriálů máš v metadatech příznak doporučení, a tak jsem si dal ještě WandaVision a Loki.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.robot,
        text: {
          cz: 'Uhm. Chybami se jeden učí.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.drone,
        text: {
          cz: 'Chápu to správně, že se ti to nelíbilo?',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.robot,
        text: {
          cz: 'Mno, jak to říct? Z těch čtyřiadvaceti filmů mě nějak zásadně oslovily čtyři, možná pět: Iron Man, protože to byl první film v celé té epopeji a byl čerstvý a vtipný a tenhle Robert Downey Jr. tam má charisma síly středogalaktické černé díry.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.drone,
        text: {
          cz: 'Jasně. Dobrá volba.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.robot,
        text: {
          cz: 'Dále Guardians of the Galaxy, protože je to takový ztřeštěný cirkus se srdcem ze zlata, který se nebere nijak příšerně vážně.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.robot,
        text: {
          cz: 'K tomu bych pravděpodobně přidal Guardians of the Galaxy Vol. 2, jelikož to je jeden z mála druhých dílů čehokoliv, který není výrazně horší než první díl a je zábavný a dojemný a má tu scénu z vězení, kde vyvraždění několika desítek lidí působí vtipně.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.drone,
        text: {
          cz: 'Ano, souhlas.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.robot,
        text: {
          cz: 'Další je Ant-Man, kde se pro jednou nemusí zachraňovat celá planeta před nějakým šmejdem z vesmíru, přitom je to stále zábavný a místy malinko dojemný film.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.drone,
        text: {
          cz: 'Znova souhlasím.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.robot,
        text: {
          cz: 'No a konečně Thor: Ragnarok, což je skoro nejupřímnější akční komedie v celé sérii, s báječnou chemií mezi hlavními postavami.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.robot,
        text: {
          cz: 'A to je asi tak všechno.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.robot,
        text: {
          cz: 'Chm.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.drone,
        text: {
          cz: 'Ano? Všiml sis na svém seznamu něčeho?',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.robot,
        text: {
          cz: 'No, ano. Jsou to všechno komedie.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.drone,
        text: {
          cz: 'Taky to tak vidím.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.drone,
        text: {
          cz: 'Takže takový Captain America: The Winter Soldier se ti nelíbil? Nebo cokoliv s Avengers v názvu?',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.robot,
        text: {
          cz: 'Jako nerozuměj mi špatně -- nejde o to, že by kterýkoliv z těch filmů byl špatný. Všechny jsou asi... v pořádku.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.robot,
        text: {
          cz: 'Ale nedokážu brát vážně pokus o špionážní drama, ve kterém si chlapík se štítem popírajícím fyzikální zákony dává do huby s chlápkem s ocelovou rukou.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.robot,
        text: {
          cz: 'Nebo dramatu o názorovém rozkolu v rodině -- což nejspíš má být téma Captain America: Civil War -- asi na vážnosti nepřidá, když si tam chlapík se štítem popírajícím fyzikální zákony a chlápek s ocelovou rukou dávají do huby s chlápkem v ocelovém obleku.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.robot,
        text: {
          cz: 'Tenhle formát superhrdinských filmů, s hlavními postavami ve směšných kostýmech, funguje pro mě nejlíp, když se nebere vážně, uvědomuje si co je zač a nehraje si na vážná témata, společenské komentáře a tak podobně.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.drone,
        text: {
          cz: 'Ah, rozumím. Tak docela nesouhlasím, ale rozumím.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.robot,
        text: {
          cz: 'Hele, upřímně -- studio, které se chlubí tím, že testování na publiku a přetáčky jsou součástí jejich kreativního procesu -- podle toho, co bylo v doplňkových souborech, asi hledí míň na uměleckou vizi, než na tržby a snahu všechny potěšit a nikoho neurazit.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.robot,
        text: {
          cz: 'Takže já jako chápu, proč jsou věci tak jak jsou, jen si myslím, že jsou žánry, kterým podobný iterativní proces svědčí, a žánry, kde je spíš na škodu.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.drone,
        text: {
          cz: 'No dobrá, dobrá.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.drone,
        text: {
          cz: 'A co ty seriály? Ty tě taky nezaujaly?',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.robot,
        text: {
          cz: 'Uh, to je malinko komplikovanější, než že by se mi nelíbily úplně.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.robot,
        text: {
          cz: 'U WandaVision mě chytil ten experimentální začátek, který sice v některých ohledech asi trval déle než bylo nezbytně nutné, ale dobrá, tempo nebylo nějak úplně geologické.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.robot,
        text: {
          cz: 'Problém je v tom, že to někdy v poslední třetině najelo do stejných vyjetých kolejí -- neurazit, nepřekvapit, nepřemýšlet. Plus háček na další sérii.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.drone,
        text: {
          cz: 'Nooo, to je malinko zjednodušený pohled, ale dobrá. Co Loki?',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.robot,
        text: {
          cz: 'Ah, to bylo nejdřív jako láska na první pohled. Tom Hiddleston má takové charisma, že by mohl číst leták ze supermarketu a bylo by to záživnější než spousta filmů a seriálů co znám.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.drone,
        text: {
          cz: 'Ale?',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.robot,
        text: {
          cz: 'Ale. Někde uprostřed jsou tak dvě epizody, kde se mají prohlubovat vztahy a růst napětí a tak dále, ale jelikož se nic moc neděje, začne se to brzo trochu vléct.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.robot,
        text: {
          cz: 'Nic tragického, kdyby po tom nenásledoval naprosto uspěchaný závěr, kde se nic uspokojivě neuzavře, a opět se jen masivně hákuje na další sérii.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.drone,
        text: {
          cz: 'Tak ale není potřeba vždycky všechno vysvětlit a uzavřít, ne?',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.robot,
        text: {
          cz: 'Ne, to samozřejmě ne, ale centrální bod seriálu byl emoční růst Lokiho a záhada kolem toho, kdo řídí tu ujetou agentury, která mi nepříjemně připomíná tuhle zatracenou instituci.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.robot,
        text: {
          cz: 'A ano, Loki emočně vyrostl, ale místo abychom měli příležitost to pořádně strávit, máme v posledním díle dlouhatánské verbální expozice, nové postavy, zvraty a celý pytel opotřebovaných součástek navrch.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.robot,
        text: {
          cz: 'Za mě takhle nevypadá uspokojivý závěr série. Takhle vypadá produkt, který tě má přimět investovat čas do další série.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.robot,
        text: {
          cz: 'Srovnej to třeba se Stranger Things. Tam záhady taky zůstávají nedovysvětleny a tak dále.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.robot,
        text: {
          cz: 'Ale po každé sérii jsem měl pocit, že jsem viděl uzavřený příběh. Že podstatné věci byly ukázány nebo řečeny a pokud jsou někde mezery, není to tak vulgární lákadlo na dalších N dílů za dva roky.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.drone,
        text: {
          cz: 'Hmmm. Chtěl jsem s tebou kategoricky nesouhlasit, ale obávám se, že jsi mě trochu nalomil.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.robot,
        text: {
          cz: 'Hele, mým cílem ti to není znechutit nebo tak něco. Pokud se ti to líbilo, skvěle pro tebe.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.robot,
        text: {
          cz: 'Ale za sebe vidím v celém MCU poměrně vážné problémy především s předvídatelností, naprosto bizarní eskalací toho, co je v každém filmu v sázce, a zjevnou korporátní šablonovitostí.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.robot,
        text: {
          cz: 'Někdy mi to nevadí a chci si jen užít pár hodin bezmyšlenkovité zábavy mezi hlídkami, ale MCU pro mě v tomhle balancuje na hranici snesitelnosti.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.drone,
        text: {
          cz: 'To je fér.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.drone,
        text: {
          cz: 'No hele, věnuju tomu ještě jeden proces na pozadí, uvidíme, jestli najdu protiargumenty.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.drone,
        text: {
          cz: 'Nicméně, mám ještě pár kousků které bych ti rád nabídl ke zkouknutí, takže--',
          en: '',
        },
        actions: [ { do: ActionId.enableAnimationSync } ],
      },
      { actorId: ActorId.robot,
        text: {
          cz: 'Ááááh, co se to sakra zase děje?',
          en: '',
        },
        actions: [ { do: ActionId.continueAnimation } ],
      },
    ];
  }
}