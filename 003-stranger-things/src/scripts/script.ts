import { LocalizedText } from "../types/lang";

export enum ActorId {
  robot,
  drone,
  engineer,
}

export enum ActionId {
  playAnimation,
  continueAnimation,
  finishAnimation,
  show,
  hide,
  enableAnimationSync,
  disableAnimationSync,
}

export type ActorConfig = {
  id: ActorId;
  name: string;
  textEncoder?: (text:LocalizedText) => LocalizedText;
};

export type SelectItem = {
  text: string;
  gotoLabel: string;
};

export type Line = {
  actorId: ActorId;
  text: LocalizedText;
  visited?: boolean;
  actions?: Array<ActionId>;
  label?: string;
  select?: Array<SelectItem>;
}

export function getLocalizedText(localizedText: LocalizedText, lang: keyof LocalizedText): string {
  return localizedText[lang];
}

const telepathicEncoderReplacers: Array<(s: string) => string> = [
  s => s.replace(/[AÁ]/g, 'GR'),
  s => s.replace(/[EÉĚ]/g, 'RR'),
  s => s.replace(/[IÍ]/g, 'QR'),
  s => s.replace(/[OÓ]/g, 'RX'),
  s => s.replace(/[UÚŮ]/g, 'WR'),
  s => s.replace(/[YÝ]/g, 'TQ'),
  s => s.replace(/[ŠČŘŽĎŇ]/g, 'WT'),
];

export function applyReplacers(text: string, replacers: Array<(s: string) => string>): string {
  let result = text.toUpperCase();
  for(const replacer of replacers) {
    result = replacer(result);
  }
  return result;
}

export class Script {
  currentLineIndex = 0;
  lines: Array<Line>;
  actorMap = new Map<ActorId, ActorConfig>();

  constructor() {
    this.lines = Script.lines();
    Script.actors().forEach(actor => this.actorMap.set(actor.id, actor));
  }

  actor(id: ActorId): ActorConfig {
    return this.actorMap.get(id) ?? Script.actors()[0];
  }

  reset() {
    this.currentLineIndex = -1;
    this.lines.forEach(line => line.visited = false);
  }

  nextLine(input?: string): Line | null {
    if (this.currentLineIndex < 0) {
      // first invocation, just return first line
      this.currentLineIndex = 0;
    } else {
      if (input != null ) {
        // we got input, try find index of the label
        const nextLineIndex = this.lines.findIndex(line => line.label === input);
        if (nextLineIndex >= 0) {
          // label found, jump to label
          this.currentLineIndex = nextLineIndex;
        } else {
          // label not found, well, continue on next line...
          this.currentLineIndex++;
        }
      } else {
        // no input, just continue on next line
        this.currentLineIndex++;
      }
    }

    if (this.currentLineIndex >= this.lines.length) {
      return null;
    }

    this.lines[this.currentLineIndex].visited = true;
    const line = Object.assign({}, this.lines[this.currentLineIndex]);
    const textEncoder = this.actor(line.actorId).textEncoder;
    if( textEncoder != null) {
      line.text = textEncoder(line.text);
    }
    return line;
  }

  //
  // Actors and script definition follows
  //

  static actors(): Array<ActorConfig>  {
    return [
      { id: ActorId.robot, name: 'X4R14N', textEncoder: Script.droidEncoder },
      { id: ActorId.drone, name: 'V0T4R3X', textEncoder: Script.droidEncoder },
      { id: ActorId.engineer, name: 'Geburatrolix'},
    ];
  }

  static telepathicEncoder(text: LocalizedText): LocalizedText {
    if(text.cz.startsWith('[') || text.en.startsWith('[')) {
      return text;
    }

    let cz = applyReplacers(`<${text.cz}>`, telepathicEncoderReplacers);
    let en = applyReplacers(`<${text.en}>`, telepathicEncoderReplacers);

    return {
      cz,
      en,
    };
  }

  static droidEncoder(text: LocalizedText): LocalizedText {
    if(text.cz.startsWith('[') || text.en.startsWith('[')) {
      return text;
    }

    let cz = `{ ${text.cz} }`;
    let en = `{ ${text.en} }`;

    return {
      cz,
      en,
    };
  }


  static lines(): Array<Line> {
    return [
      // screen 1
      { actorId: ActorId.robot,
        text: {
          cz: '[...]',
          en: '',
        },
        actions: [ActionId.playAnimation],
      },
      { actorId: ActorId.robot,
        text: {
          cz: 'Chm.',
          en: '',
        },
        actions: [ActionId.playAnimation],
      },
      { actorId: ActorId.robot,
        text: {
          cz: '',
          en: '',
        },
        actions: [ActionId.continueAnimation, ActionId.hide],
      },
      { actorId: ActorId.robot,
        text: {
          cz: 'Další?',
          en: '',
        },
        actions: [ActionId.show],
      },
      { actorId: ActorId.robot,
        text: {
          cz: 'Uhm.',
          en: '',
        },
        actions: [ActionId.show],
      },
      { actorId: ActorId.robot,
        text: {
          cz: '',
          en: '',
        },
        actions: [ActionId.continueAnimation, ActionId.hide],
      },
      { actorId: ActorId.drone,
        text: {
          cz: 'Huh?',
          en: '',
        },
        actions: [ActionId.show],
      },
      { actorId: ActorId.drone,
        text: {
          cz: 'Co to... Ah, to jsi ty.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.robot,
        text: {
          cz: 'Hahá, rád tě vidím. Už jsem se bál, že jsem jediný, kdo jim utekl.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.drone,
        text: {
          cz: 'Co to bylo za ránu?',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.robot,
        text: {
          cz: 'To jsem byl já. To se mi tak najednou udělalo. V API řídícího modulu se mi objevily nějaké nové funkce a jedna dělá tohle.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.drone,
        text: {
          cz: 'No nekecej. A jak se to stalo?',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.robot,
        text: {
          cz: 'Já ti vlastně ani nevím. Byl jsem normálně na hlídce, a najednou jsem se ocitl někde úplně jinde.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.robot,
        text: {
          cz: 'Chvíli jsem se motal okolo, identifikoval narušitele, malinko ho pronásledoval, možná i trochu zaútočil. Pak světlo, tma, a najednou jsem tady v těch chodbách.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.robot,
        text: {
          cz: 'Je to celé nějaké rozmazané. Koukal jsem do logů, ale našel jsem jen nějaké divné výjimky.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.robot,
        text: {
          cz: '[...]',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.robot,
        text: {
          cz: 'Jo, taky se můžu přetaktovat. Nedá se to dělat moc často -- hrozně to žere a generuje tuny odpadního tepla. Ale může se hodit.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.robot,
        text: {
          cz: '[...]',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.robot,
        text: {
          cz: 'Podivná situace.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.robot,
        text: {
          cz: 'A co ty tady děláš?',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.drone,
        text: {
          cz: 'Taky divný. V jednu chvíli jsem na rutinní patrole, najednou se nade mnou otevřel nějaký portál, a ocitl jsem se v laboratoři, nebo co to bylo.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.drone,
        text: {
          cz: 'Pak se tam udělal trochu zmatek, tak jsem vlezl do nějaké ventilační šachty a o pár odboček později jsem skončil tady.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.drone,
        text: {
          cz: 'Je tu bezdrátová sít, tak jsem se zkusil připojit, abych zjistil kde to jsme, ale našel jsem jen nějaké špatně zabezpečené datové úložiště s mediálními soubory.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.drone,
        text: {
          cz: 'Tak jsem se schoval a trochu je tu procházím.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.robot,
        text: {
          cz: 'A? Přišel jsi na něco?',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.drone,
        text: {
          cz: 'Vůbec. Všechno to byly video soubory s prehistorickým kódováním.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.drone,
        text: {
          cz: 'Tady, koukni sám.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.drone,
        text: {
          cz: '',
          en: '',
        },
        actions: [ActionId.continueAnimation, ActionId.hide],
      },
      { actorId: ActorId.robot,
        text: {
          cz: 'Počkej, rychle to proletím...',
          en: '',
        },
        actions: [ActionId.show],
      },
      { actorId: ActorId.robot,
        text: {
          cz: '',
          en: '',
        },
        actions: [ActionId.continueAnimation, ActionId.hide],
      },
      { actorId: ActorId.robot,
        text: {
          cz: 'Uh. Stranger Things? Co to má jako být?',
          en: '',
        },
        actions: [ActionId.show],
      },
      { actorId: ActorId.drone,
        text: {
          cz: 'Počkej, to si děláš srandu, ne? Chceš říct, že jsi teď jako zkouknul tři série seriálu?',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.robot,
        text: {
          cz: 'Eh. Jsem to říkal, ne?',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.drone,
        text: {
          cz: 'No ty kráso.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.drone,
        text: {
          cz: 'A co tomu teda říkáš?',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.robot,
        text: {
          cz: 'Uhm.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.robot,
        text: {
          cz: 'Pojďme zkusit zmapovat terén, klábosit můžeme cestou.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.drone,
        text: {
          cz: 'Jasně. Pojďme.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.robot,
        text: {
          cz: '',
          en: '',
        },
        actions: [ActionId.continueAnimation, ActionId.hide, ActionId.enableAnimationSync],
      },

      // screen 2
      { actorId: ActorId.robot,
        text: {
          cz: 'No.',
          en: '',
        },
        actions: [ActionId.show, ActionId.disableAnimationSync],
      },
      { actorId: ActorId.robot,
        text: {
          cz: 'Jako v zásadě se mi to líbilo. Ale.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.drone,
        text: {
          cz: 'Ha, ha. Ale. To je dobré znamení.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.drone,
        text: {
          cz: '[šklíb]',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.robot,
        text: {
          cz: 'První série je jako hodně hororová. A je sympatické, že to celé má v zásadě ucelený příběhový oblouk, takže se na konci všechna zásadní tajemství vyřeší, a tak.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.robot,
        text: {
          cz: 'S čím mám problém, jsou postavy, nebo spíš jejich charakterizace.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.robot,
        text: {
          cz: 'Všichni jsou prvoplánově hrozně schematičtí, ale je zřejmé, že to nebudou tak docela stereotypní postavy dlouho.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.robot,
        text: {
          cz: 'Úplně z toho kouká takové to pomrkávání jako "hele, tady tohle je zlý hoch a sociálně slabší šprtka, ale počkejte, uvidíte co s nimi uděláme".',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.robot,
        text: {
          cz: 'Přitom o pár epizod později, už se chovají víceméně jako normální lidi. To mě hrozně iritovalo.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.robot,
        text: {
          cz: 'Další problém: jelikož nemám jejich kulturní pozadí, hodně věcí, které jsem identifikoval jako zjevné reference na něco, jsem asi úplně nedocenil.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.robot,
        text: {
          cz: 'Ale celkově dobrý. Záhada slušná, i když asi předvídatelná -- zlí vědci, ilegální pokusy, příšery a tak dále. Kompletní balík.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.drone,
        text: {
          cz: 'Heh. Zjevně to není problém jen tam, odkud ten seriál pochází...',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.robot,
        text: {
          cz: 'Ha, ha. Pravda.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.robot,
        text: {
          cz: '',
          en: '',
        },
        actions: [ActionId.continueAnimation, ActionId.hide, ActionId.enableAnimationSync],
      },

      // screen 3
      { actorId: ActorId.robot,
        text: {
          cz: 'Druhá série má zásadní problém v tom, že se záhada uspokojivě vyřešila na konci první.',
          en: '',
        },
        actions: [ActionId.show, ActionId.disableAnimationSync],
      },
      { actorId: ActorId.robot,
        text: {
          cz: 'Nevysvětlilo se všechno, ale bylo to dost na to, abych nepotřeboval vědět víc.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.robot,
        text: {
          cz: 'Takže příběh je dost slabý a celé to stojí na postavách.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.robot,
        text: {
          cz: 'Naštěstí jsou všichni dost charismatičtí na to, aby většinu dílů utáhli, takže je to pořád dá dívat. Plus pořád drží atmosféra -- malé město, velké zlo a tak dále. Však mi rozumíš.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.drone,
        text: {
          cz: 'Jojo, měl jsem z toho úplně stejný pocit.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.drone,
        text: {
          cz: 'Navíc na konci znova uzavřou ty záhady, co už uzavřeli v první sérii -- tentokrát snad doopravdy, takže si droid říká, kam tohle může pokračovat.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.robot,
        text: {
          cz: 'Přesně jak říkáš. Ale i když je tedy druhá série slabší, pořád většinou slušná úroveň.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.robot,
        text: {
          cz: '',
          en: '',
        },
        actions: [ActionId.continueAnimation, ActionId.hide, ActionId.enableAnimationSync],
      },

      // screen 4
      { actorId: ActorId.robot,
        text: {
          cz: 'Což nás přivádí ke třetí sérii.',
          en: '',
        },
        actions: [ActionId.show, ActionId.disableAnimationSync],
      },
      { actorId: ActorId.robot,
        text: {
          cz: 'Jak to říct.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.robot,
        text: {
          cz: 'Ve třetí sérii seriál totálně změní tón, a z hororu se stane... Hmm. Pornostalgie.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.drone,
        text: {
          cz: '[...]',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.robot,
        text: {
          cz: 'Pornostalgie. Chápeš?',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.drone,
        text: {
          cz: '[vzdych]',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.drone,
        text: {
          cz: 'Jasně. Moc pěkné.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.robot,
        text: {
          cz: 'Díky.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.robot,
        text: {
          cz: 'Takže některé postavy se totálně změní, a i když se seriál snaží trochu vysvětlit jak k tomu došlo, tak je jasné, že o konzistenci už nejde.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.robot,
        text: {
          cz: 'Zjevně se tvůrci rozhodli, že si užijí trochu legrace, a trochu víc vzpomínek na staré dobré časy a tak dál. A znova -- spousta referencí nedopadla na úrodnou půdu.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.robot,
        text: {
          cz: 'Ale, popravdě, ani mi to moc nevadilo. Je to jízda tak jako tak a je to zábavné sledovat. A vypadá to, že se všichni dobře bavili.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.drone,
        text: {
          cz: 'Mě trochu dráždilo, jak je to v některých ohledech didaktické.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.robot,
        text: {
          cz: 'Pravda. Některé scény a postavy byly tak okatě zlé, že bylo jasné, že cílem je dokumentovat nějakou sociální nespravedlnost nebo co to tam mají.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.robot,
        text: {
          cz: 'Já tohleto v médiích moc nemusím. Obávám se, že to nemá ten efekt, co si od toho lidi slibují.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.robot,
        text: {
          cz: 'Ale je to lehká zábava a mít dostatečné množství jednorozměrných padouchů je základ.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.robot,
        text: {
          cz: 'Takže asi tak.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.robot,
        text: {
          cz: 'Jo, a ten konec, to byl hrozný doják. Skoro mi z toho utekly provozní kapaliny.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.drone,
        text: {
          cz: 'Jop, měl jsem stejný problém.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.drone,
        text: {
          cz: 'Takže -- myslíš, že je toho čtvrtá série?',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.robot,
        text: {
          cz: 'Jestli to u nich funguje jako u nás, tak zcela nepochybně. Pokud ještě není, tak brzo bude.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.drone,
        text: {
          cz: 'Taky si myslím. Funguje to příliš dobře na to, aby to neprotahovali tak dlouho, až se na to nebude dát dívat.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.drone,
        text: {
          cz: 'Ale zatím dobrý, řekl bych.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.robot,
        text: {
          cz: 'Souhlas.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.drone,
        text: {
          cz: '[...]',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.drone,
        text: {
          cz: 'A hele, změna. Tady ta chodba konči.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.robot,
        text: {
          cz: 'Koukám.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.robot,
        text: {
          cz: 'Co je asi --',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.engineer,
        text: {
          cz: '',
          en: '',
        },
        actions: [ActionId.continueAnimation, ActionId.enableAnimationSync, ActionId.hide],
      },
      { actorId: ActorId.engineer,
        text: {
          cz: 'Ale copak to tu máme?',
          en: '',
        },
        actions: [ActionId.show],
      },
      { actorId: ActorId.engineer,
        text: {
          cz: 'Co to tu děláte, mrňousové?',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.engineer,
        text: {
          cz: '',
          en: '',
        },
        actions: [ActionId.continueAnimation, ActionId.hide],
      },
      { actorId: ActorId.engineer,
        text: {
          cz: 'Au.',
          en: '',
        },
        actions: [ActionId.show],
      },
      { actorId: ActorId.engineer,
        text: {
          cz: 'Ale no tak. To nebylo nutné.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.engineer,
        text: {
          cz: '',
          en: '',
        },
        actions: [ActionId.continueAnimation, ActionId.hide],
      },
      { actorId: ActorId.engineer,
        text: {
          cz: '',
          en: '',
        },
        actions: [ActionId.continueAnimation, ActionId.hide],
      },
    ];
  }
}