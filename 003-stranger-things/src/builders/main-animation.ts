import { GameObjects } from "phaser";

export class MainAnimationBuilder {
  currentTweenIndex = 0;
  animationTweens = new Array<Phaser.Tweens.Tween>();
  activeTween: Phaser.Tweens.Tween | null = null;
  stopped = false;

  background1: Phaser.GameObjects.Image | null = null;
  background2: Phaser.GameObjects.Image | null = null;
  background3: Phaser.GameObjects.Image | null = null;
  background4: Phaser.GameObjects.Image | null = null;

  engineer: Phaser.GameObjects.Image | null = null;
  engineerSpellSprite: Phaser.GameObjects.Sprite | null = null;
  engineerSpellSound: Phaser.Sound.BaseSound | null = null;

  droneSprite: Phaser.GameObjects.Sprite | null = null;

  robotSprite: Phaser.GameObjects.Sprite | null = null;
  robotStepSound: Phaser.Sound.BaseSound | null = null;
  robotBeepSound: Phaser.Sound.BaseSound | null = null;
  robotPowerShotSound: Phaser.Sound.BaseSound | null = null;

  trashContainer: Phaser.GameObjects.Image | null = null;
  suitcase: Phaser.GameObjects.Image | null = null;
  suitcaseSound: Phaser.Sound.BaseSound | null = null;

  preloadAssets(scene: Phaser.Scene) {
    scene.load.image('background-01', 'images/background-01.png');
    scene.load.image('background-02', 'images/background-02.png');
    scene.load.image('background-03', 'images/background-03.png');
    scene.load.image('background-04', 'images/background-04.png');

    scene.load.spritesheet('robot-idle', 'images/robot-idle.png', { frameWidth: 192, frameHeight: 180 });
    scene.load.spritesheet('robot-walk', 'images/robot-walk.png', { frameWidth: 220, frameHeight: 193 });
    scene.load.spritesheet('robot-shot', 'images/robot-shot.png', { frameWidth: 262, frameHeight: 186 });
    scene.load.spritesheet('robot-powershot', 'images/robot-powershot.png', { frameWidth: 685, frameHeight: 192 });

    scene.load.spritesheet('drone-idle', 'images/drone-idle.png', { frameWidth: 203, frameHeight: 126 });
    scene.load.spritesheet('drone-fly', 'images/drone-fly.png', { frameWidth: 200, frameHeight: 164 });
    scene.load.spritesheet('drone-turn', 'images/drone-turn.png', { frameWidth: 220, frameHeight: 204 });

    scene.load.image('trash-container', 'images/trash.png');
    scene.load.image('suitcase', 'images/suitcase.png');

    scene.load.image('engineer', 'images/engineer.png');
    scene.load.spritesheet('engineer-spell', 'images/engineer-spell.png', { frameWidth: 192, frameHeight: 192 });

    scene.load.audio('engineer-spell-sound', 'audio/engineer-spell.mp3');
    scene.load.audio('robot-powershot-sound', 'audio/robot-powershot.ogg');
    scene.load.audio('robot-beep-sound', 'audio/robot-beep.ogg');
    scene.load.audio('robot-step-sound', 'audio/robot-step.ogg');
  }

  createObjects(scene: Phaser.Scene) {
    const speedModifier = 1;

    const actorsY = 850;
    const robotFiringPosition = 800;

    this.background1 = scene.add.image(scene.game.scale.width / 2, scene.game.scale.height / 2, 'background-01').setOrigin(0.5, 0.5).setScale(0.65, 0.65).setVisible(false);
    this.background2 = scene.add.image(scene.game.scale.width / 2, scene.game.scale.height / 2, 'background-02').setOrigin(0.5, 0.5).setScale(0.65, 0.65).setVisible(false);
    this.background3 = scene.add.image(scene.game.scale.width / 2, scene.game.scale.height / 2, 'background-03').setOrigin(0.5, 0.5).setScale(0.65, 0.65).setVisible(false);
    this.background4 = scene.add.image(scene.game.scale.width / 2, scene.game.scale.height / 2, 'background-04').setOrigin(0.5, 0.5).setScale(0.65, 0.65).setVisible(false);

    this.robotSprite = scene.add.sprite(200, actorsY, 'robot').setScale(1, 1).setOrigin(0.5, 1).setDepth(10).setVisible(false);
    scene.anims.create({
      key: 'robot-idle',
      frames: scene.anims.generateFrameNames('robot-idle', { start: 0, end: 11 }),
      frameRate: 15,
      repeat: -1,
    });
    scene.anims.create({
      key: 'robot-walk',
      frames: scene.anims.generateFrameNames('robot-walk', { start: 0, end: 11 }),
      frameRate: 15,
      repeat: -1,
    });
    scene.anims.create({
      key: 'robot-shot',
      frames: scene.anims.generateFrameNames('robot-shot', { start: 0, end: 15 }),
      frameRate: 15,
    });
    scene.anims.create({
      key: 'robot-powershot',
      frames: scene.anims.generateFrameNames('robot-powershot', { start: 0, end: 17 }),
      frameRate: 15,
    });

    this.droneSprite = scene.add.sprite(1500, actorsY, 'drone').setScale(1, 1).setOrigin(0.5, 1).setScale(0.7).setVisible(false);
    scene.anims.create({
      key: 'drone-idle',
      frames: scene.anims.generateFrameNames('drone-idle', { start: 0, end: 11 }),
      frameRate: 12,
      repeat: -1,
    });
    scene.anims.create({
      key: 'drone-fly',
      frames: scene.anims.generateFrameNames('drone-fly', { start: 0, end: 11 }),
      frameRate: 15,
      repeat: -1,
    });
    scene.anims.create({
      key: 'drone-turn',
      frames: scene.anims.generateFrameNames('drone-turn', { start: 0, end: 15 }),
      frameRate: 15,
    });

    this.engineerSpellSprite = scene.add.sprite(0, actorsY, 'engineerSpell').setOrigin(0.5, 0.75).setScale(4).setVisible(false);
    scene.anims.create({
      key: 'spell-activate',
      frames: scene.anims.generateFrameNames('engineer-spell', { start: 0, end: 47 }),
      frameRate: 30,
    });

    this.engineer = scene.add.image(1500, 920, 'engineer').setOrigin(0.5, 1).setScale(0.6, 0.6).setVisible(false);
    this.trashContainer = scene.add.image(1500, actorsY, 'trash-container').setOrigin(0.5, 1).setScale(0.6, 0.6).setVisible(true);
    this.suitcase = scene.add.image(1500, actorsY - 10, 'suitcase').setOrigin(0.5, 1).setScale(0.6, 0.6).setDepth(5).setVisible(false);

    // sounds
    this.robotStepSound = scene.sound.add('robot-step-sound', {
      volume: 0.01,
      loop: true,
      rate: 3.5,
    });
    this.robotBeepSound = scene.sound.add('robot-beep-sound', {
      volume: 0.08,
      rate: 0.7,
    });
    this.robotPowerShotSound = scene.sound.add('robot-powershot-sound', {
      volume: 0.05,
      rate: 2,
    });
    this.engineerSpellSound = scene.sound.add('engineer-spell-sound', {
      volume: 0.07
    });


    // start, robot idling
    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.robotSprite,
      alpha: 1,
      duration: speedModifier * 2000,
      onStart: () => {
        this.robotSprite?.play('robot-idle');
      },
      onComplete: () => {
      },
    }));

    // robot moves closer to trash container
    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.robotSprite,
      x: robotFiringPosition,
      delay: speedModifier * 200,
      duration: speedModifier * 2000,
      onStart: () => {
        this.robotStepSound!.play();
        this.robotSprite?.play('robot-walk');
      },
      onComplete: () => {
        this.robotStepSound!.stop();
        this.robotSprite?.play('robot-idle');
      },
    }));

    // robot shoots
    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.robotSprite,
      x: robotFiringPosition + 200,
      delay: speedModifier * 200,
      duration: speedModifier * 1000,
      onStart: () => {
        this.robotPowerShotSound?.play();
        this.robotSprite?.setX(robotFiringPosition + 200);
        this.robotSprite?.play('robot-powershot');
      },
      onComplete: () => {
        this.robotSprite?.setX(robotFiringPosition);
        this.robotSprite?.play('robot-idle');
        this.playNext();
      },
    }));

    // trash container disappears
    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.trashContainer,
      alpha: 0,
      duration: speedModifier * 100,
      onStart: () => {
        this.suitcase?.setVisible(true);
      },
      onComplete: () => {
        this.playNext();
      },
    }));

    // drone flies out
    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.droneSprite,
      y: actorsY - 200,
      delay: speedModifier * robotFiringPosition,
      duration: speedModifier * 1000,
      onStart: () => {
        this.droneSprite?.play('drone-turn').setVisible(true);
      },
      onComplete: () => {
        this.droneSprite?.setFlipX(true).play('drone-idle');
      },
    }));

    // drone transmits data
    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.droneSprite,
      x: { from: 1490, to: 1510 },
      ease: Phaser.Math.Easing.Sine.InOut,
      repeat: 10,
      duration: speedModifier * 50,
      yoyo: true,
      onStart: () => {
        this.robotBeepSound!.play();
        this.droneSprite?.stop();
      },
      onComplete: () => {
        this.droneSprite?.play('drone-idle');
        this.droneSprite?.setX(1500);
      },
    }));

    // robot supercomputes
    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.robotSprite,
      scale: { from: 0.95, to: 1 },
      ease: Phaser.Math.Easing.Sine.InOut,
      repeat: 10,
      duration: speedModifier * 50,
      yoyo: true,
      onStart: () => {
        this.robotBeepSound!.play();
        this.robotSprite?.setTint(0xff0000);
        this.robotSprite?.stop();
      },
      onComplete: () => {
        this.robotSprite?.setTint(0xffffff);
        this.robotSprite?.play('robot-idle');
      },
    }));

    // they both move to the next screen
    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.robotSprite,
      x: scene.scale.width + this.robotSprite?.width * 3,
      delay: speedModifier * 200,
      duration: speedModifier * 2000,
      onStart: () => {
        this.robotStepSound!.play();
        this.robotSprite?.play('robot-walk');
      },
      onComplete: () => {
        this.robotStepSound!.stop();
        this.droneSprite?.play('drone-turn');
        this.playNext();
      },
    }));

    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.droneSprite,
      x: scene.scale.width + this.droneSprite.width * 2,
      delay: speedModifier * 1000,
      duration: speedModifier * 1000,
      onStart: () => {
        this.droneSprite?.setFlipX(false).play('drone-fly');
      },
      onComplete: () => {
        this.playNext();
      },
    }));

    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.droneSprite,
      x: scene.scale.width * 0.75,
      delay: speedModifier * 200,
      duration: speedModifier * 3000,
      onStart: () => {
        this.droneSprite?.setX(-2 * this.droneSprite?.width);
        this.robotSprite?.setX(-2 * this.robotSprite?.width);
        this.suitcase?.setVisible(false);
        this.background1?.setVisible(false);
        this.background2?.setVisible(true);
      },
      onComplete: () => {
        this.droneSprite?.play('drone-turn');
        this.playNext();
      },
    }));

    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.droneSprite,
      delay: speedModifier * 1000,
      duration: speedModifier * 1000,
      onStart: () => {
        this.droneSprite?.play('drone-turn');
        this.droneSprite?.setX(scene.scale.width * 0.75);
      },
      onComplete: () => {
        this.playNext();
      },
    }));

    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.robotSprite,
      x: scene.scale.width * 0.25,
      delay: speedModifier * 1000,
      duration: speedModifier * 1500,
      onStart: () => {
        this.robotStepSound!.play();
        this.droneSprite?.setFlipX(true).play('drone-idle');
      },
      onComplete: () => {
        this.robotStepSound!.stop();
        this.robotSprite?.play('robot-idle');
      },
    }));

    // they both move to the next screen
    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.robotSprite,
      x: scene.scale.width + this.robotSprite?.width * 3,
      delay: speedModifier * 200,
      duration: speedModifier * 2000,
      onStart: () => {
        this.robotStepSound!.play();
        this.robotSprite?.play('robot-walk');
      },
      onComplete: () => {
        this.robotStepSound!.stop();
        this.droneSprite?.play('drone-turn');
        this.playNext();
      },
    }));

    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.droneSprite,
      x: scene.scale.width + this.droneSprite.width * 2,
      delay: speedModifier * 1000,
      duration: speedModifier * 1000,
      onStart: () => {
        this.droneSprite?.setFlipX(false).play('drone-fly');
      },
      onComplete: () => {
        this.playNext();
      },
    }));

    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.droneSprite,
      x: scene.scale.width * 0.75,
      delay: speedModifier * 200,
      duration: speedModifier * 3000,
      onStart: () => {
        this.droneSprite?.setX(-2 * this.droneSprite?.width);
        this.robotSprite?.setX(-2 * this.robotSprite?.width);
        this.suitcase?.setVisible(false);
        this.background2?.setVisible(false);
        this.background3?.setVisible(true);
      },
      onComplete: () => {
        this.droneSprite?.play('drone-turn');
        this.playNext();
      },
    }));

    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.droneSprite,
      delay: speedModifier * 1000,
      duration: speedModifier * 1000,
      onStart: () => {
        this.droneSprite?.play('drone-turn');
        this.droneSprite?.setX(scene.scale.width * 0.75);
      },
      onComplete: () => {
        this.playNext();
      },
    }));

    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.robotSprite,
      x: scene.scale.width * 0.25,
      delay: speedModifier * 1000,
      duration: speedModifier * 1500,
      onStart: () => {
        this.robotStepSound!.play();
        this.droneSprite?.setFlipX(true).play('drone-idle');
      },
      onComplete: () => {
        this.robotStepSound!.stop();
        this.robotSprite?.play('robot-idle');
      },
    }));

    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.robotSprite,
      x: scene.scale.width + this.robotSprite?.width * 3,
      delay: speedModifier * 200,
      duration: speedModifier * 3000,
      onStart: () => {
        this.robotStepSound!.play();
        this.robotSprite?.play('robot-walk');
      },
      onComplete: () => {
        this.robotStepSound!.stop();
        this.droneSprite?.play('drone-turn');
        this.playNext();
      },
    }));

    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.droneSprite,
      x: scene.scale.width + this.droneSprite.width * 2,
      delay: speedModifier * 1000,
      duration: speedModifier * 1500,
      onStart: () => {
        this.droneSprite?.setFlipX(false).play('drone-fly');
      },
      onComplete: () => {
        this.playNext();
      },
    }));

    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.droneSprite,
      x: scene.scale.width * 0.75,
      delay: speedModifier * 200,
      duration: speedModifier * 3000,
      onStart: () => {
        this.droneSprite?.setX(-2 * this.droneSprite?.width);
        this.robotSprite?.setX(-2 * this.robotSprite?.width);
        this.suitcase?.setVisible(false);
        this.background3?.setVisible(false);
        this.background4?.setVisible(true);
      },
      onComplete: () => {
        this.droneSprite?.play('drone-turn');
        this.playNext();
      },
    }));

    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.droneSprite,
      delay: speedModifier * 1000,
      duration: speedModifier * 1000,
      onStart: () => {
        this.droneSprite?.play('drone-turn');
        this.droneSprite?.setX(scene.scale.width * 0.75);
      },
      onComplete: () => {
        this.playNext();
      },
    }));

    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.robotSprite,
      x: scene.scale.width * 0.25,
      delay: speedModifier * 1000,
      duration: speedModifier * 1500,
      onStart: () => {
        this.robotStepSound!.play();
        this.droneSprite?.setFlipX(true).play('drone-idle');
      },
      onComplete: () => {
        this.robotStepSound!.stop();
        this.robotSprite?.play('robot-idle');
      },
    }));

    // drone turns
    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.droneSprite,
      duration: speedModifier * 1000,
      onStart: () => {
        this.droneSprite?.play('drone-turn');
      },
      onComplete: () => {
        this.droneSprite?.setFlipX(false).play('drone-fly');
        this.playNext();
      },
    }));

    // robot moves to firing position
    this.animationTweens.push(scene.add.tween({
      paused: true,
      x: robotFiringPosition,
      targets: this.robotSprite,
      duration: speedModifier * 1000,
      onStart: () => {
        this.robotStepSound!.play();
        this.robotSprite?.play('robot-walk');
        this.playNext();
      },
      onComplete: () => {
        this.robotStepSound!.stop();
        this.robotSprite?.play('robot-idle');
      },
    }));

    // drone moves away
    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.droneSprite,
      x: robotFiringPosition,
      duration: speedModifier * 1000,
      onStart: () => {
        this.playNext();
      },
      onComplete: () => {
        this.droneSprite?.play('drone-idle');
      },
    }));

    // engineer enters
    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.engineer,
      x: 1500,
      duration: speedModifier * 3000,
      onStart: () => {
        this.engineer?.setVisible(true);
      },
      onComplete: () => {
      },
    }));

    // robot shoots
    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.robotSprite,
      x: robotFiringPosition + 200,
      delay: speedModifier * 200,
      duration: speedModifier * 1000,
      onStart: () => {
        this.robotPowerShotSound?.play();
        this.robotSprite?.setX(robotFiringPosition + 200);
        this.robotSprite?.play('robot-powershot');
      },
      onComplete: () => {
        this.robotSprite?.setX(robotFiringPosition);
        this.robotSprite?.play('robot-idle');
      },
    }));

    // engineer casts spell
    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: [this.droneSprite, this.robotSprite],
      alpha: 0,
      duration: speedModifier * 1000,
      onStart: () => {
        this.engineerSpellSound?.play();
        this.engineerSpellSprite?.setX(robotFiringPosition).setVisible(true).play('spell-activate');
      },
      onComplete: () => {
      },
    }));

    // engineer leaves
    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.engineer,
      x: scene.scale.width + 1.5 * this.engineer?.width,
      duration: speedModifier * 3000,
      onStart: () => {
        this.engineer?.setVisible(true);
      },
      onComplete: () => {
      },
    }));
  }

  startAnimation(scene: Phaser.Scene) {
    this.stopped = false;
    this.currentTweenIndex = 0;
    this.background1?.setVisible(true);
    this.robotSprite?.play('robot-idle').setVisible(true);
    this.engineer?.setX(scene.scale.width + 1.5 * this.engineer?.width).setVisible(false);
    this.animationTweens.forEach(t => t.setTimeScale(1));

    this.playNext();
  }


  stopAnimation() {
    this.stopped = true;
    this.animationTweens.forEach(tween => {
      tween.stop();
      tween.setTimeScale(1);
    });
  }

  playNext() {
    if (this.stopped) {
      return;
    }

    if (this.currentTweenIndex < this.animationTweens.length) {
      this.activeTween = this.animationTweens[this.currentTweenIndex++];
      this.activeTween?.play();
    }
  }

  isAnimationPlaying() {
    return this.activeTween?.isPlaying() ?? false;
  }

  skip() {
    if (this.stopped) {
      return;
    }

    this.activeTween?.setTimeScale(100);
  }

}