export enum LanguageCode {
  cz = 'cz',
  en = 'en',
}

export type LocalizedText = {
  cz: string;
  en: string;
};

export const LanguageMap = new Map<LanguageCode, string> ([
  [LanguageCode.cz, "Česky"],
  [LanguageCode.en, "English"]
]);

export const LanguageList = [
  LanguageCode.cz,
  // LanguageCode.en,
];

export const LanguageRegistryKey = 'lang';


export function getLocalizedText(localizedText: LocalizedText, lang: keyof LocalizedText): string {
  return localizedText[lang];
}
