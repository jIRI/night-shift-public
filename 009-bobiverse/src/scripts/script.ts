import { AvailableSounds, SoundPlayerBuilder, Sounds } from "../builders/sound-player";
import { LocalizedText } from "../types/lang";

export enum ActorId {
  left,
  right,
  robot,
  drone,
  dummy,
}

export enum ActionId {
  playAnimation,
  continueAnimation,
  finishAnimation,
  show,
  hide,
  enableAnimationSync,
  disableAnimationSync,
  playSound,
}

export type ActionConfig = {
  do: ActionId.playAnimation
  | ActionId.continueAnimation
  | ActionId.finishAnimation
  | ActionId.show
  | ActionId.hide
  | ActionId.enableAnimationSync
  | ActionId.disableAnimationSync,
} | {
  do: ActionId.playSound,
  sound: AvailableSounds
};

export type ActorConfig = {
  id: ActorId;
  name: string;
  nameEncoder?: (text:LocalizedText) => LocalizedText;
  textEncoder?: (text:LocalizedText) => LocalizedText;
};

export type SelectItem = {
  text: string;
  gotoLabel: string;
};

export type Line = {
  actorId: ActorId;
  text: LocalizedText;
  visited?: boolean;
  actions?: Array<ActionConfig>;
  label?: string;
  select?: Array<SelectItem>;
}

export function getLocalizedText(localizedText: LocalizedText, lang: keyof LocalizedText): string {
  return localizedText[lang];
}

const telepathicEncoderReplacers: Array<(s: string) => string> = [
  s => s.replace(/[AÁ]/g, 'GR'),
  s => s.replace(/[EÉĚ]/g, 'RR'),
  s => s.replace(/[IÍ]/g, 'QR'),
  s => s.replace(/[OÓ]/g, 'RX'),
  s => s.replace(/[UÚŮ]/g, 'WR'),
  s => s.replace(/[YÝ]/g, 'TQ'),
  s => s.replace(/[ŠČŘŽĎŇ]/g, 'WT'),
];

export function applyReplacers(text: string, replacers: Array<(s: string) => string>): string {
  let result = text.toUpperCase();
  for(const replacer of replacers) {
    result = replacer(result);
  }
  return result;
}

export function applyReplacersToParts(text: string, replacers: Array<(s: string) => string>): string {
  let result = '';
  let parts = text.split('|');
  while(parts.length > 0) {
    result = result
      .concat(applyReplacers(parts.shift() ?? '', replacers))
      .concat(parts.shift() ?? '');
  }
  return result;
}

export class Script {
  currentLineIndex = 0;
  lines: Array<Line>;
  actorMap = new Map<ActorId, ActorConfig>();

  constructor() {
    this.lines = Script.lines();
    Script.actors().forEach(actor => this.actorMap.set(actor.id, actor));
  }

  actor(id: ActorId): ActorConfig {
    return this.actorMap.get(id) ?? Script.actors()[0];
  }

  reset() {
    this.currentLineIndex = -1;
    this.lines.forEach(line => line.visited = false);
  }

  nextLine(input?: string): Line | null {
    if (this.currentLineIndex < 0) {
      // first invocation, just return first line
      this.currentLineIndex = 0;
    } else {
      if (input != null ) {
        // we got input, try find index of the label
        const nextLineIndex = this.lines.findIndex(line => line.label === input);
        if (nextLineIndex >= 0) {
          // label found, jump to label
          this.currentLineIndex = nextLineIndex;
        } else {
          // label not found, well, continue on next line...
          this.currentLineIndex++;
        }
      } else {
        // no input, just continue on next line
        this.currentLineIndex++;
      }
    }

    if (this.currentLineIndex >= this.lines.length) {
      return null;
    }

    this.lines[this.currentLineIndex].visited = true;
    const line = Object.assign({}, this.lines[this.currentLineIndex]);
    const textEncoder = this.actor(line.actorId).textEncoder;
    if( textEncoder != null) {
      line.text = textEncoder(line.text);
    }
    return line;
  }

  //
  // Actors and script definition follows
  //

  static actors(): Array<ActorConfig>  {
    return [
      { id: ActorId.left, name: 'Choraqorax' },
      { id: ActorId.right, name: 'Rutygamurosus' },
    ];
  }

  static telepathicEncoder(text: LocalizedText): LocalizedText {
    if(text.cz.startsWith('[') || text.en.startsWith('[')) {
      return text;
    }

    let cz = text.cz;
    let en = text.en;

    if(cz.startsWith('||') || en.startsWith('||')) {
      cz = cz.substr(2);
      en = en.substr(2);
    } else {
      cz = `<${applyReplacersToParts(text.cz, telepathicEncoderReplacers)}>`;
      en = `<${applyReplacersToParts(text.en, telepathicEncoderReplacers)}>`;
    }

    return {
      cz,
      en,
    };
  }

  static droidEncoder(text: LocalizedText): LocalizedText {
    if(text.cz.startsWith('') || text.en.startsWith('')) {
      return text;
    }

    let cz = `{ ${text.cz} }`;
    let en = `{ ${text.en} }`;

    return {
      cz,
      en,
    };
  }

  static lines(): Array<Line> {
    return [
      { actorId: ActorId.left,
        text: {
          cz: '...takže, abych to shrnul: je to pěkně v pytli.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Předchůdci, jo? Měl jsem za to, že to je jen nějaká spiklenecká teorie, nebo tak něco. ',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Ne, ne. Předchůdci jsou zcela zjevně skutečná záležitost.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'A chápu to dobře, že Umakvacelok si myslí, že ty tenepy jsou pozůstatek nějakého experimentu s multimodálním entaglementem?',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Ano. Ale je téměř jisté, že ta otravná invaze co jsme teď sanovali, je jen součástí nějakého jiného plánu, který s těmi tenepy souvisí.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Jako že by ta skupinka, co při bránování musela spálit planetu, dokázala pracovat s technologií které my nerozumíme?',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Ti? Ne, to byli jen užiteční idioti. Buď to byl zastírací manévr, a smog z jejich bránování měl zakrýt nějaký delikátnější průnik, a nebo dostali za úkol sem něco pronést.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Uh, to nezní dobře.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'To nezní. Máme seznam civilizací, které by, teoreticky, mohly zvládnout manipulovat s tenepy, ale u většiny nedává smysl proč by to chtěli udělat.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'No, jestli ony nám nenastanou zajímavé časy.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Taky se bojím. ',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Jak tohle všechno zvládáš?',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'No je to bída. Ještě že mám ty pozemské knížky.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Ha, ha. Taky jsi to mu propadl?',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Neřekl bych propadl, ale je to dobrý, když potřebuješ na chvilku vypnout.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'A co čteš?',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Hele, napojil jsem se rovnou na Yoraggoraxurose, tak ten mi vybírá takovou odpočinkovou literaturu. Zrovna teď jsem dočetl první tři díly Bobiverse.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Bobi-co?',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Ah, ty knížky se jmenují ve skutečnosti jinak, ale je to o chlápkovi co se jmenuje Bob a stane se z něj samoreplikující vesmírná loď, která postupně kolonizuje Zemi přilehlý vesmír. Takže Bobiverse.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.right,
        text: {
          cz: '[šklíb]',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'No ano, je to blbost. Ale většinou je to vtipné, trochu to hraje na nostalgickou vlnu, takže jsou tam nějaké reference na pozemská média, a vesměs to má zlaté srdce.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Do toho nějací ti antagonisté ze Země a možná časem i odjinud, nějaká nezávadná politika, a tak dále.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Pokud potřebuješ na chvíli vypnout mozek a jen tak se trochu bavit, mohl bys dopadnout podstatně hůř.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Dobře, bobře, já tě nesoudím.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.left,
        text: {
          cz: '[checht]',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Jak to popisuješ, předpokládám, že to nejsou nějaké tlustopisy. Krátké, svižné, nenáročné?',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Přesně.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Tak mi to šoupni, já se na to zkusím podívat, když si tedy myslíš, že to není ztráta času.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.left,
        text: {
          cz: '[šoup]',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.right,
        text: {
          cz: '[chmát]',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Ale jestli se mi to nebude líbit, řeknu šéfce, že bys chtěl udělat prezentaci pro celý barák.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.right,
        text: {
          cz: '[šklíb]',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.left,
        text: {
          cz: '[šklíb]',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Moc vtipné.',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'No hele, díky za tip. Špatné zprávy nějak opatrně předám ředitelce, je dost pravděpodobné, že to bude chtít slyšet ještě osobně. Tak se...',
          en: '',
        },
        actions: [ ],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Ne, ne, ne, ne. Ne sirénu.',
          en: '',
        },
        actions: [ { do: ActionId.playSound, sound: 'alarm' }, { do: ActionId.enableAnimationSync} ],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Co to zase...',
          en: '',
        },
        actions: [ { do: ActionId.playAnimation} ],
      },
   ];
  }
}