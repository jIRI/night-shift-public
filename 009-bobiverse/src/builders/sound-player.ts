export class Sounds {
  alarm: Phaser.Sound.BaseSound | null = null;
  doorOpen: Phaser.Sound.BaseSound | null = null;
}

export type AvailableSounds = keyof Sounds;

export class SoundPlayerBuilder extends Sounds {
  preloadAssets(scene: Phaser.Scene) {
    scene.load.audio('alarm-sound', 'audio/alarm.mp3');
    scene.load.audio('door-open-sound', 'audio/open-door.mp3');
  }

  createObjects(scene: Phaser.Scene) {
    this.alarm = scene.sound.add('alarm-sound', {
      volume: 0.1,
      rate: 1,
    });
    this.doorOpen = scene.sound.add('door-open-sound', {
      volume: 0.06,
      rate: 1.1,
    });
  }

  play(key: AvailableSounds) {
    switch(key) {
      case 'alarm':
        this.alarm?.play();
        return;
      case 'doorOpen':
        this.doorOpen?.play();
        return;
      default:
        return;
    }
  }
}