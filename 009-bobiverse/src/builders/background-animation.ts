import { SoundPlayerBuilder } from "./sound-player";

export class BackgroundAnimationBuilder {
  soundPlayer: SoundPlayerBuilder | null = null;

  currentTweenIndex = 0;
  animationTweens = new Array<Phaser.Tweens.Tween>();
  activeTween: Phaser.Tweens.Tween | null = null;
  stopped = false;

  door: Phaser.GameObjects.Image | null = null;
  suitcase: Phaser.GameObjects.Image | null = null;

  preloadAssets(scene: Phaser.Scene) {
    scene.load.image('background-3', 'images/background-3.png');
    scene.load.image('suitcase', 'images/suitcase.png');
  }

  createObjects(scene: Phaser.Scene, soundPlayer: SoundPlayerBuilder) {
    this.soundPlayer = soundPlayer;

    const speedModifier = 1;
    const suitcaseY = scene.scale.height / 5;
    this.door = scene.add.image(scene.scale.width / 2, 0, 'background-3').setOrigin(0.5, 0).setDepth(-2).setVisible(true);

    this.suitcase = scene.add.image(scene.scale.width / 2, suitcaseY, 'suitcase').setOrigin(0.5, 0.5).setScale(0.4, 0.4).setTint(0x707070).setDepth(-5).setVisible(true);



    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.suitcase,
      y: { from: suitcaseY - 10, to: suitcaseY + 10 },
      yoyo: true,
      repeat: -1,
      duration: speedModifier * 1500,
      onStart: () => {
        this.playNext();
      },
    }));
    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.door,
      y: { from: 0, to: 20 },
      scale: { from: 1, to: 0.9 },
      duration: speedModifier * 800,
      onStart: () => {
        soundPlayer.play('doorOpen');
        this.playNext();
      },
      onComplete: () => {
      },
    }));
    this.animationTweens.push(scene.tweens.addCounter({
      paused: true,
      from: 255,
      to: 100,
      duration: speedModifier * 800,
      onUpdate: (tween) => {
          const value = Math.floor(tween.getValue());
          this.door?.setTint(Phaser.Display.Color.GetColor(value, value, value));
      },
      onComplete: () => {
        this.playNext();
      }
    }));
    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.door,
      x: { from: scene.scale.width / 2, to: scene.scale.width / 2 - this.door.width },
      duration: speedModifier * 400,
      onComplete: () => {
      },
    }));
  }

  startAnimation(scene: Phaser.Scene) {
    this.stopped = false;
    this.currentTweenIndex = 0;
    this.animationTweens.forEach(t => t.setTimeScale(1));

    this.playNext();
  }

  stopAnimation() {
    this.stopped = true;
    this.animationTweens.forEach(tween => {
      tween.stop();
      tween.setTimeScale(1);
    });
  }

  playNext() {
    if (this.stopped) {
      return;
    }

    if (this.currentTweenIndex < this.animationTweens.length) {
      this.activeTween = this.animationTweens[this.currentTweenIndex++];
      this.activeTween?.play();
    }
  }

  isAnimationPlaying() {
    return this.activeTween?.isPlaying() ?? false;
  }

  skip() {
    if (this.stopped) {
      return;
    }

    this.activeTween?.setTimeScale(100);
  }

}