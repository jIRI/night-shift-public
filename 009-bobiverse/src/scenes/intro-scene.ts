import {LanguageSelectorBuilder} from '../builders/language-selector';
import { getLocalizedText, LanguageRegistryKey, LocalizedText } from '../types/lang';

export class IntroScene extends Phaser.Scene {
  text: Phaser.GameObjects.Text | null = null;
  introSound: Phaser.Sound.BaseSound | null = null;
  music: Phaser.Sound.BaseSound | null = null;
  languageSelector = new LanguageSelectorBuilder();
  titleText: Phaser.GameObjects.Text | null = null;
  subtitleText: Phaser.GameObjects.Text | null = null;
  mouseDown = false;
  ignoreInput = false;

  titleLocalizedText: LocalizedText = {
    cz: 'NOČNÍ ŠICHTA',
    en: 'NIGHT SHIFT'
  };
  subtitleLocalizedText: LocalizedText = {
    cz: 'ep. 9: Regrese',
    en: 'ep. 9: Regression'
  };

  constructor() {
    super({ key: 'IntroScene' });
  }

  preload(): void {
    this.load.audio('intro', 'audio/intro.mp3');
    this.load.audio('music', 'audio/music.ogg');

    // ensure font is loaded prior to rendering
    (this.load as any).rexWebFont({
      custom: {
          families: ['IntroFont'],
          urls: [
            'fonts.css',
          ]
      }
    });

  }

  create(): void {
    this.languageSelector.createObjects(this);

    this.titleText = this.add.text(
      0.5 * this.game.scale.width, 0.5 * this.game.scale.height,
      this.titleLocalizedText.cz,
      {
        fontFamily: 'IntroFont',
        fontSize: '10em',
        color: '#14EAE8',
        align: 'center',
    }).setOrigin(0.5, 0.5);

    this.subtitleText = this.add.text(
      0.5 * this.game.scale.width, 0.6 * this.game.scale.height,
      this.subtitleLocalizedText.cz,
      {
        fontFamily: 'IntroFont',
        fontSize: '5em',
        color: '#14EAE8',
        align: 'center',
    }).setOrigin(0.5, 0.5);


    this.registerInputHandlers();

    this.cameras.main.once(Phaser.Cameras.Scene2D.Events.FADE_OUT_COMPLETE, () => {
      this.introSound?.stop();
      this.scene.start('MainScene');
    });

    this.introSound = this.sound.add('intro', {
      volume: 0.15,
    });
    this.introSound.play();

    this.music = this.sound.add('music', {
      loop: true,
      volume: 0.1,
    });
    this.music.play();

    this.cameras.main.fadeIn(1000, 0, 0, 0);
  }

  private registerInputHandlers() {
    this.input.keyboard.enableGlobalCapture();
    this.input.keyboard.addCapture(Phaser.Input.Keyboard.KeyCodes.SPACE);
    this.input.keyboard.addCapture(Phaser.Input.Keyboard.KeyCodes.ENTER);
    this.input.keyboard.addCapture(Phaser.Input.Keyboard.KeyCodes.UP);
    this.input.keyboard.addCapture(Phaser.Input.Keyboard.KeyCodes.DOWN);
        this.ignoreInput = this.game.scene.isActive('IntroScene');
    this.input.on('gameobjectdown', () => {
      if (this.ignoreInput) {
        return;
      }
      this.refreshTexts();
      this.startTransition();
    }, this);

    this.input.keyboard.on('keydown', (event: any) => {
      if (this.ignoreInput) {
        return;
      }
      switch (event.keyCode) {
        case Phaser.Input.Keyboard.KeyCodes.SPACE:
        case Phaser.Input.Keyboard.KeyCodes.ENTER:
          this.startTransition();
          break;
        default:
          this.refreshTexts();
          break;
      }
    });
  }

  private refreshTexts() {
    this.titleText?.setText(getLocalizedText(this.titleLocalizedText, this.game.registry.get(LanguageRegistryKey)));
    this.subtitleText?.setText(getLocalizedText(this.subtitleLocalizedText, this.game.registry.get(LanguageRegistryKey)));
  }

  private startTransition() {
    this.ignoreInput = true;
    this.cameras.main.fadeOut(1000, 0, 0, 0);
  }
}
