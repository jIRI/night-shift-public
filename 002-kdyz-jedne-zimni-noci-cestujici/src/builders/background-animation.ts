import { GameObjects } from "phaser";

export class BackgroundAnimationBuilder {
  currentTweenIndex = 0;
  animationTweens = new Array<Phaser.Tweens.Tween>();
  stopped = false;

  extra1: Phaser.GameObjects.Image | null = null;
  extra2: Phaser.GameObjects.Image | null = null;
  scientist: Phaser.GameObjects.Image | null = null;


  preloadAssets(scene: Phaser.Scene) {
    scene.load.image('extra1', 'images/extras-left.png');
    scene.load.image('extra2', 'images/extras-right.png');
    scene.load.image('scientist', 'images/right.png');
  }

  createObjects(scene: Phaser.Scene) {
    const tintColor = 0xa0a0a0;
    const actorsY = 480;
    const speedModifier = 1;

    this.extra1 = scene.add.image(0, actorsY, 'extra1').setScale(0.25, 0.25).setOrigin(0.5, 1).setTint(tintColor).setVisible(true);
    this.extra2 = scene.add.image(0, actorsY, 'extra2').setScale(0.25, 0.25).setOrigin(0.5, 1).setTint(tintColor).setVisible(true);

    this.scientist = scene.add.image(0, actorsY, 'scientist').setScale(0.25, 0.25).setOrigin(0.5, 1).setTint(tintColor).setVisible(true);
    // scientist's vertical wobble
    scene.add.tween({
      targets: this.scientist,
      y: { from: 480, to: 483 },
      ease: Phaser.Math.Easing.Sine.InOut,
      repeat: -1,
      duration: 2000,
      yoyo: true,
    });

    // scientist peeks
    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.scientist,
      x: scene.scale.width - this.scientist?.width / 14,
      delay: speedModifier * 500,
      duration: speedModifier * 1000,
      onComplete: () => this.playNext(),
    }));

    // extras look at scientist
    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.extra1,
      delay: speedModifier * 1000,
      x: scene.scale.width / 2,
      duration: speedModifier * 100,
      completeDelay: speedModifier * 1000,
      onStart: () => this.extra1?.setFlipX(false),
      onComplete: () => this.playNext(),
    }));

    // extra 1 runs
    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.extra1,
      x: -this.extra1?.width * 1.25,
      duration: speedModifier * 2000,
      onStart: () => {
        this.extra1?.setFlipX(true);
        this.playNext();
      },
    }));

    // extra 2 runs
    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.extra2,
      x: -this.extra2?.width * 1.25,
      duration: speedModifier * 2000,
      onStart: () => this.playNext(),
    }));

    // scientist follows
    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.scientist,
      x: scene.scale.width / 2,
      duration: speedModifier * 1500,
      onComplete: () => this.playNext(),
    }));

    // scientist looks right
    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.scientist,
      x: scene.scale.width / 2 + this.scientist?.width / 10,
      delay: speedModifier * 1000,
      duration: speedModifier * 1500,
      onStart: () => this.scientist?.setFlipX(true),
      onComplete: () => this.playNext(),
    }));

    // scientist looks left
    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.scientist,
      x: scene.scale.width / 2 - this.scientist?.width / 10,
      delay: speedModifier * 1000,
      duration: speedModifier * 1500,
      onStart: () => this.scientist?.setFlipX(false),
      onComplete: () => this.playNext(),
    }));


    // scientist moves to right
    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.scientist,
      x: scene.scale.width + this.scientist.width,
      delay: speedModifier * 1000,
      duration: speedModifier * 2000,
      onStart: () => this.scientist?.setFlipX(true),
      onComplete: () => this.stopAnimation(),
    }));
  }

  startAnimation(scene: Phaser.Scene) {
    this.stopped = false;
    this.scientist?.setX(scene.scale.width + this.scientist?.width).setVisible(true);
    this.extra1?.setX(scene.scale.width / 2 + this.extra1?.width / 4).setFlipX(true).setVisible(true);
    this.extra2?.setX(scene.scale.width / 2 - this.extra2?.width / 4).setVisible(true);
    this.currentTweenIndex = 0;
    this.playNext();
  }

  stopAnimation() {
    this.stopped = true;
    this.scientist?.setVisible(false);
    this.extra1?.setVisible(false);
    this.extra2?.setVisible(false);
    this.animationTweens.forEach(t => t.stop());
  }

  playNext() {
    if( this.stopped ) {
      return;
    }

    this.animationTweens[this.currentTweenIndex++].play();
  }
}