import { BackgroundAnimationBuilder } from '../builders/background-animation';
import { DialogBox } from '../objects/dialog-box';
import { Speaker, SpeakerPosition, SpeakerState } from '../objects/speaker';
import { ActionId, ActorId, Line, Script } from '../scripts/script';
import { getLocalizedText, LanguageRegistryKey } from '../types/lang';

export class MainScene extends Phaser.Scene {
  textBox: DialogBox | null = null;
  speakers = new Map<ActorId, Speaker>();

  leftSpeaker: Speaker | null = null;
  rightSpeakerFirst: Speaker | null = null;
  rightSpeaker: Speaker | null = null;
  mouseDown: boolean = false;
  backgroundAnimBuilder = new BackgroundAnimationBuilder();
  script = new Script();
  ignoreInput = false;

  constructor() {
    super({ key: 'MainScene', active: false });
  }

  preload(): void {
    this.createLoadingProgressBar();

    this.load.image('background', 'images/background.png');
    this.load.image('left', 'images/left.png');
    this.load.image('rightFirst', 'images/right-first.png');
    this.load.image('right', 'images/right.png');
    this.load.image('dialogContinue', 'images/dialog_continue.png');
    this.load.image('dialogContinueHighlight', 'images/dialog_continue_hl.png');
    this.load.image('dialogPanel', 'images/dialog_panel.png');
    this.load.image('dialogName', 'images/dialog_name.png');
    this.backgroundAnimBuilder.preloadAssets(this);
   }

  create(): void {
    this.add.image(0, 0, 'background').setOrigin(0, 0);

    this.backgroundAnimBuilder.createObjects(this);

    this.leftSpeaker = new Speaker({
      scene: this,
      textureName: 'left',
      initialState: SpeakerState.inactive,
      position: SpeakerPosition.leftBottom,
      activeScale: 0.75,
      inactiveScale: 0.65,
    });
    this.speakers.set(ActorId.left, this.leftSpeaker);

    this.rightSpeakerFirst = new Speaker({
      scene: this,
      initialState: SpeakerState.inactive,
      textureName: 'rightFirst',
      position: SpeakerPosition.rightBottom,
      activeScale: 0.75,
      inactiveScale: 0.65,
      originOffset: {x: -0.35, y: 0},
    });
    this.speakers.set(ActorId.rightFirst, this.rightSpeakerFirst);

    this.rightSpeaker = new Speaker({
      scene: this,
      initialState: SpeakerState.hidden,
      textureName: 'right',
      position: SpeakerPosition.rightBottom,
      activeScale: 0.9,
      inactiveScale: 0.8,
      originOffset: {x: -0.25, y: 0.1},
    });
    this.speakers.set(ActorId.right, this.rightSpeaker);

    this.textBox = new DialogBox({
      scene: this,
      textureNames: {
        name: 'dialogName',
        box: 'dialogPanel',
        continue: 'dialogContinue',
        continueHighlighted: 'dialogContinueHighlight',
      }
    });

    this.registerInputHandlers();

    this.script.reset();
    this.tryGetNextLine();

    this.cameras.main.fadeIn(1000, 0, 0, 0)

    this.cameras.main.once(Phaser.Cameras.Scene2D.Events.FADE_OUT_COMPLETE, () => {
      this.backgroundAnimBuilder.stopAnimation();
      this.scene.start('OutroScene');
      this.scene.stop('MainScene');
    })
  }

  private registerInputHandlers() {
    this.ignoreInput = this.game.scene.isActive('MainScene');
    this.input.on('pointerup', () => {
      if (this.ignoreInput) {
        return;
      }
      this.tryGetNextLine();
    }, this);

    this.input.keyboard.on('keydown', (event: any) => {
      if (this.ignoreInput) {
        return;
      }

      switch (event.keyCode) {
        case Phaser.Input.Keyboard.KeyCodes.SPACE:
        case Phaser.Input.Keyboard.KeyCodes.ENTER:
          this.tryGetNextLine();
          break;
        default:
          break;
      }
    });
  }

  private tryGetNextLine() {
    const line = this.script.nextLine();
    if (line != null) {
      this.applyLine(line);
    } else {
      this.startTransition();
    }
  }

  private applyLine(line: Line) {
    let textboxVisible = true;
    const activeActor = this.speakers.get(line.actorId);
    const inactiveActors = Array.from(this.speakers.values()).filter(i => i !== activeActor);

    if (line.actions?.includes(ActionId.show)) {
      activeActor?.show(SpeakerState.inactive);
    }
    if (activeActor?.speakerState !== SpeakerState.active) {
      activeActor?.activate();
      inactiveActors
        .filter(a => a.speakerState === SpeakerState.active)
        .forEach(a => a.deactivate());
    }
    if (line.actions?.includes(ActionId.hide)) {
      activeActor?.hide();
      textboxVisible = false;
    }

    this.textBox!.setCharacterName(this.script.actor(line.actorId).name);
    this.textBox!.setText(getLocalizedText(line.text, this.game.registry.get(LanguageRegistryKey)));
    this.textBox!.setVisible(textboxVisible);

    if (line.actions?.includes(ActionId.playBackgroundAnimation)) {
      this.backgroundAnimBuilder.startAnimation(this);
    }
    if (line.actions?.includes(ActionId.finishBackgroundAnimation)) {
      this.backgroundAnimBuilder.stopAnimation();
    }
  }

  private startTransition() {
    this.ignoreInput = true;
    this.cameras.main.fadeOut(1000, 0, 0, 0);
  }

  private createLoadingProgressBar() {
    const progressBar = this.add.graphics();
    const progressBox = this.add.graphics();
    progressBox.fillStyle(0x222222, 0.8);
    progressBox.fillRect(this.game.scale.width - 330, this.game.scale.height - 60, 320, 50);

    this.load.on('progress', (value: number) => {
      progressBar.clear();
      progressBar.fillStyle(0xffffff, 1);
      progressBar.fillRect(this.game.scale.width - 320, this.game.scale.height - 50, 300 * value, 30);
    });

    this.load.on('complete', () => {
      progressBar.destroy();
      progressBox.destroy();
    });
  }
}
