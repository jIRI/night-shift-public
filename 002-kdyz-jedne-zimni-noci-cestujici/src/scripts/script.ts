import { LocalizedText } from "../types/lang";

export enum ActorId {
  left,
  right,
  rightFirst,
}

export enum ActionId {
  playBackgroundAnimation,
  finishBackgroundAnimation,
  show,
  hide,
}

export type ActorConfig = {
  id: ActorId;
  name: string;
  textEncoder?: (text:LocalizedText) => LocalizedText;
};

export type SelectItem = {
  text: string;
  gotoLabel: string;
};

export type Line = {
  actorId: ActorId;
  text: LocalizedText;
  visited?: boolean;
  actions?: Array<ActionId>;
  label?: string;
  select?: Array<SelectItem>;
}

export function getLocalizedText(localizedText: LocalizedText, lang: keyof LocalizedText): string {
  return localizedText[lang];
}

const telepathicEncoderReplacers: Array<(s: string) => string> = [
  s => s.replace(/[AÁ]/g, 'GR'),
  s => s.replace(/[EÉĚ]/g, 'RR'),
  s => s.replace(/[IÍ]/g, 'QR'),
  s => s.replace(/[OÓ]/g, 'RX'),
  s => s.replace(/[UÚŮ]/g, 'WR'),
  s => s.replace(/[YÝ]/g, 'TQ'),
  s => s.replace(/[ŠČŘŽĎŇ]/g, 'WT'),
];

export function applyReplacers(text: string, replacers: Array<(s: string) => string>): string {
  let result = text.toUpperCase();
  for(const replacer of telepathicEncoderReplacers) {
    result = replacer(result);
  }
  return result;
}

export class Script {
  currentLineIndex = 0;
  lines: Array<Line>;
  actorMap = new Map<ActorId, ActorConfig>();

  constructor() {
    this.lines = Script.lines();
    Script.actors().forEach(actor => this.actorMap.set(actor.id, actor));
  }

  actor(id: ActorId): ActorConfig {
    return this.actorMap.get(id) ?? Script.actors()[0];
  }

  reset() {
    this.currentLineIndex = -1;
    this.lines.forEach(line => line.visited = false);
  }

  nextLine(input?: string): Line | null {
    if (this.currentLineIndex < 0) {
      // first invocation, just return first line
      this.currentLineIndex = 0;
    } else {
      if (input != null ) {
        // we got input, try find index of the label
        const nextLineIndex = this.lines.findIndex(line => line.label === input);
        if (nextLineIndex >= 0) {
          // label found, jump to label
          this.currentLineIndex = nextLineIndex;
        } else {
          // label not found, well, continue on next line...
          this.currentLineIndex++;
        }
      } else {
        // no input, just continue on next line
        this.currentLineIndex++;
      }
    }

    if (this.currentLineIndex >= this.lines.length) {
      return null;
    }

    this.lines[this.currentLineIndex].visited = true;
    const line = Object.assign({}, this.lines[this.currentLineIndex]);
    const textEncoder = this.actor(line.actorId).textEncoder;
    if( textEncoder != null) {
      line.text = textEncoder(line.text);
    }
    return line;
  }

  //
  // Actors and script definition follows
  //

  static actors(): Array<ActorConfig>  {
    return [
      { id: ActorId.left, name: 'Zarxerogarus' },
      { id: ActorId.rightFirst, name: 'Geburatrolix' },
      { id: ActorId.right, name: applyReplacers('Unilektiv', telepathicEncoderReplacers), textEncoder: Script.telepathicEncoder },
    ];
  }

  static telepathicEncoder(text: LocalizedText): LocalizedText {
    if(text.cz.startsWith('[') || text.en.startsWith('[')) {
      return text;
    }

    let cz = applyReplacers(`<${text.cz}>`, telepathicEncoderReplacers);
    let en = applyReplacers(`<${text.en}>`, telepathicEncoderReplacers);

    return {
      cz,
      en,
    };
  }



  static lines(): Array<Line> {
    return [
      { actorId: ActorId.left,
        text: {
          cz: '[šoup]',
          en: '',
        },
        actions: [ActionId.playBackgroundAnimation],
      },
      { actorId: ActorId.rightFirst,
        text: {
          cz: '[chmát]',
          en: '',
      }},

      { actorId: ActorId.left,
        text: {
          cz: 'Ehm.',
          en: '',
      }},
      { actorId: ActorId.left,
        text: {
          cz: 'A víš jak, opatrně.',
          en: '',
      }},

      { actorId: ActorId.rightFirst,
        text: {
          cz: 'Eh, jasně. Neboj.',
          en: '',
      }},
      { actorId: ActorId.rightFirst,
        text: {
          cz: '[...]',
          en: '',
      }},
      { actorId: ActorId.rightFirst,
        text: {
          cz: 'Uh, sakra. Jsou to támhle ti vědátoři z pátého?',
          en: '',
      }},
      { actorId: ActorId.left,
        text: {
          cz: 'Ah, to jsou oni. Myslíš, že nás viděli?',
          en: '',
      }},
      { actorId: ActorId.rightFirst,
        text: {
          cz: 'Kámo, z té jejich polyfonní telepatie se mi dělá husí kůže.',
          en: '',
      }},
      { actorId: ActorId.rightFirst,
        text: {
          cz: 'Takže nebudu nic riskovat.',
          en: '',
      }},
      { actorId: ActorId.left,
        text: {
          cz: 'Počkej, ještě jsme se nedomluvili --',
          en: '',
      }},
      { actorId: ActorId.rightFirst,
        text: {
          cz: 'Neboj, na ty rázy v rozvodech se ti kouknu a dám vědět.',
          en: '',
        }},
      { actorId: ActorId.rightFirst,
        text: {
          cz: 'Mizím.',
          en: '',
      }},
      { actorId: ActorId.rightFirst,
        text: {
          cz: '',
          en: '',
        },
        actions: [ActionId.hide],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Kolego.',
          en: '',
        },
        actions: [ActionId.finishBackgroundAnimation, ActionId.show,],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Uhm. Zdravím vás.',
          en: '',
      }},
      { actorId: ActorId.right,
        text: {
          cz: 'S kýmpak jste to tu mluvil?',
          en: '',
        },
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Ále, to byl jeden technik ze sítí.',
          en: '',
      }},
      { actorId: ActorId.right,
        text: {
          cz: 'A copak jste si to tady předávali?',
          en: '',
        },
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Předávali? Jak to myslíte? My jsme u nás na dvanáctém detekovali nějaké podivné výkyvy v energetickém toku, tak jsem ho prosil, aby se na to podíval.',
          en: '',
      }},
      { actorId: ActorId.right,
        text: {
          cz: 'To zase byla nějaká zakázaná média ze Země, že ano?',
          en: '',
      }},
      { actorId: ActorId.left,
        text: {
          cz: 'Média ze Země? To vůbec. Kde bych k takové věci přišel?',
          en: '',
      }},
      { actorId: ActorId.right,
        text: {
          cz: 'Jen si nemyslete. My jsme vás viděli jak se tuhle paktujete s tím protivou z archívu.',
          en: '',
      }},
      { actorId: ActorId.left,
        text: {
          cz: 'Z archívu? Myslíte Yoraggoraxurose? Ale to si ho musíte s někým plést.',
          en: '',
      }},
      { actorId: ActorId.right,
        text: {
          cz: 'Ha, takže se znáte. My jsme to věděli!',
          en: '',
      }},
      { actorId: ActorId.right,
        text: {
          cz: 'Možná bychom ho mohli nahlásit pro podezření, že šíří po institutu Pozemský brak.',
          en: '',
      }},
      { actorId: ActorId.right,
        text: {
          cz: 'A mohlo by se udělat vyšetřování.',
          en: '',
      }},
      { actorId: ActorId.right,
        text: {
          cz: 'To bychom pak viděli jak to je, že?',
          en: '',
      }},
      { actorId: ActorId.left,
        text: {
          cz: '[polk]',
          en: '',
      }},
      { actorId: ActorId.left,
        text: {
          cz: 'Tak já myslím, že pouštět se hned do vyšetřování je dost extrémní, nemyslíte?',
          en: '',
      }},
      { actorId: ActorId.left,
        text: {
          cz: '[kašly kašly]',
          en: '',
      }},
      { actorId: ActorId.left,
        text: {
          cz: 'Navíc i kdyby on se třeba někdy zmínil o něčem s čím přijde u nich na oddělení do styku, tak to nikdy není žádný brak nebo jak jste to říkali.',
          en: '',
      }},
      { actorId: ActorId.right,
        text: {
          cz: 'Ale?',
          en: '',
      }},
      { actorId: ActorId.left,
        text: {
          cz: 'On má náhodou vytříbený vkus.',
          en: '',
      }},
      { actorId: ActorId.right,
        text: {
          cz: 'Jako například?',
          en: '',
      }},
      { actorId: ActorId.left,
        text: {
          cz: 'Jako tuhle třeba říkal, samozřejmě jen ve velmi hrubých rysech a v rozsahu v jakém to směrnice dovoluje, že narazil na knihu s takovým zvláštním názvem.',
          en: '',
      }},
      { actorId: ActorId.left,
        text: {
          cz: 'Když jedné zimní noci cestující.',
          en: '',
      }},
      { actorId: ActorId.right,
        text: {
          cz: 'Prosím?',
          en: '',
      }},
      { actorId: ActorId.left,
        text: {
          cz: 'Tak se to jmenuje. Když jedné zimní noci cestující. Je to od toho chlápka, co napsal Neviditelná města.',
          en: '',
      }},
      { actorId: ActorId.left,
        text: {
          cz: '[...]',
          en: '',
      }},
      { actorId: ActorId.left,
        text: {
          cz: 'To je taky moc zajímavá záležitost.',
          en: '',
      }},
      { actorId: ActorId.left,
        text: {
          cz: 'Prý.',
          en: '',
      }},
      { actorId: ActorId.left,
        text: {
          cz: 'Žádný brak.',
          en: '',
      }},
      { actorId: ActorId.right,
        text: {
          cz: 'Áno?',
          en: '',
      }},
      { actorId: ActorId.left,
        text: {
          cz: 'Ano, ano. Když jedné zimní noci cestující je takový jako postmoderní experimentální román, bez příběhu v tradičním stylu.',
          en: '',
      }},
      { actorId: ActorId.left,
        text: {
          cz: 'Ale.',
          en: '',
      }},
      { actorId: ActorId.left,
        text: {
          cz: 'Jsou to takové úryvky z fiktivních knih a mezi nimi se děje meta příběh Čtenáře a Čtenářky, kteří se snaží sehnat kompletní kopii Když jedné zimní noci cestující.',
          en: '',
      }},
      { actorId: ActorId.left,
        text: {
          cz: 'Jak říkám, hodně meta. Každá z kapitol má mít jiný žánr a má působit jako od jiného autora. I když já bych řekl, že stejně je v tom pořád cítit jedna ruka.',
          en: '',
      }},
      { actorId: ActorId.left,
        text: {
          cz: 'Taky se to snaží působit, jako by mezi jednotlivými kapitolami byla nějaká souvislost, jako by se postavy přesouvaly z jedné do druhé.',
          en: '',
      }},
      { actorId: ActorId.left,
        text: {
          cz: 'Ale podle mě je to taková jako hra. Buzení dojmu spíš než skutečná souvislost.',
          en: '',
      }},
      { actorId: ActorId.left,
        text: {
          cz: 'Ono se to tam i v jedné části metapříběhu probírá, jak čtenáři hledají a nachází významy, které autor nezamýšlel.',
          en: '',
      }},
      { actorId: ActorId.left,
        text: {
          cz: 'Trochu mi to v tomhle připomnělo právě Neviditelná města.',
          en: '',
      }},
      { actorId: ActorId.left,
        text: {
          cz: 'Tam má člověk taky pocit, že popisy jednotlivých měst jsou metafory. Občas působí jako druhy emocí, občas jako druhy lidských povah.',
          en: '',
      }},
      { actorId: ActorId.left,
        text: {
          cz: 'A občas jako něco úplně jiného.',
          en: '',
      }},
      { actorId: ActorId.left,
        text: {
          cz: 'Je to malinko jako sbírat rtuť pinzetou -- už to skoro vypadá, že to člověk má, ale místo toho jen vnikne víc nepořádku.',
          en: '',
      }},
      { actorId: ActorId.left,
        text: {
          cz: 'Tak něco.',
          en: '',
      }},
      { actorId: ActorId.left,
        text: {
          cz: '[kašly kašly]',
          en: '',
      }},
      { actorId: ActorId.left,
        text: {
          cz: 'Takže podobně asi musí působit i Když jedné zimní noc cestující.',
          en: '',
      }},
      { actorId: ActorId.right,
        text: {
          cz: 'Takže trochu jako reakce na, nebo experimentování s Barthesovu Smrtí autora?',
          en: '',
      }},
      { actorId: ActorId.left,
        text: {
          cz: 'Uh, já Barthese neznám, ale ano, zní to přibližně správně -- autor není pro výklad textu podstatný, celá interpretace je na čtenáři.',
          en: '',
      }},
      { actorId: ActorId.right,
        text: {
          cz: 'Dobrá tedy.',
          en: '',
      }},
      { actorId: ActorId.right,
        text: {
          cz: 'Pokud se nám tu nešíří brak, pak je asi všechno v pořádku.',
          en: '',
      }},
      { actorId: ActorId.left,
        text: {
          cz: 'Ne, ne. Žádný brak, všechno v pořádku.',
          en: '',
      }},
      { actorId: ActorId.right,
        text: {
          cz: 'A bylo to Když jedné zimní noci cestující?',
          en: '',
      }},
      { actorId: ActorId.left,
        text: {
          cz: 'Ano, ano. Když jedné zimní noci cestující, přesně jak říkáte.',
          en: '',
      }},
      { actorId: ActorId.right,
        text: {
          cz: '[...]',
          en: '',
      }},
      { actorId: ActorId.right,
        text: {
          cz: 'Nebudeme vás tedy už dále zdržovat.',
          en: '',
      }},
      { actorId: ActorId.right,
        text: {
          cz: 'Přejeme klidný zbytek noční směny.',
          en: '',
      }},
      { actorId: ActorId.left,
        text: {
          cz: 'Ah, děkuji! Vám taky.',
          en: '',
      }},
      { actorId: ActorId.right,
        text: {
          cz: '',
          en: '',
        },
        actions: [ActionId.hide],
      },
      { actorId: ActorId.left,
        text: {
          cz: '[...]',
          en: '',
      }},
      { actorId: ActorId.left,
        text: {
          cz: 'Uf.',
          en: '',
      }},
      { actorId: ActorId.left,
        text: {
          cz: 'O fous.',
          en: '',
        },
      },
      { actorId: ActorId.left,
        text: {
          cz: '',
          en: '',
        },
        actions: [ActionId.hide],
      },
   ];
  }
}