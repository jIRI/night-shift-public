
export type DialogBoxConfig = {
  scene: Phaser.Scene;
  textureNames: {
    box: string;
    name: string;
    continue: string;
    continueHighlighted: string;
  };
};

export class DialogBox extends Phaser.GameObjects.Container {
  config: DialogBoxConfig;
  characterName: Phaser.GameObjects.Text;
  text: Phaser.GameObjects.Text;
  imageBox: Phaser.GameObjects.Image;
  imageName: Phaser.GameObjects.Image;
  imageContinue: Phaser.GameObjects.Image;
  imageContinueHighlight: Phaser.GameObjects.Image;
  revealTextTween: Phaser.Tweens.Tween;
  continueTween: Phaser.Tweens.Tween;

  constructor(config: DialogBoxConfig) {
    super(config.scene);
    this.config = config;

    this.imageName = new Phaser.GameObjects.Image(this.scene, 0, 0, this.config.textureNames.name).setOrigin(0, 0);
    this.imageBox = new Phaser.GameObjects.Image(this.scene, 0.06 * this.imageName.width, 0.75 * this.imageName.height, this.config.textureNames.box).setOrigin(0, 0);
    this.imageContinue = new Phaser.GameObjects.Image(this.scene, this.imageBox.width, 0.84 * (this.imageName.height + this.imageBox.height), this.config.textureNames.continue).setOrigin(1, 1);
    this.imageContinueHighlight = new Phaser.GameObjects.Image(this.scene, this.imageBox.width, this.imageBox.height, this.config.textureNames.continueHighlighted).setOrigin(1, 1).setVisible(false);

    this.text = new Phaser.GameObjects.Text(
      this.scene,
      3 * this.imageBox.x, 1.3 * this.imageBox.y,
      '',
      {
        fontFamily: 'serif',
        fontSize: '4em',
        color: '#14EAE8',
        maxLines: 3,
        wordWrap: {
          width: 0.9 * this.imageBox.width,
        }
    }).setOrigin(0, 0);

    this.characterName = new Phaser.GameObjects.Text(
      this.scene,
      this.imageName.width / 2, 0.4 * this.imageName.height,
      '',
      {
        fontFamily: 'serif',
        fontSize: '4em',
        color: '#000000',
        maxLines: 1,
    }).setOrigin(0.5, 0.5);

    this.revealTextTween = this.scene.tweens.add({
      targets: this.text,
      alpha: { from: 0, to: 1 },
      ease: Phaser.Math.Easing.Cubic.In,
      duration: 200,
    }).on('complete', () => this.continueTween.restart());

    this.continueTween = this.scene.tweens.add({
      targets: this.imageContinue,
      alpha: { from: 0, to: 1 },
      ease: Phaser.Math.Easing.Linear,
      repeat: -1,
      duration: 1000,
      yoyo: true,
    });

    this.add(this.imageName);
    this.add(this.imageBox);
    this.add(this.imageContinueHighlight);
    this.add(this.imageContinue);
    this.add(this.characterName);
    this.add(this.text);

    this.setSize(this.imageBox.width, 0.95 * (this.imageName.height + this.imageBox.height));
    this.setScale(1, 1);

    this.scene.add.container((this.scene.game.scale.width - this.width ) / 2, (this.scene.game.scale.height - this.height), this);
  }

  setCharacterName(name: string) {
    this.characterName.setText(name);
  }

  setText(text: string) {
    this.continueTween.stop();
    this.imageContinue.setAlpha(0);
    this.text.setText(text);
    this.revealTextTween.restart();
  }
}