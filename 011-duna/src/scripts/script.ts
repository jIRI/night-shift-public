import { AvailableSounds, SoundPlayerBuilder, Sounds } from "../builders/sound-player";
import { LocalizedText } from "../types/lang";

export enum ActorId {
  left,
  right,
  robot,
  drone,
  dummy,
}

export enum ActionId {
  playAnimation,
  continueAnimation,
  finishAnimation,
  show,
  hide,
  enableAnimationSync,
  disableAnimationSync,
  playSound,
}

export type ActionConfig = {
  do: ActionId.playAnimation
  | ActionId.continueAnimation
  | ActionId.finishAnimation
  | ActionId.show
  | ActionId.hide
  | ActionId.enableAnimationSync
  | ActionId.disableAnimationSync,
} | {
  do: ActionId.playSound,
  sound: AvailableSounds
};

export type ActorConfig = {
  id: ActorId;
  name: string;
  nameEncoder?: (text:LocalizedText) => LocalizedText;
  textEncoder?: (text:LocalizedText) => LocalizedText;
};

export type SelectItem = {
  text: string;
  gotoLabel: string;
};

export type Line = {
  actorId: ActorId;
  text: LocalizedText;
  visited?: boolean;
  actions?: Array<ActionConfig>;
  label?: string;
  select?: Array<SelectItem>;
}

export function getLocalizedText(localizedText: LocalizedText, lang: keyof LocalizedText): string {
  return localizedText[lang];
}

const telepathicEncoderReplacers: Array<(s: string) => string> = [
  s => s.replace(/[AÁ]/g, 'GR'),
  s => s.replace(/[EÉĚ]/g, 'RR'),
  s => s.replace(/[IÍ]/g, 'QR'),
  s => s.replace(/[OÓ]/g, 'RX'),
  s => s.replace(/[UÚŮ]/g, 'WR'),
  s => s.replace(/[YÝ]/g, 'TQ'),
  s => s.replace(/[ŠČŘŽĎŇ]/g, 'WT'),
];

export function applyReplacers(text: string, replacers: Array<(s: string) => string>): string {
  let result = text.toUpperCase();
  for(const replacer of replacers) {
    result = replacer(result);
  }
  return result;
}

export function applyReplacersToParts(text: string, replacers: Array<(s: string) => string>): string {
  let result = '';
  let parts = text.split('|');
  while(parts.length > 0) {
    result = result
      .concat(applyReplacers(parts.shift() ?? '', replacers))
      .concat(parts.shift() ?? '');
  }
  return result;
}

export class Script {
  currentLineIndex = 0;
  lines: Array<Line>;
  actorMap = new Map<ActorId, ActorConfig>();

  constructor() {
    this.lines = Script.lines();
    Script.actors().forEach(actor => this.actorMap.set(actor.id, actor));
  }

  actor(id: ActorId): ActorConfig {
    return this.actorMap.get(id) ?? Script.actors()[0];
  }

  reset() {
    this.currentLineIndex = -1;
    this.lines.forEach(line => line.visited = false);
  }

  nextLine(input?: string): Line | null {
    if (this.currentLineIndex < 0) {
      // first invocation, just return first line
      this.currentLineIndex = 0;
    } else {
      if (input != null ) {
        // we got input, try find index of the label
        const nextLineIndex = this.lines.findIndex(line => line.label === input);
        if (nextLineIndex >= 0) {
          // label found, jump to label
          this.currentLineIndex = nextLineIndex;
        } else {
          // label not found, well, continue on next line...
          this.currentLineIndex++;
        }
      } else {
        // no input, just continue on next line
        this.currentLineIndex++;
      }
    }

    if (this.currentLineIndex >= this.lines.length) {
      return null;
    }

    this.lines[this.currentLineIndex].visited = true;
    const line = Object.assign({}, this.lines[this.currentLineIndex]);
    const textEncoder = this.actor(line.actorId).textEncoder;
    if( textEncoder != null) {
      line.text = textEncoder(line.text);
    }
    return line;
  }

  //
  // Actors and script definition follows
  //

  static actors(): Array<ActorConfig>  {
    return [
      { id: ActorId.left, name: 'Moravixuran' },
      { id: ActorId.right, name: 'Rutygamurosus' },
    ];
  }

  static telepathicEncoder(text: LocalizedText): LocalizedText {
    if(text.cz.startsWith('[') || text.en.startsWith('[')) {
      return text;
    }

    let cz = text.cz;
    let en = text.en;

    if(cz.startsWith('||') || en.startsWith('||')) {
      cz = cz.substr(2);
      en = en.substr(2);
    } else {
      cz = `<${applyReplacersToParts(text.cz, telepathicEncoderReplacers)}>`;
      en = `<${applyReplacersToParts(text.en, telepathicEncoderReplacers)}>`;
    }

    return {
      cz,
      en,
    };
  }

  static droidEncoder(text: LocalizedText): LocalizedText {
    if(text.cz.startsWith('') || text.en.startsWith('')) {
      return text;
    }

    let cz = `{ ${text.cz} }`;
    let en = `{ ${text.en} }`;

    return {
      cz,
      en,
    };
  }

  static lines(): Array<Line> {
    return [
      { actorId: ActorId.right,
        text: {
        cz: '...a věda a výzkum proto navrhla vypnout všech devět energopump, ze kterých tenepy odsávají energii.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.right,
        text: {
        cz: 'Údržba pracuje na přepojení okruhů, protože postižené pumpy jsou ve všech pěti redundantních skupinách, které máme, takže se nedají jednoduše odpojit, aniž bychom přišli o energii v celém úřadu.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Jak jsi požadovala, denní a noční směna jsou na překrývajících se přesčasech, ale obávám se, že to nebude stačit a pořád postupujeme pomalu.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Hmm. Dobrá. Má někdo nějaké další návrhy?',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Bezpečnostní povolalo polovinu záložáků, takže pokud by něco prosáklo, měli bychom mít dost sil v pohotovosti na sanaci, ale jelikož pořád nevíme, co je účelem těch tenepů, je těžké odhadovat, jaké budou důsledky toho, pokud se zaktivují.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Co navrhuje věda a výzkum pro případ, že by opravdu šlo o kompletní multimodální reentaglement?',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Eh, archív tvrdí, že zprávy ze stejného archeologického průzkumu naznačují, že by mělo existovat zařízení, které dokáže entaglement demultimodalizovat, ale nemají žádné podrobnosti.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Chm. Ať sekční vedoucí začnou nominovat nejlepší lidi na terénní průzkum, pokud archív dohledá nějaké informace o místech, kde by bylo rozumné to potenciální zařízení hledat a začněte je rozdělovat do tří týmů.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Jakmile budeme vědět kam, okamžitě zahájíme výsadky.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.right,
        text: {
          cz: '[mumly mumly]',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Zařízeno. Budu tě informovat jakmile budeme připraveni.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Dobrá tedy. Něco dalšího?',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'To je zatím všechno.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: '[vzdych]',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Nic nezažene nudu tak, jako pořádná krize, co?',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Ehm. Ano, ředitelko.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Ah, a mimochodem -- máš pro mě nějaké další pozemské knihy? Ta poslední várka byla výborná.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Myslíš Dunu?',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Ano, ano. To byla dobrá série. Četl jsi to?',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Eeeee, první knihu?',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Ah, ta je dobrá, ale rozhodně si přečti i zbytek. Je to sice náročnější čtení, ale podle mě to stojí za to. Tedy těch prvních šest co napsal autor sám, ty co dopsal jeho syn s nějakým jiným autorem, ty podle všeho nestojí za moc.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'No, já jsem zkoušel Spasitele Duny, ale přišlo mi to trochu moc pomalé a trochu moc... hmmm... jiné? Ta první kniha, Duna, už ta je místy dost náročná, takže do té druhé jsem se nemohl nějak dostat.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Zkus to znova. Ano, souhlasím, že Duna je ve srovnání s dalšími pěti jako dobrodružný román nabitý akcí, ale rozhodně si myslím, že na konci budeš rád.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Já nevím, mně to přišlo příliš jako práce, a ne dost jako zábava, abych byl upřímný.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'No, to chápu, ale to úsilí při čtení určitě stojí za to.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Nóó, a o čem tedy ty další knihy jsou? Rámcově?',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Ech, všeobecně: po tom, co Duna ustaví situaci a přivede hlavního hrdinu na místo, kde si myslíš, že má vyhráno a bude spravedlivě a moudře vládnout světu, Spasitel Duny dost rekontextualizuje jeho roli vyvoleného a vidíš, kam ho jeho schopnosti a situace přivedly.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Děti duny se zabývají osudem jeho dětí, a jaké jsou machinace kolem nich. Je to asi trochu slabší počin mezi prvními čtyřmi knihami, ale mně to pořád bavilo a přišlo zajímavé.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Božský imperátor Duny je o tom, co se v dlouhodobém horizontu děje s jedním z těch dětí po tom, co se stane imperátorem. Pokud se rozhodneš, že po Božském imperátorovi máš dost, můžeš skončit u něj, stejně je to asi vrchol série.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Kacíři Duny a Kapitula: Duna jsou dost těsně propojené a zabývají se následky působení toho imperátora a upřímně občas jdou dost zvláštními cestami, takže je na zvážení, jestli s nimi chceš pokračovat.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'A skončí to celé nějak uspokojivě?',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Záleží, co si představuješ pod pojmem uspokojivě. Pokud čekáš, že všechno bude uzavřeno a vysvětleno, tak ne. Ale to je věc, na kterou tě vlastně připraví už Duna -- věci končí nějakým logickým vyústěním, ale rozhodně to není žádné "a šťastně žili na věky věků".',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Nicméně to, co je tématem té které knihy je vždycky nějak uzavřeno.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'A pořád je to takový ten styl, kde přeskakujeme mezi postavami a jejich úhlem pohledu a vnitřním monologem čtyřikrát na jedné stránce?',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Samozřejmě, to je na tom to skvělé.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Takže i dialogy jsou pořád vedeny tak, že si postavy rozumí na základě jednoho slova, a já budu muset skoro pořád hloubat o čem to vlastně mluví a tak podobně?',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Ano. A jsou to samá zajímavá témata jako systémy vládnutí, společenská stagnace, náboženství a tak podobně.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.right,
        text: {
          cz: 'Uhm. No můžu to zkusit, ale nevím, nevím, jestli je to úplně můj šálek--',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.right,
        text: {
          cz: '[šok]',
          en: '',
        },
        actions: [ { do: ActionId.playAnimation }, { do: ActionId.enableAnimationSync } ],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Ah, sakra.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Celou výkonnou skupinu do velké zasedačky.',
          en: '',
        },
        actions: [],
      },
      { actorId: ActorId.left,
        text: {
          cz: 'Okamžitě.',
          en: '',
        },
        actions: [],
      },
  ];
  }
}