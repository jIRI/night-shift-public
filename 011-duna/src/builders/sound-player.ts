export class Sounds {
  noise: Phaser.Sound.BaseSound | null = null;
}

export type AvailableSounds = keyof Sounds;

export class SoundPlayerBuilder extends Sounds {
  preloadAssets(scene: Phaser.Scene) {
    scene.load.audio('noise-sound', 'audio/noise.mp3');
  }

  createObjects(scene: Phaser.Scene) {
    this.noise = scene.sound.add('noise-sound', {
      volume: 0.3,
      rate: 1,
    });
  }

  play(key: AvailableSounds) {
    switch(key) {
      case 'noise':
        this.noise?.play();
        return;
      default:
        return;
    }
  }
}