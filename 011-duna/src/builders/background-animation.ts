import { SoundPlayerBuilder } from "./sound-player";

export class BackgroundAnimationBuilder {
  soundPlayer: SoundPlayerBuilder | null = null;

  currentTweenIndex = 0;
  animationTweens = new Array<Phaser.Tweens.Tween>();
  activeTween: Phaser.Tweens.Tween | null = null;
  stopped = false;

  viewGood: Phaser.GameObjects.Image | null = null;
  viewBad: Phaser.GameObjects.Image | null = null;

  preloadAssets(scene: Phaser.Scene) {
    scene.load.image('view-good', 'images/view-good.png');
    scene.load.image('view-bad', 'images/view-bad.png');
  }

  createObjects(scene: Phaser.Scene, soundPlayer: SoundPlayerBuilder) {
    this.soundPlayer = soundPlayer;

    const speedModifier = 1;
    this.viewGood = scene.add.image(scene.game.scale.width / 2, 0, 'view-good').setOrigin(0.5, 0).setAlpha(1).setDepth(-20).setVisible(true);
    this.viewBad = scene.add.image(scene.game.scale.width / 2, 0, 'view-bad').setOrigin(0.5, 0).setAlpha(0).setDepth(-15).setVisible(true);

    //on demand anims
    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.viewGood,
      alpha: 1,
      duration: speedModifier * 300,
      onStart: _ => {
        this.soundPlayer?.play('noise');
      },
      onComplete: _ => {
        this.playNext();
      },
    }));
    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.viewGood,
      alpha: { from: 1, to: 0 },
      yoyo: true,
      repeat: 3,
      duration: speedModifier * 60,
      onStart: _ => {
        this.playNext();
      },
      onComplete: _ => {
        this.viewGood?.setAlpha(0);
      },
    }));
    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.viewBad,
      alpha: { from: 0, to: 1 },
      yoyo: true,
      repeat: 3,
      duration: speedModifier * 60,
      onComplete: _ => {
        this.viewBad?.setAlpha(1);
        this.playNext();
      },
    }));
    this.animationTweens.push(scene.add.tween({
      paused: true,
      delay: 1000,
      targets: this.viewGood,
      alpha: 0,
      duration: speedModifier * 300,
      onStart: _ => {
        this.soundPlayer?.play('noise');
      },
      onComplete: _ => {
        this.playNext();
      },
    }));
    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.viewGood,
      alpha: { from: 0, to: 1 },
      yoyo: true,
      repeat: 3,
      duration: speedModifier * 60,
      onStart: _ => {
        this.playNext();
      },
      onComplete: _ => {
        this.viewGood?.setAlpha(1);
      },
    }));
    this.animationTweens.push(scene.add.tween({
      paused: true,
      targets: this.viewBad,
      alpha: { from: 1, to: 0 },
      yoyo: true,
      repeat: 3,
      duration: speedModifier * 60,
      onComplete: _ => {
        this.viewBad?.setAlpha(0);
      },
    }));
  }

  startAnimation(scene: Phaser.Scene) {
    this.stopped = false;
    this.currentTweenIndex = 0;
    this.animationTweens.forEach(t => t.setTimeScale(1));

    this.playNext();
  }

  stopAnimation() {
    this.stopped = true;
    this.animationTweens.forEach(tween => {
      tween.stop();
      tween.setTimeScale(1);
    });
  }

  playNext() {
    if (this.stopped) {
      return;
    }

    if (this.currentTweenIndex < this.animationTweens.length) {
      this.activeTween = this.animationTweens[this.currentTweenIndex++];
      this.activeTween?.play();
    }
  }

  isAnimationPlaying() {
    return this.activeTween?.isPlaying() ?? false;
  }

  skip() {
    if (this.stopped) {
      return;
    }

    this.activeTween?.setTimeScale(100);
  }

}